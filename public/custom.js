/* eslint-disable no-undef */
/* jshint-disable no-undef */
PNotify.prototype.options.styling = 'bootstrap3'
PNotify.prototype.options.styling = 'fontawesome'
function consumeAlert () {
  window.alert = function (message, type = 'success') {
    PNotify.removeAll()
    new PNotify({
      text: message,
      type: type,
      delay: 3000,
      styling: 'bootstrap3',
      icon: false
    })
  }
  window.prompt = function (message, success, failure) {
    PNotify.removeAll()(new PNotify({
      title: 'Confirmation Needed',
      text: 'Are you sure?',
      // icon: 'glyphicon glyphicon-question-sign',
      icon: false,
      hide: false,
      confirm: {
        confirm: true
      },
      buttons: {
        closer: false,
        sticker: false
      },
      history: {
        history: false
      }
    })).get().on('pnotify.confirm', success).on('pnotify.cancel', failure)
  }
}

consumeAlert()

function consumeConnfirm () {
  PNotify.removeAll()
  window.confirm = function (message, successCallback, errorCallback) {
    (
      new PNotify({
        title: 'Are you sure?',
        text: message,
        // icon: 'glyphicon glyphicon-question-sign',
        icon: false,
        hide: true,
        confirm: {
          confirm: true
        },
        buttons: {
          closer: false,
          sticker: false
        },
        history: {
          history: false
        }
      })
    ).get().on('pnotify.confirm', successCallback)
  }
}
consumeConnfirm()

jQuery(document).ready(function () {
  jQuery(document).on('click', '.navbar-toggle', function () {
    jQuery('#sidebar').removeClass('menu-min')
    jQuery('#sidebar').toggleClass('mobile')
  })
  jQuery(document).on('keydown change', 'input', function () {
    jQuery(this).removeClass('contains-error')
    jQuery(this).siblings('.with-error').remove()
  })
  jQuery(document).on('click', '.Select', function () {
    jQuery(this).find('.contains-error').removeClass('contains-error')
    jQuery(this).siblings('.with-error').remove()
  })
})
jQuery(document).on('click', '.trasher', function (event) {
  event.preventDefault()
  var th = jQuery(this)
  confirm('Are your sure?', function () {
    th.parent().parent().remove()
  })
})

String.prototype.filename = function () {
  var filename = this.split('--')[0].split('/').pop();
  return filename.slice(0,24)+'.'+filename.split('.').pop();
}

String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
}