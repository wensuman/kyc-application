export const DASHBOARD = 'DASHBOARD';
export const SAVER = 'SAVER';
export const GETTER = 'GETTER';

export function dashboard() {
    return {
        type: DASHBOARD,
        payload: ''
    }
}

export const actions = {
    dashboard,
}

const ACTION_HANDLERS = {
    [DASHBOARD]: (state, action) => {

    }
};

const initialState = {}

export default function reducer(state = initialState, action) {

    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}
