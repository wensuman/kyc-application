import {injectReducer} from '../../store/reducers'
export default (store) => ({
    path: '*',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const container = require('./container').default;
            const reducer = require('./module').default;
            injectReducer(store, {key: 'dashboard', reducer});
            cb(null, container);
        }, '*');
    }
})
