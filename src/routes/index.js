import CoreLayout from '../layouts/corelayout'
import ErrorLayout from '../layouts/errorlayout'
import DashboardRoute from './user/dashboard'
import TemplateRoute from './user/template'
import TemplateViewRoute from './user/template-view'
import ProfileRoute from './user/profile'
import SubmissionRoute from './user/submissions'
import AuthLayout from '../layouts/authlayout'
import LoginRoute from './auth/login'
import RegisterRoute from './auth/register'
import VerifyRoute from './auth/verify'
import TicketRoute from './user/ticket'
import NotFoundRoute from './notfound'
import ForgotRoute from './auth/forgot-password'

export const createRoutes = (store) => ({
  childRoutes: [
    {
      component: AuthLayout,
      childRoutes: [
        LoginRoute(store),
        RegisterRoute(store),
        VerifyRoute(store),
        ForgotRoute(store)
      ]
    },
    {
      component: CoreLayout,
      childRoutes: [
        DashboardRoute(store),
        TemplateRoute(store),
        TemplateViewRoute(store),
        ProfileRoute(store),
        SubmissionRoute(store),
        TicketRoute(store)
      ]
    },
    {
      component: ErrorLayout,
      childRoutes: [
        NotFoundRoute(store)
      ]
    }
  ]
})

export default createRoutes
