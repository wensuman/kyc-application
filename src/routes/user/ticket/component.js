import React from 'react'
import { Urls } from '../../../config/app'
import Ajax from '../../../modules/request'
import Select from 'react-select'
import 'react-select/dist/react-select.css'

export class Ticket extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loaded: false,
      bank_id: '',
      tickets: [],
      pagination: {links: {}}
    }
    this.loadTickets = this.loadTickets.bind(this)
    this.submit = this.submit.bind(this)
    this.loadBanks = this.loadBanks.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount () {
    this.loadTickets()
  }

  loadTickets (url) {
    Ajax().get(url || Urls.tickets).then((response) => {
      this.setState({
        tickets: response.data.data,
        pagination: response.data.meta.pagination
      })
    })
  }

  loadBanks () {
    Ajax().get(Urls.banks).then((response) => this.setState({
      banks: response.data.data.map(function (bank) {
        return {label: bank.profile.name, value: bank.id}
      })
    }))
  }

  submit () {
    if(!this.state.question_title){
      alert('Please, enter a title for your question.','error');
      return false;
    }
    if(!this.state.question_detail){
      alert('Please, describe your questions in few lines','error');
      return false;
    }

    if (this.state.question_detail && this.state.question_title)
      Ajax().post(Urls.tickets, {
        title: this.state.question_title,
        detail: this.state.question_detail,
      }).then((response) => {
        if (response.status === 201) {
          alert('Your ticket has been successfully created.')
          this.setState({question_detail: '', question_title: ''})
          this.loadTickets()
          return
        }
        alert('Something went wrong. Please try again later.', 'error')
      })
  }

  render () {
    return (
      <div className="card animated fadeIn">
        <div className="card-content">
          <h3 className="card-title">Send us your query</h3>
          <form>
            <div className="col-sm-12 no-padding">
              <div className="create-ticket-wrapper">
                <div className="col-md-12 no-padding">
                  <div className='form-group'>
                    <label className="label-control">Title</label>
                    <input  value={this.state.question_title} type="text" name="title" onChange={(event) => {this.setState({question_title: event.target.value})}}
                           className="form-control"/>
                  </div>
                </div>
                <div className="col-md-12 no-padding">
                  <div className='form-group'>
                    <label className="label-control">Description</label>
                    <textarea
                      onChange={(event) => {this.setState({question_detail: event.target.value})}}
                      className="form-control" name="descriptions" value={this.state.question_detail}
                      cols="80"></textarea>
                  </div>
                </div>
                <div className="col-sm-12 no-padding">
                  <button onClick={this.submit} title="Submitting your question" type="button"
                          className="btn btn-primary">
                    Submit
                  </button>
                </div>
              </div>
            </div>
            {this.state.tickets && this.state.tickets.length > 0 && <div className="col-sm-12 no-padding">
              <div className="ticket-response">
                <div className="response-wrapper">
                  {this.state.tickets.map((map, index) => (
                    <div key={map.id} className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href={`#collapse${index}`}
                             aria-expanded="false" aria-controls="collapseOne" className="collapsed">
                            <h5 className="panel-title collapse-head">
                              {map.ticket.title}
                              <i className="material-icons pull-right">keyboard_arrow_down</i>
                            </h5>
                          </a>
                          {map.ticket.answer && <span className="checked-question">
                                                       <i className="material-icons">done</i>
                                                    </span>}
                        </div>
                        <div id={`collapse${index}`} className="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOne" aria-expanded="false" style={{height: '0px'}}>
                          <div className="panel-body">
                            <div className="col-xs-12 question-detail"><span
                              className="answer-detail">Que:</span>{map.ticket.detail}</div>
                            {map.ticket.answer && <div className="col-xs-12"><span className="answer-detail">Ans:</span><span
                              className="ans-note">{map.ticket.answer}</span></div> }
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <div className="col-xs-12">
                <div className='pagination-buttons pull-right'>
                  {this.state.pagination.links.previous && <button className="btn btn-primary" onClick={(event) => {
                    event.preventDefault()
                    this.loadTickets(this.state.pagination.links.previous)
                  }}>Previous</button> }
                  {this.state.pagination.links.next && <button className="btn btn-primary" onClick={(event) => {
                    event.preventDefault()
                    this.loadTickets(this.state.pagination.links.next)
                  }}>Next</button> }
                </div>
              </div>
            </div>}
            <div className="clearfix"></div>
          </form>
        </div>
      </div>
    )
  }
}

export default Ticket

