import {connect} from 'react-redux'
export default (store) => ({
    path: 'support',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            cb(null, connect((state)=>state, {})(require('./component').default));
        }, 'support');
    }
})
