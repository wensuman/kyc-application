import React from 'react'
import Ajax from '../../../modules/request'
import { Urls } from '../../../config/app'
import 'react-select/dist/react-select.css'
import Loader from '../../../components/loader'

export class Profile extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      old_password: '',
      new_password: '',
      new_password_confirmation: '',
      loading: true
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.formValid = this.formValid.bind(this)
    this.changePassword = this.changePassword.bind(this)
  }

  componentDidMount () {
    Ajax().get(Urls.form.template).then((response) => {
      let data = response.data.data
      this.setState({...data, loading: false})
    })
  }

  changePassword (event) {
    event.preventDefault();
    if ( this.formValid() ) {      
      Ajax().post(Urls.changePassword, this.state).then((response) => {
        if (response.status === 202) {
          this.setState({
            'new_password': '',
            'old_password': '',
            'new_password_confirmation': ''
          })
          alert('Password Changed Successfully', 'success')
          return
        }
        alert('Something went wrong. Please make sure, your old password is correct.', 'error')
      }).catch(() => {
      })
    } else {
      alert('Password mismatched or password length less than 8.', 'error');
    }
  }

  formValid () {
    return this.state.old_password && this.state.new_password.length >= 8 && (this.state.new_password === this.state.new_password_confirmation)
  }

  render () {
    if (this.state.loading) {
      return <div><Loader/></div>
    }
    return (
      <div className="container-fluid profile-section animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-content">
                <h3 className="card-title">Reset Password</h3>
                <div className="logo"></div>
                <div className="single-line">
                  <label className="control-label">Old Password</label>
                  <input className='form-control' title="Please input your old password" id="passOne"
                         onChange={(event) => {this.setState({old_password: event.target.value})}} type="password"
                         value={this.state.old_password}/>
                </div>
                <div className="single-line">
                  <label className="control-label">New Password</label>
                  <input title="Password must be atleast 8 character" id="passThree"
                         onChange={(event) => {this.setState({new_password: event.target.value})}} type="password"
                         className="form-control"
                         maxLength={10}
                         value={this.state.new_password} minLength={8} />
                </div>
                <div className="single-line">
                  <label className="control-label">Confirm New Password</label >
                  <input title="Type your password again" id="passTwo"
                         onChange={(event) => {this.setState({new_password_confirmation: event.target.value})}}
                         className="form-control"
                         maxLength={10}
                         type="password" value={this.state.new_password_confirmation} minLength={8} />
                  <div className="hide-show"></div>
                </div>
                <button className="btn btn-rose btn-fill" title="Pressing this button will change your password"
                        onClick={this.changePassword}>
                  Change
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Profile
