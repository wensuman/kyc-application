import React from 'react'
import { Urls } from '../../../config/app'

export class View extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    if (this.props.show !== undefined && !this.props.show) {
      return <div></div>;

    }
    if ( this.props.children  ||  this.props.value ) {

      return ( 

        <div className="template-part-detail">
          <span className="pull-left">{this.props.label}</span>
          <span className="pull-right">{this.props.children ? this.props.children : this.props.value}</span>
        </div>
      )
    }
    return (<div></div>)

  }
}

export class Display extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    if (this.props.show !== undefined && !this.props.show) {
      return <div></div>;
    }
    return <div className="template-part-detail">
      <span className="pull-left">{this.props.label}</span>
      <span className="pull-right">
                              {this.props.url ? (
                                <a target="_blank" href={ Urls.site + this.props.url }>View</a>
                              ) : 'Not uploaded'}
                            </span>
    </div>
  }
}
export class Panel extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return <div className="panel panel-default">
      <div className="panel-heading" role="tab" id={this.props.title.split(' ')[0]+'_tab'}>
        <h4 className="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href={'#' + this.props.title.split(' ')[0]}
             aria-expanded="false" aria-controls={this.props.title.split(' ')[0]}>
            {this.props.title}<i className="content pull-right"></i>
          </a>
        </h4>
      </div>
      <div id={this.props.title.split(' ')[0]} className="panel-collapse collapse" role="tabpanel"
           aria-labelledby={this.props.title.split(' ')[0]}>
        <div className="panel-body">
          <div className="col-xs-12 no-padding">
            {this.props.children}
          </div>
        </div>
      </div>
    </div>
  }
}