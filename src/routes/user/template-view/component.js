import React from 'react'
import { Link } from 'react-router'
import { Urls } from '../../../config/app'
import Loader from '../../../components/loader'
import Ajax from '../../../modules/request'
import AgeCalculator from '../../../components/form/agecalculator'
import { Display, Panel, View } from './structure'

export class Template extends React.Component {

  constructor (props) {
    super(props)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.state = {loaded: false}
  }

  componentDidMount () {
    Ajax().get(Urls.form.template).then((response) => {
      if (!response.data.data) {
        this.props.router.push('/form/edit')
        return
      }
      let data = {loaded: true}
      if (response.data.data) {
        data = response.data.data
        data.loaded = true
      }
      this.setState(data)
    })
  }

  isNepaleseNrn () {
    return this.state.nationality === 'nepalese' || this.state.nationality === 'nrn'
  }

  isNotNepaleseNrn () {
    return this.state.nationality === 'foreigner'
  }

  isNotMinor () {
    return AgeCalculator(this.state.date_of_birth_en) >= 16
  }

  isCountryIndia () {
    return this.state.country === 'India'
  }

  isNotCountryIndia () {
    return this.state.country !== 'India'
  }

  isRefugee () {
    return this.state.is_refugee === 'yes'
  }

  isNotRefugee () {
    return this.state.is_refugee === 'no'
  }

  render () {
    if (!this.state.loaded) {
      return <Loader/>;
    }
    let verificationStatus = ''
    if (this.state.verification_requested_at) {
      verificationStatus = (
        <div className="col-xs-12 no-padding" >
          <div className="alert alert-info">
            <span>Your profile is under verification process.</span>
          </div>
        </div>
      )
    } else {
      verificationStatus = (
        <div className="col-xs-12 no-padding" >
        {this.state.rejected_at && 
          <div className="alert alert-danger ">
            Your profile is not accepted yet. Please edit your profile and submit again.
          </div>
        }
          
        </div>
      )
    }
    return (
      <div className="card-wrapper">
        {verificationStatus}
        <div className="template-part card animated fadeIn">
          <div className="card-content customer-display">
            {!this.state.verified_at && !this.state.verification_requested_at && <div className="col-xs-2 pull-right" style={{marginTop:"10px",paddingRight:"15px"}}>
            <Link style={{padding:'10px'}} to='/form/edit' className="pull-right visible-xs  btn btn-round btn-primary" title="Edit Profile"><i className="material-icons">create</i></Link>
            <Link to='/form/edit' className="pull-right visible-sm visible-md visible-lg  btn btn-primary">Edit Profile</Link>
          </div>}
            <div className="clearfix">
              {this.state.verified_at && <div className="outer-tip-wrapper">
                <i title="Your profile has been accepted." className="fa fa-check "></i>
                <span className="custom-tool-tip">
                      accepted
                   </span>
              </div>
              }
            </div>
            <div className="clearfix"></div>
            <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <Panel title="Identity">
                
                <View label="Name" show={this.state.first_name} value={[this.state.first_name, this.state.middle_name, this.state.last_name].filter((filter) => {return filter && filter.length}).join(' ')}/>
                <View label="Gender" show={this.state.gender} value={this.state.gender}/>
                {this.state.date_of_birth_np && this.state.date_of_birth_en && <div>
                  <View label="Date of birth" value={`${this.state.date_of_birth_en} (A.D.) / ${this.state.date_of_birth_np} (B.S.)` }/>                
                </div> }
              </Panel>
              <Panel title="Nationality">                
                <View label="Nationality" show={this.state.nationality} value={this.state.nationality ? this.state.nationality.toUpperCase() : ''}/>
                
                {( this.isNepaleseNrn() && this.isNotMinor() ) &&  <div>

                  <div>
                    <View label="Citizenship Number" show={this.state.citizenship_number} value={this.state.citizenship_number ? this.state.citizenship_number : 'No'}/>
                    <View label="Citizenship Issued Date" show={this.state.citizenship_issued_on ? true : false} value={this.state.citizenship_issued_on ? this.state.ccitizenship_issued_on + ' (B.S.)' : ''}/>
                    <View label="Citizenship Issued District" show={this.state.citizenship_issued_district} value={this.state.citizenship_issued_district}/>
                    
                  </div>
                  
                  <View label="Voter ID Number" show={this.state.voters_id_number}  value={this.state.voters_id_number}/>
                  <View label="Individual PAN Number" show={this.state.pan_number} value={this.state.pan_number}/>
                  <View label="Passport?" show={this.state.has_passport} value={this.state.has_passport}/>
                  

                  {this.state.has_passport === 'yes'&& <div>
                    <View label="Passport Number" show={this.state.passport_number} value={this.state.passport_number}/>
                    <View label="Passport Issued Date" show={this.state.passport_issued_on} value={this.state.passport_issued_on}/>
                    <View label="Passport Expiry Date" show={this.state.passport_expiry_date} value={this.state.passport_expiry_date}/>
                    <View label="Passport Issued District" show={this.state.passport_issued_district} value={this.state.passport_issued_district}/>
                  </div>
                  }
                  <View label="Driving license?" show={this.state.has_driving_license} value={this.state.has_driving_license}/>
                  {this.state.has_driving_license === 'yes'&& <div>
                    <View label="Driving License Number" show={this.state.driving_license_number} value={this.state.driving_license_number}/>
                    <View label="Driving License Issued Date" show={this.state.driving_license_issued_on} value={this.state.driving_license_issued_on}/>
                    <View label="Driving License Expiry Date" show={this.state.driving_license_expiry_date} value={this.state.driving_license_expiry_date}/>                  
                  </div>}
                </div>}


                { this.isNotNepaleseNrn() && <div>
                  <View label="Country" show={this.state.country} value={this.state.country}/>
                  
                  { this.isNotCountryIndia() && <div>
                    <View label="Refugee?" show={this.state.is_refugee} value={this.state.is_refugee}/>
                    
                    {this.isRefugee() && <div>
                        <View label="Refugee ID Number" show={this.state.refugee_id_number} value={this.state.refugee_id_number}/>
                        <View label="Refugee ID Type" show={this.state.refugee_id_type} value={this.state.refugee_id_type}/>
                        <View label="Refugee ID Issued Date" show={this.state.refugee_id_issued_on} value={this.state.refugee_id_issued_on}/>
                        <View label="Refugee Issued Authority" show={this.state.refugee_issued_authority} value={this.state.refugee_issued_authority}/>
                      </div>}
                  </div> }

                  { ( this.isNotRefugee() || this.isCountryIndia() ) && <div>
                      <View label="Passport Number" show={this.state.foreign_passport_number} value={this.state.foreign_passport_number}/>
                      <View label="Passport Issued Date" show={this.state.foreign_passport_issued_on} value={this.state.foreign_passport_issued_on}/>
                      <View label="Passport Expiry Date" show={this.state.foreign_passport_expiry_date} value={this.state.foreign_passport_expiry_date}/>
                      <View label="VISA Expiry Date" show={this.state.foreign_visa_expiry_date} value={this.state.foreign_visa_expiry_date}/>
                      <View label="Valid Identification?" show={this.state.any_other_valid_identification} value={this.state.any_other_valid_identification}/>
                    </div>}

                  {((this.isNotCountryIndia() && this.isRefugee()) || ( this.isNotNepaleseNrn() && this.state.any_other_valid_identification === 'yes'  )) &&
                  <div>
                    <View label="ID Number" show={this.state.identification_number} value={this.state.identification_number}/>
                    <View label="ID Type" show={this.state.identification_number} value={this.state.identification_number}/>
                    <View label="Issuing Authority" show={this.state.identification_number} value={this.state.identification_number}/>
                    <View label="ID Issued Date" show={this.state.identification_number} value={this.state.identification_number}/>
                  </div>}
                </div>}
              </Panel>

              <Panel title="Address">
                <View label="District" show={this.state.current_district_name} value={this.state.current_district_name}/>
                <View label="Municipality" show={this.state.current_municipality_name} value={this.state.current_municipality_name}/>
                <View label="Ward Number" show={this.state.current_ward_number} value={this.state.current_ward_number}/>
                <View label="Town/City" show={this.state.current_town} value={this.state.current_town}/>
                <View label="Street" show={this.state.current_street} value={this.state.current_street}/>              
                <View label="House Number" show={this.state.current_house_number} value={this.state.current_house_number}/>

                <View label="Tenant in current residence?" show={this.state.is_tenant} value={this.state.is_tenant}/>
                {this.state.is_tenant && this.state.is_tenant === 'yes' && <div>
                  <View label="Land Lord's Full Name" show={this.state.landlord_name} value={this.state.landlord_name}/>
                  <View label="Landlord's Contact Number" show={this.state.landlord_contact_number} value={this.state.landlord_contact_number}/>
                  <View label="Landlord's Email" show={this.state.landlord_email} value={this.state.landlord_email}/>
                </div>}
                
                {this.isNepaleseNrn() && <div>

                  <View label="Is current address same as permanent address?" show={this.state.current_address_same_as_permanent} value={this.state.current_address_same_as_permanent} />
                  {this.state.current_address_same_as_permanent === 'no' && 
                  <div>
                    <div className="template-part-detail">
                      Permanent Address
                    </div>
                    <View label="District" show={this.state.permanent_district_name} value={this.state.permanent_district_name}/>
                    <View label="Municipality/VDC" show={this.state.permanent_municipality_name} value={this.state.permanent_municipality_name}/>
                    <View label="Ward Number" show={this.state.permanent_ward_number} value={this.state.permanent_ward_number}/>
                    <View label="Town/City" show={this.state.permanent_town} value={this.state.permanent_town}/>
                    <View label="Street/Tole" show={this.state.permanent_street} value={this.state.permanent_street}/>                  
                    <View label="House Number" show={this.state.permanent_house_number} value={this.state.permanent_house_number}/>
                  </div>}
                </div>}

                <View label="Is communication address different than current and permanent address?" show={this.state.current_address_same_as_communication} value={this.state.current_address_same_as_communication} />
                
                {this.state.current_address_same_as_communication === 'yes' &&  <div>
                  <div className="template-part-detail">
                    Communication Address
                  </div>
                  <View label="Country" show={this.state.communication_country} value={this.state.communication_country}/>
                  <View label="Town/City" show={this.state.communication_town} value={this.state.communication_town}/>
                  <View label="Street" show={this.state.communication_street} value={this.state.communication_street}/>
                  <View label="Apartment Number" show={this.state.communication_house_number} value={this.state.communication_house_number}/>
                  <View label="Email Address" show={this.state.communication_email} value={this.state.communication_email}/>
                  <View label="P.O.B. Number" show={this.state.communication_p_o_b_number} value={this.state.communication_p_o_b_number}/>
                </div>}
              </Panel>
              <Panel title="Occupation">
                <View label="Profession" show={this.state.profession} value={this.state.profession}/>              
                <View label="Employment" show={this.state.employment_type} value={this.state.employment_type}/>
                {this.state.employment_type === 'both' || this.state.employment_type === 'business' && 
                  <div>
                    <View label="Nature of Business" show={this.state.nature_of_business} value={this.state.nature_of_business}/>
                    <View label="Other business" show={ this.state.nature_of_business === 'other' } value={this.state.other_nature_of_business}/>
                    <View label="PAN/VAT  Number of Business" show={this.state.pan_number_business} value={this.state.pan_number_business}/>
                  </div>
                }
                {this.state.employment_type === 'other' && 
                    <View label="Other Employment" show={this.state.other_employment} value={this.state.other_employment}/>
                }

                <div className="template-part-detail">
                  <table className="table table-hover">
                    <tbody>
                    <tr>
                      <td>Organization Name</td>
                      <td>Address</td>
                      <td>Designation</td>
                      <td>Estimated Anual Income</td>
                    </tr>
                    {this.state.organization_name_1 && <tr>
                      <td>{this.state.organization_name_1}</td>
                      <td>{this.state.organization_address_1}</td>
                      <td>{this.state.organization_designation_1}</td>
                      <td>{this.state.organization_estimate_annual_income_1}</td>
                    </tr>}
                    {this.state.employment_type === 'both' && this.state.organization_name_2 && <tr>
                      <td>{this.state.organization_name_2}</td>
                      <td>{this.state.organization_address_2}</td>
                      <td>{this.state.organization_designation_2}</td>
                      <td>{this.state.organization_estimate_annual_income_2}</td>
                    </tr>}
                    {this.state.other_income_source && <tr>
                      <td>Other income source</td>
                      <td colSpan={2}>{this.state.other_income_source}</td>
                      <td>{this.state.other_estimated_annual_income}</td>
                    </tr>}
                    </tbody>
                  </table>
                </div>
              </Panel>
              <Panel title="Family Details">
                <View label="Marital status" value={this.state.marital_status}/>
                <View show={this.state.marital_status && this.state.marital_status.toLowerCase() === 'married'}
                      label="Spouse" value={this.state.spouse_name}/>
                <View label="Father" value={this.state.father_name}/>
                <View label="Mother" value={this.state.mother_name}/>
                <View label="Grandfather" value={this.state.grand_father_name}/>
                <View label="Grandmother" value={this.state.grand_mother_name}/>
                <View label="Name of son's"
                      value={[this.state.son_name_1, this.state.son_name_2, this.state.son_name_3].filter(Boolean).join(', ')}/>
                <View label="Name of daughter's"
                      value={[this.state.daughter_name_1, this.state.daughter_name_2, this.state.daughter_name_3].filter(Boolean).join(', ')}/>
                <View show={this.state.gender === 'female' && this.state.marital_status === 'married'}
                      label="Father in Law" value={this.state.father_in_law}/>
                <View show={this.state.gender === 'female' && this.state.marital_status === 'married'}
                      label="Mother in Law" value={this.state.mother_in_law}/>
              </Panel>
              <Panel title="Miscellaneous">
                <View label="Religion" value={this.state.religion}/>
                {this.state.religion === 'other' &&
                  <View label="Other Religion" value={this.state.other_religion}/>
                }
                <View label="Education" value={this.state.education}/>
              </Panel>
              <Panel title="Documents">
                <Display show={this.state.nationality === 'nepalese' || this.state.nationality === 'nrn'} label="Photo"
                         url={this.state.photo_person}/>
                <Display
                  show={(this.state.nationality === 'nepalese' || this.state.nationality === 'nrn') && this.state.has_citizenship}
                  label="Citizenship"
                  url={this.state.photo_citizenship}/>
                <Display show={this.state.nationality === 'nepalese' || this.state.nationality === 'nrn'}
                         label="Utility Bill" url={this.state.photo_utility_bill}/>
                <Display
                  show={(this.state.nationality === 'nepalese' || this.state.nationality === 'nrn') && this.state.photo_driving_liscence}
                  label="Driving License"
                  url={this.state.photo_driving_liscence}/>
                <Display
                  show={ (this.state.nationality === 'nepalese' || this.state.nationality === 'nrn' ) && this.state.has_voters_id}
                  label="Voters Identification"
                  url={this.state.photo_voter_id}/>
                <Display
                  show={this.state.nationality === 'foreigner' && this.state.country !== 'India' && this.state.is_refugee === 'no'}
                  label="Photo" url={this.state.foreigner_photo_person}/>
                <Display
                  show={this.state.nationality === 'foreigner' && this.state.country !== 'India' && this.state.is_refugee === 'no'}
                  label="Passport" url={this.state.foreigner_photo_passport}/>
                <Display
                  show={this.state.nationality === 'foreigner' && this.state.country !== 'India' && this.state.is_refugee === 'no'}
                  label="Visa" url={this.state.foreigner_photo_visa}/>
                <Display
                  show={this.state.nationality === 'foreigner' && this.state.country !== 'India' && this.state.is_refugee === 'no'}
                  label="Identification" url={this.state.foreigner_photo_id_card}/>

                <Display show={this.state.country === 'India'} label="Photo" url={this.state.indian_photo_person}/>
                <Display show={this.state.country === 'India'} label="Identification"
                         url={this.state.indian_photo_id_card}/>
                <Display show={this.state.country === 'India'} label="Identification Photo"
                         url={this.state.indian_photo_valid_identification}/>
                <Display show={this.state.country === 'India'} label="Additional Document"
                         url={this.state.indian_photo_any_other_documents}/>
                <Display label="Signature" url={this.state.client_signature}/>
                <Display show={!this.state.client_signature} label="Right Thumb"
                         url={this.state.client_right_thumb_image}/>
                <Display show={!this.state.client_signature} label="Left Thumb" url={this.state.client_left_thumb_image}/>
              </Panel>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Template
