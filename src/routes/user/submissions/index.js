import {connect} from 'react-redux'
export default (store) => ({
    path: 'submissions',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
          cb(null, connect((state)=>state, {})(require('./component').default));
        }, 'submissions');
    }
})
