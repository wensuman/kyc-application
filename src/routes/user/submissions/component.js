import React from 'react'
import { Urls } from '../../../config/app'
import Ajax from '../../../modules/request'
import Select from 'react-select'
import 'react-select/dist/react-select.css'
import Form from '../../../components/form'
import Loader from '../../../components/loader'

export class Submissions extends React.Component {
  constructor (props) {
    super(props)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.state = {
      template: '',
      bank_id: '',
      other_bank_details: [],
      banks: [],
      bank: '',
      history: [],
      number_of_banks: 0,
      original_template: '',
      availing_credit_card: 'no',
      is_history: false,
      submitted_banks: [],
      accepted_by: [],
      rejected_by: [],
      loaded: false,
    }
    this.loadHistory = this.loadHistory.bind(this)
    this.bankClass = this.bankClass.bind(this)
    this.sendForm = this.sendForm.bind(this)
    this.append = this.append.bind(this)
    this.prepareForm = this.prepareForm.bind(this)
    this.submitForBank = this.submitForBank.bind(this)
    this.updateTemplate = this.updateTemplate.bind(this)
  }

  updateTemplate (template) {
    this.setState({template: template})
  }

  componentDidMount () {
    jQuery && jQuery(document).on('keyup', '.bank-div input', function () {
      jQuery(this).removeAttr('style')
    })
    Ajax().get(Urls.form.template).then(response => {
      response.data.data && this.setState({template: response.data.data, original_template: response.data.data})
    })
    Ajax().get(Urls.banks).then(( response => {
      response.data && response.data.data && this.setState({banks: response.data.data})
      this.loadHistory()
    }))

  }

  loadHistory () {
    Ajax().get(Urls.form.history).then(( response => {
      if (response.data) {
        this.setState({history: response.data.data})
        let histor = [], rejected = [], accepted = []
        response.data.data.map((history) => {
          histor.push(history.bank_id)
          history.rejected_at ? rejected.push(history.bank_id) : ''
          history.verified_at ? accepted.push(history.bank_id) : ''
        })
        this.setState({submitted_banks: histor, rejected_by: rejected, accepted_by: accepted, loaded: true})
      }
    }))
  }

  bank_details () {
    let bold = []
    jQuery('.bank-div').map(function () {
      let hold = {}
      jQuery(this).find('input').map(function (map) {
        hold[jQuery(this).attr('name')] = jQuery(this).val()
        return map
      })
      bold.push(hold)
    })
    return bold
  }

  setHistoryIfAvailable (bank) {
    let hold = this.state.history.filter(function (history) {
      return history.bank_id === bank.id
    })[0];
    jQuery('.bank-div').remove();
    if (hold) {
      this.setState(hold)
      if (typeof hold.accounts === 'object') {
        hold.accounts.map(this.append)
      }
      this.setState({is_history: true})
    } else {
      this.setState({
        bank_id: bank.id,
        bank_name: bank.profile.name,
        template: this.state.original_template,
        relationship_with_account_holder: '',
        availing_card_type: '',
        account_number: '',
        availing_credit_card: 'no',
        is_history: false,
        reason: ''
      })
    }
  }

  append (map) {
    setTimeout(() => {
      let id = ''
      if (map) {
        id = `some_${Math.floor(Math.random() * 1000)}`
      }
      jQuery('.other_banks').append(`<tr class="bank-div" id="${id}">${jQuery('.temp').html()}`)
      if (map) {
        let jQuery2 = jQuery(`#${id}`)
        jQuery2.find('[name=name]').val(map.name)
        jQuery2.find('[name=account_type]').val(map.account_type)
        jQuery2.find('[name=branch]').val(map.branch)
      }
    }, 100)
  }

  submitForBank () {
    let message = []
    if (!this.state.account_number) {
      jQuery('.account_number_name').parent().addClass('has-error')
      message.push('Please enter the account number associated with this bank.')
    }
    if (!this.state.relationship_with_account_holder) {
      jQuery('.relationship').addClass('has-error')
      message.push('Please select your relationship with the account holder.')
    }
    if (this.state.relationship_with_account_holder === 'Other' && !this.state.relationship_with_account_holder_other) {
      jQuery('.relationship').find('.form-group').addClass('has-error')
      message.push('Please mention the your relationship with account holder.')
    }
    if (!this.state.availing_credit_card) {
      jQuery('.credit-card-information').addClass('has-error')
      message.push('Please mention if your account is associated with any card.')
    }
    if (this.state.availing_credit_card && this.state.availing_credit_card === 'Yes' && !this.state.availing_card_type) {
      jQuery('.credit-card-information').addClass('has-error')
      message.push('Please select card type')
    }
    if (jQuery('.bank-div').find('[name]').filter(function () {
        let b = jQuery(this).val() === ''
        if (b) jQuery(this).css('border-bottom', 'red 2px solid')
        return b
      }).length > 0) {
      message.push('Please fill up the other bank detail fields property')
    }
    if (!this.state.terms_and_condition_accepted) {
      message.push('Please agree with terms and condition.')
    }
    if (message.length > 0) {
      alert(message[0], 'error')
      return
    }
    let data = {
      template: Object.assign({}, this.state.template, {activeStep: 1, change: false}),
      accounts: this.bank_details(),
      bank_id: this.state.bank_id,
      relationship_with_account_holder: this.state.relationship_with_account_holder,
      availing_card_type: this.state.availing_card_type,
      availing_credit_card: this.state.availing_credit_card,
      terms_and_condition_accepted: this.state.terms_and_condition_accepted,
      account_number: this.state.account_number,
    }
    this.setState({submitting: true})
    Ajax().post(`${Urls.site}api/submission/`, {
      data: data,
      bank_id: this.state.bank_id
    }).then((response) => {
      this.setState({submitting: false})
      if (response.response && response.response.status === 400) {
        alert('You have already submitted your profile to this bank.', 'error')
        return
      }
      if (response.status === 201) {
        alert(`Your form is submitted to the bank.`)
        this.loadHistory()
      }
    })
  }

  change (name, event) {
    this.setState({[name]: event.target.value})
  }

  bankClass (id) {
    let classy = ['column-custom']
    this.state.submitted_banks.includes(id) ? classy.push('already-submitted') : ''
    this.state.rejected_by.includes(id) ? classy.push('rejected') : ''
    this.state.accepted_by.includes(id) ? classy.push('accepted') : ''
    this.state.bank_id === id ? classy.push('current') : ''
    return classy.join(' ')
  }

  prepareForm () {
    if (this.state.accepted_by.includes(this.state.bank_id)) {
      return <div className="alert alert-success green-state">
        Your form has already been accepted.
      </div>
    } else if (this.state.rejected_by.includes(this.state.bank_id)) {
      return this.sendForm('Your form was declined.')
    } else if (this.state.submitted_banks.includes(this.state.bank_id)) {
      return <div className="alert alert-info">Your form is under verification process. The bank will notify you after it is verified.</div>
    } else {
      return this.sendForm()
    }
  }

  sendForm () {
    return (
      <div className="submission-bank-content clearfix">
        <div className="col-sm-12 no-padding">
          {this.state.bank_name && <h4 className="header-bank-name">{this.state.bank_name}</h4>}
        </div>
        <div className="col-sm-12 no-padding">
          {this.state.reason && <div className="alert alert-danger"><span> Message: {this.state.reason}</span></div> }
        </div>
        <div className="col-sm-12 no-padding">
          <div className="account_number col-sm-6 no-padding">
            <div className="form-group">
              <h4>Enter your account number *</h4>
              <input value={this.state.account_number} maxLength={20} className="form-control account_number_name"
                     onChange={this.change.bind(this, 'account_number')}/>
            </div>
          </div>
          <div className="relationship col-sm-6 p-r-0">
            <h4>
              Relationship with account holder *:</h4>
            <Select searchable={false}
                    onChange={(value) => {
                      jQuery('.relationship').removeClass('has-error')
                      this.setState({relationship_with_account_holder: value.value})
                    }}
                    options={[{ label: 'I am an account holder (Individual/Joint)', value: 'I am an account holder (Individual/Joint)'},
                      {label: 'I am an account operator', value: 'I am an account operator'},
                      {label: 'I am shareholder', value: 'I am shareholder'},
                      {label: 'I am the proprietor', value: 'I am the proprietor'},
                      {
                        label: 'I am a member of board of directors or working committee',
                        value: 'I am a member of board of directors or working committee'
                      },
                      {label: 'Executive chief', value: 'Executive chief'},
                      {label: 'Other', value: 'Other'}]}
                    value={this.state.relationship_with_account_holder}/>
          </div>
         
          {
            this.state.relationship_with_account_holder === 'Other' && 
            <div className='form-group col-sm-12'>
            <input placeholder="Describe your relationship with account holder" type="text"
                   className="form-control relationship_with_account_holder" maxLength={30}
                   value={this.state.relationship_with_account_holder_other}
                   onChange={(event) => {this.setState({relationship_with_account_holder_other: event.target.value})}}/>
            </div>
          }
          <div style={{ 'clear':'both'}}></div>
          <div className="credit-card-information">
            <div className="col-sm-2 no-padding">
              <h4>Availing Cards</h4>
            </div>
              <div className="form-fields-radio col-sm-10">
                <div className="col-sm-3">
                  <label><input type="radio" onChange={(event) => { }} checked={this.state.availing_card_type === 'Debit'}
                  onClick={() => { this.setState({ availing_card_type: 'Debit' }) }} />Debit</label>
                </div>
                <div className="col-sm-3">
                  <label><input type="radio" onChange={(event) => { }} checked={this.state.availing_card_type === 'Credit'}
                  onClick={() => { this.setState({ availing_card_type: 'Credit' }) }} />Credit</label>
                </div>
                <div className="col-sm-3">
                  <label><input type="radio" onChange={(event) => { }} checked={this.state.availing_card_type === 'Both'}
                    onClick={() => { this.setState({ availing_card_type: 'Both' }) }} />Both</label>
                </div>
              </div>
            {this.state.availing_credit_card === 'yes' && <div className="card-info-label">
              <label>Card Type</label>
              <label>
                <input type="radio" checked={this.state.availing_card_type === 'Master'}
                       onClick={() => {this.setState({availing_card_type: 'Master'})}}/>Master
              </label>
              <label>
                <input type="radio" checked={this.state.availing_card_type === 'Visa'}
                       onClick={() => {this.setState({availing_card_type: 'Visa'})}}/> Visa
              </label>
              <label>
                <input type="radio" checked={this.state.availing_card_type === 'Debit'}
                       onClick={() => {this.setState({availing_card_type: 'Debit'})}}/>Debit
              </label>
            </div>}
          </div>
          <div className="dealing-with-any-other-bank clearfix">
            <h4>Dealing with any other banks?</h4>
            <div className="table-responsive">
              <table className="table table-bordered">
                <thead>
                <tr>
                  <td>Name of bank</td>
                  <td>Account type</td>
                  <td>Branch</td>
                  <td>Action</td>
                </tr>
                </thead>
                <tbody className="other_banks">
                <tr className="temp" style={{display: 'none'}}>
                  <td><input required maxLength={40} name="name" type="text"/>
                  </td>
                  <td><input required maxLength={20} name="account_type" type="text"/>
                  </td>
                  <td><input required maxLength={20} name="branch" type="text"/>
                  </td>
                  <td style={{textAlign: 'center'}}>
                    <button className="btn btn-danger trasher"><i className="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
            <button title="Add new bank" className="btn btn-primary" onClick={this.append}>
              +
            </button>
          </div>
          {this.state.is_history && <Form change={this.updateTemplate}
                                          template={Object.assign({voters_data:{}}, this.state.template, {
                                            activeStep: 1,
                                            change: false
                                          })}></Form>}
          <div className="confirmation-check">
            <label>
              <input type="checkbox" checked={this.state.terms_and_condition_accepted}
                     onClick={() => {this.setState({terms_and_condition_accepted: !this.state.terms_and_condition_accepted})}}
                     className=""/> I hereby declare that the information furnished hereinabove is
              complete, correct and true to the best of my knowledge and belief. I
              authorize {this.state.bank_name} to make any enquiries regarding the information declared
              hereinabove.
            </label>
          </div>
          <button disabled={this.state.submitting} className="btn btn-primary submit-submission clearfix" onClick={this.submitForBank}>
            {this.state.submitting ? <i className="fa fa-spin fa-spinner"></i> : 'Submit'}
          </button>

        </div>
      </div>
    )
  }

  render () {
    if (!this.state.loaded) {
      return <Loader/>
    }
    if (!this.state.template || ( this.state.original_template && !this.state.original_template.verified_at)) {
      return  <div className="card-wrapper">
        <div className="col-xs-12 no-padding">            
            <div className="alert alert-info note-state clearfix">
              <p>Your form is under verification process. You will be notified and will be able to submit your form after it has been verified by our admin.</p>
            </div>
        </div>
      </div>
    }
    return (
      <div className="card-wrapper">
        <div className="card">
          <div className="card-content">
            <div className="bank-submission-lists transition-item animated fadeIn">
              <div className="row banks">
                {this.state.banks.filter((bank) => {return bank.profile && bank.profile.name}).map((bank, key) => (
                  <div title={bank.profile.name} className={this.bankClass(bank.id)} key={bank.id}>
                    <div className="bank-label-wrapper">
                      <label>
                        <div className="bank-profile">
                          { bank.profile && bank.profile.pic ? <img src={Urls.bank + bank.profile.pic} alt={bank.profile.name} width="100"/> :
                            <img src="/assets/images/image_placeholder.jpg" width="100" alt={bank.profile.name}/> }
                          <label className="radio-label">
                            <input type="radio" name="bank_name" className="option-input radio"
                                   onClick={this.setHistoryIfAvailable.bind(this, bank)}/>
                          </label>
                            {this.state.accepted_by.includes(bank.id) ? <span className="radio-tool-tip">
                                <span>Status: Accepted</span> </span>: (this.state.rejected_by.includes(bank.id) ?
                                <span className="radio-tool-tip"> <span>Status: Rejected</span> </span> : (
                                this.state.submitted_banks.includes(bank.id) ? <span className="radio-tool-tip"><span>Status: Pending</span> </span> : <span className="radio-tool-tip"><span>{bank.profile.name}</span> </span>
                                ))}
                        </div>
                      </label>
                    </div>
                  </div>
                ))}
              </div>
              {this.state.bank_id ? this.prepareForm() :
                <div className="alert alert-success note-state clearfix">Select a bank</div>}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Submissions
