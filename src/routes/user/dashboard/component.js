import React from 'react'
import { Link } from 'react-router'
import { Urls } from '../../../config/app'
import Ajax from '../../../modules/request'

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pic: '',
      logs: [],
      history: [],
      loaded: false
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.name = this.name.bind(this)
    this.displayAddress = this.displayAddress.bind(this)
  }

  componentDidMount() {
    Ajax().get(Urls.form.template).then((response) => {
      this.setState({ loaded: true }), response.data.data && this.setState(response.data.data)
    })
    Ajax().get(Urls.profile).then((response) => { response.data.data && this.setState(response.data.data) })
    jQuery('.main-panel').addClass('digital-id-page') 
  }

  componentWillUnmount() {
    jQuery('.main-panel').removeClass('digital-id-page')
  }

  displayAddress(){
    let address = [this.state.current_town, this.state.current_ward_no].filter(Boolean).join(' ');
    address = [address, this.state.current_municipality_name].filter(Boolean).join(', ');
    address = address.length > 0 ? <div className="label-floating">
        <span className='form-control profile-name'><span className="profile-label">Address </span> <span> : </span>
          {address}
        </span>
      </div> : '';
    return <div>
      {address}
      {this.state.current_district_name && <div className="label-floating">
        <span className='form-control profile-name'><span className="profile-label">District </span> <span> : </span>
          {this.state.current_district_name}
        </span>
      </div>}
    </div>;
  }

  static attribute(history, index) {
    return (
      <tr key={history.id}>
        <td>{history.id}</td>
        <td>{history.bank_name}</td>
        <td>{history.created_at}</td>
      </tr>
    )
  }

  name() {
    return [this.state.first_name, this.state.middle_name, this.state.last_name].filter(Boolean).join(' ')
  }
  render() {
    if (!this.state.loaded) {
      return <loader />
    }
    let hasNoProfile = (
      <div className="profile-wrapper">
        <div className="col-sm-12 text-center">
          <div className="userProfileImage">
            <Link className="btn btn-primary" to="/form/edit">Create Profile</Link>
          </div>
        </div>
      </div>
    )
    let hasProfile = (
      <div className="profile-wrapper digital-profile">
        <div className="profile-name-wrap text-center">
        </div>
        <div className="digital col-left">
          <div className="label-floating ">
            <span className="form-control profile-name"><span className="profile-label">ID No. </span><span> : </span> {this.state.hashed_id}</span>
          </div>
          <div className='label-floating'>
            <span className="form-control profile-name">
              <span className="profile-label">Name </span> <span> : </span> {this.name()}</span>
          </div>
         {this.displayAddress()}
          <div className='label-floating'>
            {this.state.date_of_birth_en && <span
              className="form-control profile-name profile-dob"> <span className="profile-label">Date of Birth</span> <span> : </span>

              <span className="date-of-birth"> {this.state.date_of_birth_en} (A.D.) <br />
                <span>{this.state.date_of_birth_np} (B.S.)</span></span>
            </span>
            }

          </div>
          {this.state.phone_number && <div className="label-floating">
            <span className="form-control profile-phone">
              <span className="profile-label">Phone No. </span> <span> : </span>
              {this.state.phone_number}
            </span>
          </div>}
        </div>
        <div className="digital col-right">
          <div className="userProfileImage">
            {this.state.photo_person
              ? <img src={Urls.site + this.state.photo_person} alt="Your photo" />
              : <img src='/profil-pic_dummy.png' alt="Your photo" />}
          </div>
          <div className="userprofileSign">
            {this.state.client_signature && <img src={Urls.site + this.state.client_signature} alt="Your signature" />}
          </div>
        </div>
      </div>
    )
    return (
      <div className="transition-item animated fadeIn">
        <div className="col-sm-12">
          <div className="row">
            <div className=" col-xs-12 col-sm-12 col-md-12">
              <form>
                <div className="row">
                  <div className="col-md-12">
                    {this.state.first_name && this.state.last_name ? hasProfile : hasNoProfile}
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
