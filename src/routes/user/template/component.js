import React from 'react'
import Form from '../../../components/form'
import { Urls } from '../../../config/app'
import Ajax from '../../../modules/request'
import Loader from '../../../components/loader'
import DefaultProperties from '../../../components/form/default'

export class Template extends React.Component {

  constructor (props) {
    super(props)
    this.state = {loaded: false, template: {}}
    this.componentDidMount = this.componentDidMount.bind(this)
    this.verify = this.verify.bind(this)

  }

  componentDidMount () {
    Ajax().get(Urls.form.template).then((response) => {
      if (response.data.data && (response.data.data.verified_at || response.data.data.verification_requested_at)) {
        this.props.router.push('/form')
        return
      }
      let data = {loaded: true, template: {}}
      data.template = DefaultProperties
      if (response.data.data) {
        data.template = Object.assign({}, data.template, response.data.data)
      }
      data.template.activeStep = 1
      data.template.change = false
      data.template.uploading = false
      this.setState(data)
    })
  }

  verify (data) {

    Ajax().post(Urls.form.template, {data: data, is_verify_request: true}).then((response) => {
      if (response.status === 201 || response.status === 202) {
        alert('Your profile has been submitted for verification successfully.')
        this.props.router.push('/form')
        return
      }
      alert('We are currently experiencing technical difficulties. Please try again later.', 'error')
    })
  }

  render () {
    if (!this.state.loaded) {
      return <Loader/>
    }
    return <Form step={this.props.router.params.step ? this.props.router.params.step: 1} change={(state) => {Ajax().post(Urls.form.template, {data: state})}} template={this.state.template}
                 action={this.verify}/>
  }
}

export default Template
