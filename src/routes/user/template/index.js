import {connect} from 'react-redux'
export default (store) => ({
    path: 'form/edit',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            cb(null, connect((state)=>state, {})(require('./component').default));
        }, 'form');
    }
})
