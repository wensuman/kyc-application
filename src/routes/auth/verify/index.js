import {injectReducer} from '../../../store/reducers'
export default (store) => ({
    path: 'verify',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const container = require('./container').default;
            cb(null, container);
        }, 'verify');
    }
})
