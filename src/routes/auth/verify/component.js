import React from 'react'
import Ajax from '../../../modules/request'
import { Urls } from '../../../config/app'
import { Link } from 'react-router'

export default class VerifyCode extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      phone_number: this.props.location.query.email,
      verification_code: '',
    }
    this.change = this.change.bind(this)
    this.verified = this.verified.bind(this)
    this.verifyCode = this.verifyCode.bind(this)
  }

  change (name, event) {
    if (event.target.type === 'checkbox') {
      this.state[name] === 'on'
        ? event.target.value = ''
        : event.target.value = 'on'
    }
    this.setState({[name]: event.target.value})
  }

  verifyCode (event) {
    event.preventDefault()
    if (!this.state.phone_number) {
      this.setState({error_msg: 'Enter either mobile number or email you used to register with our website.'})
      return
    }
    if (!this.state.verification_code) {
      this.setState({error_msg: 'Please enter verification code.'})
      return
    }
    this.setState({verifying: true, register_password: '', register_email: '', register_password_confirmation: ''})
    Ajax().post(Urls.verify, {
      phone_number: this.state.phone_number,
      verification_code: this.state.verification_code
    }).then(this.verified)
  }

  verified (response) {
    this.setState({verifying: false})
    if (response.status === 201) {
      alert('Account verified successfully. Please, login')
      this.props.router.push('/login')
      return
    }
    if (response.response && response.response.data) {
      let responses = response.response.data.errors
      for (let i in responses) {
        this.setState({error_msg: responses[i][0]})
        return
      }
    }
    this.setState({error_msg: 'Verification process failed. Please check your verification code.'})
  }

  render () {
    return <form method="post" className=" animated fadeIn">
      {this.state.error_msg  && <p className="alert-msg">{this.state.error_msg}</p>}
      <h4 className="verify_fields__user header-login">
        Enter the 4 digit code</h4>
      <div className="verify_fields__user input-field">
        <div className="icon">
          <i className="fa fa-user"></i>
        </div>
        <input required maxLength={50} onChange={this.change.bind(this, 'phone_number')}
               placeholder="Mobile number or Email" value={this.state.phone_number}/>
      </div>
      <div className="verify_fields__user input-field">
        <div className="icon">
          <i className="fa fa-lock"></i>
        </div>
        <input autoFocus maxLength={10} required placeholder="Verification code"
               onChange={this.change.bind(this, 'verification_code')}
               value={this.state.verification_code} type="password"/>
      </div>
      <button type="submit" className="kyc-btn verify-btn verify_fields__user"
              onClick={this.verifyCode}>
        {this.state.verifying ? <i className="fa fa-spin fa-spinner"></i> : 'Verify'}
      </button>
      <div className="login-bottom-wrapper">
        <div className="new-create-account">
          <Link to="/login">
            Sign In
          </Link>
        </div>
        <span>or</span>
        <div className="new-password-generate">
          <Link to="/forgot-password">
            Forgot Password?
          </Link>
        </div>
      </div>
    </form>
  }
}
