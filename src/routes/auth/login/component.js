import React from 'react'
import Ajax from '../../../modules/request'
import { Urls } from '../../../config/app'
import Cookie from 'react-cookie'
import { Link } from 'react-router'
import Operation from './module.js'
import Recaptcha from '../../../components/recaptcha'
export default class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      phone_number: '',
      remember_me: 'on',
      password: '',
      login: '',
      ajaxHappening: '',
      initial: true,
      error_msg: ''
    }
    this.captcha = ''
    this.change = this.change.bind(this)
    this.login = this.login.bind(this)
  }

  change (name, event) {
    if (event.target.type === 'checkbox') {
      this.state[name] === 'on'
        ? event.target.value = ''
        : event.target.value = 'on'
    }
    this.setState({[name]: event.target.value})

  }

  login (event) {
    event && event.preventDefault && event.preventDefault()
    if (!this.state.phone_number) {
      this.setState({error_msg: 'Please enter your mobile number or email address.'})
      return false
    }
    if (!this.state.password) {
      this.setState({error_msg: 'Please enter your password.'})
      return
    }
    if (!this.state.is_verified && window.recaptcha && window.recaptcha.execute) {
      window.recaptcha.execute()
      return
    }
    this.setState({logging: true})
    Operation()
    Ajax().post(Urls.login, {phone_number: this.state.phone_number, password: this.state.password}).then((response) => {
      this.setState({error_msg: 'Login was not recognized by our system.'})
      if (response.status === 202) {
        Cookie.save('Authorization', `Bearer ${response.data.data.token}`, {maxAge: 7200})
        this.props.router.push('/')
        return
      }
      this.setState({logging: false})
      if (response.response && response.response.data) {
        let responses = response.response.data.errors
        for (let i in responses) {
          this.setState({error_msg: responses[i][0]})
          return
        }
      }
    }).catch((error) => {
      this.setState({error_msg: 'Network error.'})
      this.setState({logging: false})
    })
  }

  render () {
    return <form method="post" className=" animated fadeIn">
      {this.state.error_msg  && <p className="alert-msg">{this.state.error_msg}</p>}
      <div className={(1 < this.state.phone_number.length && this.state.phone_number.length < 10)
        ? ' login_fields__user input-field alert-msg' : 'login_fields__user input-field'}>
        <div className="icon">
          <i className="fa fa-user"></i>
        </div>
        <input maxLength={50} onChange={this.change.bind(this, 'phone_number')}
               value={this.state.phone_number}
               type="text" placeholder="Mobile Number or Email"/>
      </div>
      <div className="login_fields__user input-field">
        <div className="icon">
          <i className="fa fa-lock"></i>
        </div>
        <input onChange={this.change.bind(this, 'password')} value={this.state.password} type="password"
               placeholder="Password"/>
        <div className="forgot login_fields__user">
          <Link to="/forgot-password">Forgot password</Link>
        </div>
      </div>
      <div className="signup_fields__user input-field">
        <Recaptcha onSolved={() => {
          this.setState({is_verified: true})
          this.login()
        }}/>
      </div>
      <button type="submit" className="kyc-btn login-btn login_fields__user "
              onClick={this.login}>
        {this.state.logging ? <i className="fa fa-spin fa-spinner"></i> : 'Sign In'}
      </button>
      <div className="login-bottom-wrapper">
        <div className="new-create-account">
          Don't have an account?
          <Link to="/sign-up">
            Sign Up
          </Link>
        </div>
      </div>
    </form>
  }
}
