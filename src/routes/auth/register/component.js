import React from 'react'
import Ajax from '../../../modules/request'
import { Urls } from '../../../config/app'
import { Link } from 'react-router'
import IsPhone from '../../../modules/phone'
import IsEmail from '../../../modules/email'
import Recaptcha from '../../../components/recaptcha'

export default class Register extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      recaptcha: false,
      registering: false,
      register_phone_number: '',
      register_password: '',
      register_email: '',
      register_password_confirmation: '',
      email: '',
      ajaxHappening: '',
      register_agree: false,
      initial: true,
      error_msg: ''
    }
    this.change = this.change.bind(this)
    this.register = this.register.bind(this)
    this.verify = this.verify.bind(this)
  }

  change (name, event) {
    if (event.target.type === 'checkbox') {
      this.state[name] === 'on'
        ? event.target.value = ''
        : event.target.value = 'on'
    }
    let value = event.target.value
    if (name === 'register_phone_number' && isNaN(value)) {
    } else
      this.setState({[name]: value})
  }

  register (event) {
    event && event.preventDefault()
    if (this.state.registering) {
      return
    }

    if (!this.state.register_phone_number && !this.state.register_email) {
      this.setState({error_msg: 'Please enter the required information before signing up.'})
      return
    }

    if (this.state.register_phone_number.length > 0) {
      if (!IsPhone(this.state.register_phone_number)) {
        this.setState({error_msg: 'Please enter a valid mobile number of 10 digits.'})
        return
      }
    }

    if (this.state.register_email.length > 0) {
      if (!IsEmail(this.state.register_email)) {
        this.setState({error_msg: 'Please enter valid email address.'})
        return
      }
    }

    if (!this.state.register_password || this.state.register_password.length < 8) {
      this.setState({error_msg: 'Please enter a strong password of atleast 8 character.'})
      return
    }

    if (this.state.register_password !== this.state.register_password_confirmation) {
      this.setState({error_msg: 'Passwords do not match.'})
      return
    }

    if (!this.state.register_agree) {
      this.setState({error_msg: 'Please accept our terms and conditions.'})
      return
    }

    if (!this.state.is_verified && window.recaptcha && window.recaptcha.execute) {
      window.recaptcha.execute()
      return
    }

    this.setState({registering: true})
    let obj = {}
    this.state.register_phone_number ? obj.phone_number = this.state.register_phone_number : ''
    this.state.register_email ? obj.email = this.state.register_email : ''
    obj.password = this.state.register_password
    obj.password_confirmation = this.state.register_password_confirmation
    obj.register_agree = 1
    Ajax().post(Urls.register, obj).then(this.verify, () => {
      this.setState({registering: false})
    })
  }

  verify (response) {
    this.setState({registering: false})
    if (response.status === 201) {
      let via = []
      if (this.state.register_email) {
        via.push('email')
      }
      if (this.state.register_phone_number) {
        via.push('SMS')
      }
      alert(`Registration was successful. We have sent the verification code through ${via.join(' and ')}. Please enter your verification code.`)
      this.props.router.push('/verify')
      return
    }
    if (response.response && response.response.data) {
      let responses = response.response.data.errors
      for (let i in responses) {
        this.setState({error_msg: responses[i][0]})
        return
      }
    }
  }

  render () {
    return <form method="post" className="animated fadeIn">
      {this.state.error_msg && <p className="alert-msg">{this.state.error_msg}</p>}
      <div className="signup_fields__user input-field">
        <div className="icon">
          <i className="ace-icon fa fa-mobile"></i>
        </div>
        <input
          className={this.state.register_phone_number && (this.state.register_phone_number.length !== 10 ) ? 'login-error-exists' : ''}
          onChange={this.change.bind(this, 'register_phone_number')}
          value={this.state.register_phone_number} type="text" maxLength={10}
          placeholder="Mobile Number"/>
        {this.state.register_phone_number && isNaN(this.state.register_phone_number) &&
        <span className="with-error">Only numbers are allowed.</span>}
      </div>
      <div className="signup_fields__user input-field">
        <div className="icon">
          <i className="ace-icon fa fa-envelope"></i>
        </div>
        <input
          className={this.state.register_email && ( !IsEmail(this.state.register_email) ) ? 'login-error-exists' : ''}
          onChange={this.change.bind(this, 'register_email')}
          value={this.state.register_email} maxLength={50} placeholder="Email Address"/>
      </div>
      <div className="signup_fields__user input-field">
        <div className="icon">
          <i className="fa fa-lock"></i>
        </div>
        <input
          className={this.state.register_password && ( this.state.register_password.length < 8 ) ? 'login-error-exists' : ''}
          onChange={this.change.bind(this, 'register_password')} value={this.state.register_password}
          type="password" placeholder="Password"/>
      </div>
      <div className='signup_fields__user input-field'>
        <div className="icon">
          <i className="fa fa-retweet"></i>
        </div>
        <input
          className={this.state.register_password_confirmation && ( this.state.register_password_confirmation !== this.state.register_password ) ? 'login-error-exists' : ''}
          onChange={this.change.bind(this, 'register_password_confirmation')}
          value={this.state.register_password_confirmation} type="password"
          placeholder="Repeat password"/>
      </div>
      <div className="field signup_fields__user">
        <input type="checkbox" id="checkbox1" checked={this.state.register_agree === 'on'}
               onChange={this.change.bind(this, 'register_agree')}/>
        <label htmlFor="checkbox1">
          <span className="ui"></span>I accept the <a
          href='https://kycnepal.com.np/terms-and-condition/'>User
          Agreement</a>
        </label>
      </div>
      <div className="signup_fields__user input-field">
        <Recaptcha onSolved={() => {
          this.setState({is_verified: true})
          this.register()
        }}/>
        <button type="submit" onClick={ this.register }
                className='kyc-btn signup-btn signup_fields__user'>
          {this.state.registering ? <i className="fa fa-spin fa-spinner"></i> : 'Sign Up'}
        </button>
        <div style={this.state.error_msg ? {marginTop: 0} : {}} className="login-bottom-wrapper">
          <div className="new-create-account">
            Already have an account?
            <Link to="/login">
              Sign In
            </Link>
          </div>
        </div>
      </div>
    </form>

  }
}
