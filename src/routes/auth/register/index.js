import {injectReducer} from '../../../store/reducers'
export default (store) => ({
    path: 'sign-up',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const container = require('./container').default;
            cb(null, container);
        }, 'sign-up');
    }
})
