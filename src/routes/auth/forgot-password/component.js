import React from 'react'
import { Link } from 'react-router'
import Ajax from '../../../modules/request'
import { Urls } from '../../../config/app'
import IsPhone from '../../../modules/phone'
import IsEmail from '../../../modules/email'
import Recaptcha from '../../../components/recaptcha'

export default class ForgotPassword extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      phone_number: '',
    }
    this.requestForNewPassword = this.requestForNewPassword.bind(this)
  }

  requestForNewPassword (event) {
    event && event.preventDefault()
    if (!IsPhone(this.state.phone_number) && !IsEmail(this.state.phone_number)) {
      this.setState({error_msg: 'Please enter either valid phone number or email address'})
      return
    }
    if (!this.state.is_verified && window.recaptcha && window.recaptcha.execute) {
      window.recaptcha.execute()
      return
    }
    this.setState({requesting: true})
    Ajax().post(Urls.forgot_password, {phone_number: this.state.phone_number}).then((response) => {
      this.setState({requesting: false})
      if (response.status === 202) {
        let text = IsPhone(this.state.phone_number) ? 'SMS.' : 'email.'
        alert(`We have just sent a new password to you by ${text}`)
        this.props.router.push('/login')
        return
      }
      if (response.response && response.response.data && response.response.data.errors && response.response.data.errors.phone_number) {
        this.setState({error_msg: 'Invalid email or phone number.'})
        return
      }
      if (response.response && response.response.data) {
        let responses = response.response.data.errors
        for (let i in responses) {
          this.setState({error_msg: responses[i][0]})
          return
        }
      }
      this.setState({error_msg: 'we are currently experience technical difficulties. Please try again later'})
    })
  }

  render () {
    return <form method="post" className="animated fadeIn">
      {this.state.error_msg && <p className="alert-msg">{this.state.error_msg}</p>}
      <div className="login_fields__user input-field">
        <div className="icon">
          <i className="fa fa-user"></i>
        </div>
        <input maxLength={50} onChange={(event) => {this.setState({phone_number: event.target.value})}}
               value={this.state.phone_number}
               type="text" placeholder="Mobile Number or Email"/>
      </div>
      <Recaptcha onSolved={() => {
        this.setState({is_verified: true})
        this.requestForNewPassword()
      }}/>
      <button type="submit" className="kyc-btn login-btn login_fields__user "
              onClick={this.requestForNewPassword}>
        {this.state.requesting ? (<i className="fa fa-spin fa-spinner"></i>) : 'Request'}
      </button>
      <div className="login-bottom-wrapper">
        <div className="new-create-account">
          Go back to
          <Link to="/login">
            Sign In
          </Link>
        </div>
      </div>
    </form>
  }
}
