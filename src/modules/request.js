import Axios from 'axios';
import Cookie from 'react-cookie'

export default () => {

    Axios.defaults.baseURL = 'http://localhost:8000';

    if (Cookie.load('Authorization')){
      Axios.defaults.headers.common['Authorization'] = Cookie.load('Authorization');
    }
    Axios.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        if(error.response && error.response.status === 401){
          Cookie.remove('Authorization');
        }
        if( error.response && error.response.data)
        {
          let errors = error.response.data.errors;
          for (let i in errors) {
            // alert(errors[i][0],'error')
          }
        }        return error;
    });
    return Axios
}

