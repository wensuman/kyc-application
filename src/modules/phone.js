export default (elem) => {
  return elem.match(/^9+([7-8][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]+)/) && elem.length === 10
}
