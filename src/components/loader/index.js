import React from 'react'
export default ()=>(
  <div className='loader-container'>
    <div className='loader-main'>
      <div className='loader--dot'></div>
      <div className='loader--dot'></div>
      <div className='loader--dot'></div>
      <div className='loader--dot'></div>
      <div className='loader--dot'></div>
      <div className='loader--dot'></div>
      <div className='loader--text'></div>
    </div>
  </div>
)