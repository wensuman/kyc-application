import React from 'react'
import Recaptcha from 'react-google-invisible-recaptcha'

export default (props)=>(
  <Recaptcha ref={ ref => {window.recaptcha = ref} } sitekey={ '6LfhIy4UAAAAAMlyMfavhXaBfTKZTs2fujibzoM6' }
             onResolved={ props.onSolved }/>
)