import React from 'react'
import { Link } from 'react-router'
import sidebarImage from './images/sidebar-1.jpg'
import mainLogo from './images/final_kyc_logo.png'
import Ajax from '../../modules/request'
import { Urls } from '../../config/app'
import Cookie from 'react-cookie'

export default class Sidebar extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      pic: '',
      first_name: '',
      last_name: '',
      loaded: false,
      template: ''
    }
    this.logout = this.logout.bind(this)
  }

  push (location) {
    this.props.router.push(location)
  }

  componentDidMount () {
    Ajax().get(Urls.form.template).then((response) => {
      this.setState({loaded: true, template: response.data.data ? response.data.data : []})
    })
  }

  logout () {
    this.setState({is_logging_out : true});
    setTimeout(() => {
      this.props.router ? this.props.router.push('/login') : window.location.href = '/login'
    }, 1500)
  }

  render () {
    return (
      <div className="sidebar" data-active-color="rose" data-background-color="black" data-image={sidebarImage}>
        <div className="logo">
          <a href="/" className="simple-text">
            <img src={mainLogo} className="img-responsive" alt="Logo"/>
          </a>
        </div>
        <div className="logo logo-mini">
          <a href="/" className="simple-text">
            K Y C <br/> NEPAL
          </a>
        </div>
        <div className="sidebar-wrapper">
          <ul className="nav">
            <li className="active">
              <Link to="/" activeClassName="active">
                <i className="material-icons">dashboard</i>
                <p>
                  Digital ID
                </p>
              </Link>
            </li>
            <li>
              <Link to="/form" activeClassName="active">
                <i className="material-icons">receipt</i>
                <p>
                  Personal Info
                </p>
              </Link>
            </li>
            <li>
              <Link to="/submissions" activeClassName="active">
                <i className="material-icons">send</i>
                <p>
                  Submissions
                </p>
              </Link>
            </li>
            <li>
              <Link to="/support" activeClassName="active">
                <i className="icon-hours-support-icon"></i>
                <p>
                  Support
                </p>
              </Link>
            </li>
            <li>
              <Link to="/settings" activeClassName="active">
                <i className="ace-icon fa fa-user"></i>
                <p>
                  Settings
                </p>
              </Link>
            </li>

            <li>
              <a href="#" onClick={(event)=>{
                event.preventDefault();
                this.setState({isLogginOut: true}); setTimeout(()=>{
                  Cookie.remove('Authorization');
                  this.props.router.push('/login');
                }, 1500)}}>
                {this.state.isLogginOut ? <i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i> : <i className="ace-icon fa fa-power-off"></i>}
                <p>
                  {!this.state.isLogginOut ? 'Log out' : 'Logging out'}
                </p>
              </a>
            </li>

          </ul>
          <div className="navbar-minimize">
            <button id="minimizeSidebar" className="btn btn-round btn-white btn-fill btn-just-icon">
              <i className="material-icons visible-on-sidebar-regular">keyboard_arrow_left</i>
              <i className="material-icons visible-on-sidebar-mini">keyboard_arrow_right</i>
            </button>
          </div>
        </div>
      </div>
    )
  }
}
