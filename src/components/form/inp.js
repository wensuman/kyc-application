import React from 'react'
import Dropzone from 'react-dropzone'

export const Inp = props => (
  <div className={props.col ? 'col-sm-' + props.col : 'col-sm-4'}>
    <div className="form-group">
      <label className="label-control">{props.label}</label>
      {props.children}
    </div>
  </div>
)

export let Clear = () => (<div className="clearfix"></div>)

export const Ttr = props => (
  <tr className="list-counter">
    <td></td>
    <td>{props.name}</td>
    <td colSpan="2">
      <div className="form-group">
        {props.children}
      </div>
    </td>
  </tr>
)

export class Display extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return <div className="dropzone-images">
      <button type="button" onClick={() => { this.ref.open() }}>
        Choose File
      </button>
      {this.props.url.filename() }
      <Dropzone ref={(node) => { this.ref = node }}
                onDrop={this.props.onDrop} accept="image/*">
      </Dropzone>
    </div>
  }
}

export class Dz extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return <div className="dropzone-images">
      <button type="button" onClick={() => { this.ref.open() }}>
        Choose File
      </button>
      No file chosen
      <Dropzone ref={(node) => { this.ref = node }}
                onDrop={this.props.onDrop} accept="image/*">
      </Dropzone>
      <br/>
    </div>
  }
}