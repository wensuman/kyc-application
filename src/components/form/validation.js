import IsEmail from '../../modules/email'
import IsPhone from '../../modules/phone'
export default () => {
  let jQuery2 = jQuery('.step-pane')
  jQuery2.find('.with-error').remove()
  jQuery2.find('.contains-error').removeClass('contains-error')
  jQuery('.step-pane [required]').each(function () {
    let th = jQuery(this)
    if (!th.val()) {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.siblings('.Select').addClass('contains-error')
      th.parents('.form-group').append('<span class="with-error"></span>')
      th.siblings('.dropzone-images, .action-wrapper').after('<p class="with-error"></p>')
      th.siblings('.dropzone-images, .action-wrapper').addClass('img-error')
    }
  })

  jQuery('[data-should-be]').each(function () {
    let th = jQuery(this)
    let val = th.val()
    //noinspection EqualityComparisonWithCoercionJS
    if (val && th.data('should-be') && th.data('should-be').trim && val.trim().toLowerCase() != th.data('should-be').trim().toLowerCase()) {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.siblings('.Select').addClass('contains-error')
      th.parents('.form-group').append('<div class="with-error">' + jQuery(this).data('error-message') + '</div>')
    }
  })
  jQuery('.step-pane [data-text-only]').each(function () {
    let th = jQuery(this)
    if (th.val() === '')
      return
    if (!th.val().match(/^[A-Za-z]+$/)) {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.parents('.form-group').append('<div class="with-error">Only alphabets are allowed in this field.</div>')
    }
  });
  jQuery('.step-pane [data-fullname-validation]').each(function () {
    let th = jQuery(this)
    if (th.val() === '')
      return    
    if ( th.val().match(/\S+/g ).length < 2 ) {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.parents('.form-group').append('<span class="with-error">Please enter full name</span>')
    }
  });

  jQuery('.step-pane [data-text]').each(function () {
    let th = jQuery(this)
    if (th.val() === '')
      return
    if (!th.val().match(/^[A-Za-z\s]+$/)) {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.parents('.form-group').append('<div class="with-error"></div>')
    }
  })

  jQuery('.step-pane [data-must-have-yes]').each(function () {
    let th = jQuery(this)
    if (th.val() === '')
      return
    if (th.val() !== 'yes') {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.siblings('.Select').addClass('contains-error')
      th.parents('.form-group').append('<div class="with-error"></div>')
      // th.siblings('.dropzone-images, .action-wrapper').after('<p class="with-error"></p>')
      // th.siblings('.dropzone-images, .action-wrapper').addClass('img-error')
    }
  })

  jQuery('.step-pane [minLength]').each(function () {
    let th = jQuery(this)
    if (th.val() === '')
      return

    if (th.val().length < th.attr('minLength')) {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.parents('.form-group').append(`<div class="with-error">Minimum ${th.attr('minLength')} characters are required</div>`)
    }
  })

  jQuery('.step-pane [type=number][max]').each(function () {
    let th = jQuery(this)
    if (th.val() === '')
      return

    if (parseInt(th.val()) > parseInt(th.attr('max'))) {
      th.parents('.form-group').find('.with-error').remove()
      th.addClass('contains-error')
      th.parents('.form-group').append(`<div class="with-error">Maximum alloweded number is  ${th.attr('max')} </div>`)
    }
  })
  jQuery('.step-pane [type=email]').each(function () {
    let th = jQuery(this)
    if (th.val() === '')
      return
    if (IsEmail(th.val()))
      return
    th.parents('.form-group').find('.with-error').remove()
    th.addClass('contains-error')
    th.parents('.form-group').append('<div class="with-error">Please enter a valid email address.</div>')
  })
  jQuery('.step-pane [type=tel]').each(function () {

    let th = jQuery(this)

    if (th.val() === '')
      return

    if (IsPhone(th.val()))
      return

    th.parents('.form-group').find('.with-error').remove()
    th.addClass('contains-error')
    th.parents('.form-group').append('<div class="with-error">Please enter a valid mobile number.</div>')
  })
  let b = jQuery2.find('.with-error').length > 0
  if (b) {
    alert('Please fill the mandatory fields.', 'error')
  }
  return b
}