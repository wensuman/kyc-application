let map, input, autocomplete
export default (lat,lng, setter)=>{
  map = new GMaps({
    div: '#gmap',
    lat: lat,
    lng: lng,
    scrollwheel: false
  }),
    input = document.getElementById('location_search'),
    autocomplete = new google.maps.places.Autocomplete(input)
  map.setCenter(lat, lng)
  map.setZoom(15)
  map.addMarker({
    lat: lat,
    lng: lng,
    title: '',
    draggable: true,
    dragend: function (e) {
      let lat = e.latLng.lat()
      let lng = e.latLng.lng()
      map.setCenter(lat, lng)
      let latlng = new google.maps.LatLng(lat, lng)
      let geocoder = geocoder = new google.maps.Geocoder()
      geocoder.geocode({'latLng': latlng}, function (results, status) {
        status === google.maps.GeocoderStatus.OK && results[1] && setter({lat: lat, lng: lng, location_search: results[1].formatted_address, change: true})
      })
    }
  })
  autocomplete.bindTo('bounds', map)
  autocomplete.addListener('place_changed', function () {
    let place = autocomplete.getPlace()
    if (!place.geometry) {
      window.alert('Autocomplete\'s returned place contains no geometry')
      return
    }
    map.removeMarkers()
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport)
    } else {
      map.setCenter(place.geometry.location)
      map.setZoom(15)
    }
    let lat = place.geometry.location.lat()
    let lng = place.geometry.location.lng()
    let latlng = new google.maps.LatLng(lat, lng)
    let geocoder = geocoder = new google.maps.Geocoder()
    geocoder.geocode({'latLng': latlng}, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          setter({lat: lat, lng: lng, location_search: results[1].formatted_address, change: true})
        }
      }
    })
    map.addMarker({
      lat: lat,
      lng: lng,
      title: place.formatted_address,
      draggable: true,
      dragend: function (e) {
        let lat = e.latLng.lat()
        let lng = e.latLng.lng()
        map.setCenter(lat, lng)

        let latlng = new google.maps.LatLng(lat, lng)
        let geocoder = geocoder = new google.maps.Geocoder()
        geocoder.geocode({'latLng': latlng}, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              setter({lat: lat, lng: lng, location_search: results[1].formatted_address, change: true})
            }
          }
        })
      }
    })
  })

  map.addListener('click', function(event) {    
    placeMarker(event.latLng);
  });

  function placeMarker(location) {

    map.removeMarkers()

    let latlng = new google.maps.LatLng(location.lat(), location.lng())
    let geocoder = geocoder = new google.maps.Geocoder()
    geocoder.geocode({'latLng': latlng}, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          setter({lat: location.lat(), lng: location.lng(), location_search: results[1].formatted_address, change: true})
        }
      }
    })    
    map.addMarker({
      position: location,
      // lat: position.lat(),
      // lng: position.lng(),
      draggable: true,
      dragend: function (e) {
        let lat = e.latLng.lat()
        let lng = e.latLng.lng()
        map.setCenter(lat, lng)

        latlng = new google.maps.LatLng(lat, lng)
        geocoder = geocoder = new google.maps.Geocoder()
        geocoder.geocode({'latLng': latlng}, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              setter({lat: lat, lng: lng, location_search: results[1].formatted_address, change: true})
            }
          }
        })
      }
    });
    map.setCenter(location.lat(), location.lng())
  }

}