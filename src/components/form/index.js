import React from 'react'
import Validation from './validation'
import { Urls } from '../../config/app'
import { Clear, Display, Dz, Inp, Ttr } from './inp'
import Map from './map'
import {
  Countries,
  Districts,
  optionEducation,
  optionEmploymentType,
  OptionGender,
  optionNatureOfBusiness,
  optionReligion,
  ProvinceData
} from '../../config/lists'
import Select from 'react-select'
import 'react-select/dist/react-select.css'
import request from 'superagent'
import Dropzone from 'react-dropzone'
import AgeCalculator from './agecalculator'
import Ajax from '../../modules/request'

export default class Form extends React.Component {
  constructor (props) {
    super(props)
    this.state = this.props.template
    this.componentDidMount = this.componentDidMount.bind(this)
    this.componentWillUnmount = this.componentWillUnmount.bind(this)
    this.submitForVerification = this.submitForVerification.bind(this)
    this.componentDidUpdate = this.componentDidUpdate.bind(this)
    this.bootDates = this.bootDates.bind(this)
    this.max_upload_size = 3145728 // 3 MB
    this.date_of_birth_en = this.date_of_birth_en.bind(this)
    this.date_of_birth_np = this.date_of_birth_np.bind(this)
    this.uploadField = this.uploadField.bind(this)
    this.thumb_image = this.thumb_image.bind(this)
  }

  static mapper (map) {
    return {
      label: map.LN,
      value: map.LN
    }
  }

  date_of_birth_np () {
    this.setState({date_of_birth_en: BS2AD(this.state.date_of_birth_np)})
  }

  date_of_birth_en () {
    this.setState({date_of_birth_np: AD2BS(this.state.date_of_birth_en)})
  }

  bootDates () {
    let th = this
    setTimeout(() => {
      jQuery('.nepali-date').nepaliDatePicker({
        npdMonth: true,
        npdYear: true,
        disableDaysAfter: '1',
        onChange: () => {
          jQuery('.nepali-date').each(function () {
            jQuery(this).removeClass('contains-error');
            jQuery(this).siblings('.with-error').remove();
            if (jQuery(this).val() !== th.state[jQuery(this).attr('name')]) {
              th.setState({[jQuery(this).attr('name')]: jQuery(this).val()})
              th[jQuery(this).attr('name')] && th[jQuery(this).attr('name')]()
            }
          })
        }
      })
      jQuery('.datepicker').each(function () {
        jQuery(this).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: "-1900:2080",
          minDate: jQuery(this).hasClass('more-than-today') ? new Date : undefined,
          maxDate: jQuery(this).hasClass('less-than-today') ? '-1D' : undefined,
          dateFormat: 'yy-mm-dd'
        }).on('change', (e) => {
          if (e.target.value !== th.state[e.target.name]) {
            th.setState({[e.target.name]: e.target.value});
            th[e.target.name] && th[e.target.name]();
          }
        });
      })
    }, 500)
  }

  componentDidMount () {
    this.bootDates()
  }

  thumb_image (way) {
    let key = `${way}_thumb_image`
    return (
      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 no-left-padding">
        <div className="dropzone-signature-wrapper">
          <h5>{way} thumb image *</h5>
          {this.state[key] ? (
            <img src={ Urls.site + this.state[key]} style={{width: '200px'}} alt={way + ' thumb'}/>
          ) : (
            <Dropzone style={{display: 'none'}} ref={(node) => { this[key] = node }}
                      onDrop={this.onDrop.bind(this, key)} accept="image/*">
            </Dropzone>
          )}
          <div className={this.state[key] ? 'action-wrapper' : ' empty action-wrapper' }>
            <span>{this.state[key] ? this.state[key].filename() : 'Upload'}</span>
            <button type="button"
                    onClick={() => { this.state[key] ? this.setState({[key]: ''}) : this[key].open() }}>
              {( !this.state[key] ) ? (<i className="fa fa-upload"></i>) : (
                <i className="fa fa-times"></i>) }
            </button>
          </div>
          { !this.state.client_signature &&
          <input id={key} required="required" type="hidden" value={this.state[key]}/> }
        </div>
      </div>
    )
  }

  submitForVerification () {
    if (Validation()) {
      return
    }
    if (!this.state.declaration_ok) {
      alert('Please, agree with terms and conditions.', 'error')
      return
    }
    jQuery(this.submitref).html('<i class="fa fa-spinner fa-spin"></i>')
    this.props.action(this.state)
  }

  gmapInit () {
    let th = this
    let lat = 27.678100729922
    let lng = 85.315631934363
    if (!this.state.lat || !this.state.lng) {
      navigator.geolocation.getCurrentPosition(function (position) {
          if (position.coords.latitude && position.coords.longitude) {
            lat = position.coords.latitude
            lng = position.coords.longitude
          }
          Map(lat, lng, (comes) => {th.setState(comes)})
        },
        function (error) {
          Map(lat, lng, (comes) => {th.setState(comes)})
        })
    } else {
      Map(this.state.lat, this.state.lng, (comes) => {this.setState(comes)})
    }
  }

  uploadField (label, key, required = false) {
    return (
      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-4">
        <div className="dropzone-wrapper">
          <h5>{label}</h5>
          {this.state[key] ? (
            <Display onDrop={this.onDrop.bind(this, key)} url={this.state[key]}
                     onDelete={() => {this.setState({[key]: ''})}}/>
          ) : (
            <Dz onDrop={this.onDrop.bind(this, key)}/>
          )}
          {required && <input id={key} required="required" type="hidden" value={this.state[key]}/>}
        </div>
      </div>
    )
  }

  componentDidUpdate () {
    this.bootDates()
    this.interval && clearTimeout(this.interval)
    this.interval = setTimeout(() => {
      const data = {
        ...this.state,
        activeStep: undefined,
        change: undefined,
        uploading: undefined,
        voters_data: undefined
      }
      this.props.change(data)
    }, 1500)
  }

  componentWillUnmount () {
    this.interval && clearTimeout(this.interval)
  }

  onDrop (meta, acceptedFiles) {
    const req = request.post(Urls.upload)
    req.attach('file', acceptedFiles[0])
    let af = acceptedFiles[0]
    if (af.size >= this.max_upload_size) {
      alert(`File: ${af.name} must be less than ${this.max_upload_size / (1024 * 1024)}MB`, 'danger')
      return
    }
    let name = `${meta}_upload_status`
    alert(`<span class="${name}"><i class="fa fa-spin fa-sipnner " style="display: block !important;"></i>Uploading</span>`)
    req.end((err, response) => {
      let data = {}
      if (err && err.response && err.response.text) {
        let json = JSON.parse(err.response.text)
        alert(json.errors.file[0], 'error')
        data.uploading = false
      }
      data[meta] = response.body.data[0].name
      data.uploading = false
      this.setState(data)
      PNotify.removeAll()
      jQuery(`#${meta}`).siblings('.with-error').remove()
    })
  }

  isNepaleseNrn () {
    return this.state.nationality === 'nepalese' || this.state.nationality === 'nrn'
  }

  isNotNepaleseNrn () {
    return this.state.nationality === 'foreigner'
  }

  isNotMinor () {
    return AgeCalculator(this.state.date_of_birth_en) >= 16
  }

  isCountryIndia () {
    return this.state.country === 'India'
  }

  isNotCountryIndia () {
    return this.state.country !== 'India'
  }

  isRefugee () {
    return this.state.is_refugee === 'yes'
  }

  isNotRefugee () {
    return this.state.is_refugee === 'no'
  }

  getChildLocation (parent) {
    if (typeof parent !== 'string') {
      return []
    }
    let district = ProvinceData.filter((item) => {return item.LN.toLowerCase() === parent.toLowerCase()})
    if (district[0]) {
      return ProvinceData.filter(function (item) { return item.P === district[0].LI }).map(Form.mapper)
    }
    return []
  }

  changeStep (step) {
    if (step > this.state.activeStep) {
      if (this.state.activeStep + 1 === step) {
        if (Validation()) {
          return
        }
      } else {
        return
      }
    }
    this.setState({activeStep: step})
    step === 3 && setTimeout(this.gmapInit.bind(this), 500)
  }

  render () {
    return (
      <div className="card-wrapper">
        {
          this.state.reason &&
          <div className="col-xs-12 no-padding" >
            <div className="alert alert-danger"> 
              Your profile was not accepted because {this.state.reason}. Please check your details and submit again.
            </div>
          </div>
        }
        <div id="form-template" className='card'>
          <div className="card-content">
            <h3 className="card-title">Please fill your information below</h3>
          </div>
          <Clear/>
          <div className="step-navigation">
            <ul className="steps">
              {['IDENTITY', 'NATIONALITY', 'ADDRESS', 'OCCUPATION', 'FAMILY', 'MISC', 'DOCUMENTS', 'DECLARATION'].map((value, num) => (
                <li style={{cursor: this.state.activeStep >= num ? 'default' : 'not-allowed' }} onClick={this.changeStep.bind(this, num + 1)} key={num + 1}
                    className={this.state.activeStep > (num + 1)
                      ? 'complete active'
                      : (this.state.activeStep === (num + 1)
                        ? 'active'
                        : '')}>
                  <span className="step">{++num}</span>
                  <span className="title">{value}</span>
                </li>
              ))}
            </ul>
          </div>
          <Clear/>
          <div className="step-content pos-rel">
            <div className='step-pane active'>
              <div className="card-content">
                {this.state.activeStep === 1 && <form>
                  <Inp label="First Name *">
                    <input data-text="true" required minLength={3} maxLength={20}
                           onChange={(event) => { event.target.value.match(/\d+/g) === null && this.setState({first_name: event.target.value})}}
                           value={this.state.first_name} type='text'
                           data-should-be={this.state.voters_data.first_name}
                           data-error-message={'First Name on voter\'s ID is ' + this.state.voters_data.first_name}
                           className='form-control'/>
                  </Inp>
                  <Inp label='Middle Name'>
                    <input data-text='true' minLength={3} maxLength={20}
                           onChange={(event) => {event.target.value.match(/\d+/g) === null && this.setState({middle_name: event.target.value})}}
                           value={this.state.middle_name} type='text'
                           data-should-be={this.state.voters_data.middle_name}
                           data-error-message={'Middle Name on voter\'s ID is ' + this.state.voters_data.middle_name}
                           className='form-control'/>
                  </Inp>
                  <Inp label='Last Name *'>
                    <input data-text='true' required minLength={3} maxLength={20}
                           onChange={(event) => {event.target.value.match(/\d+/g) === null && this.setState({last_name: event.target.value})}}
                           value={this.state.last_name} type=' text'
                           data-should-be={this.state.voters_data.last_name}
                           data-error-message={'Last Name on voter\'s ID is ' + this.state.voters_data.last_name}
                           className=' form-control'/>
                  </Inp>
                  <Clear/>
                  <Inp label='Gender *' col='3'>
                    <Select searchable={false} value={this.state.gender}
                            onChange={(value) => {this.setState({gender: value.value})}}
                            className="capitalable"
                            options={OptionGender}/>
                    <input
                      data-should-be={this.state.voters_data.gender}
                      data-error-message={'Gender on Voter\'s ID is ' + this.state.voters_data.gender} required='required'
                      type='hidden'
                      className='hidden-input' value={this.state.gender}/>
                  </Inp>
                  <div className='col-sm-9 col-xs-12 no-padding'>
                    <Inp label="Date of Birth" col='4'>
                      <div className="form-fields-radio">
                        <label><input type="radio" onChange={(event)=>{}} checked={this.state.dob_in === 'AD'}
                                      onClick={() => {this.setState({dob_in: 'AD'})}}/>A.D.</label>
                        <label><input type="radio" onChange={(event)=>{}} checked={this.state.dob_in === 'BS'}
                                      onClick={() => {this.setState({dob_in: 'BS'})}}/>B.S.</label>
                      </div>
                    </Inp>
                    <Inp label='A.D. *' col='4'>
                      <input readOnly required name='date_of_birth_en'
                             value={this.state.date_of_birth_en} type='text'
                             id='englishDate'
                             className='form-control datepicker less-than-today' disabled={this.state.dob_in !== 'AD'}/>
                    </Inp>
                    <Inp label='B.S. *' col='4'>
                      <input readOnly required name='date_of_birth_np'
                             value={this.state.date_of_birth_np} type='text'
                             data-should-be={this.state.voters_data.date_of_birth_np}
                             data-error-message={'Date of Birth on voter\'s id is ' + this.state.voters_data.date_of_birth_np}
                             id='nepaliDate'
                             className='form-control nepali-date' disabled={this.state.dob_in !== 'BS'}/>
                    </Inp>

                  </div>
                </form>}
                {this.state.activeStep === 2 && <form>
                  <div className="col-sm-12">
                    <div className="form-group no-padding nationality-form-group">
                      <div className="col-sm-2 no-mph">
                        <label className="label-control">Nationality</label>
                      </div>
                      <div className="col-sm-10 form-fields-radio">
                        <div className="col-sm-4">
                          <label ><input type="radio" onChange={(event)=>{}} checked={this.state.nationality === 'nepalese'}
                                         onClick={() => {this.setState({
                                           nationality: 'nepalese',
                                           country: '',
                                           citizenship_issued_district: this.state.citizenship_issued_district ? this.state.citizenship_issued_district : 'achham',
                                           passport_issued_district: this.state.passport_issued_district ? this.state.passport_issued_district : 'achham',
                                           is_refugee:'no',
                                           foreign_passport_number: '',
                                           foreign_passport_issued_on: '',
                                           foreign_passport_expiry_date: '',
                                           foreign_visa_expiry_date: '',
                                           any_other_valid_identification: 'no',
                                           identification_number: '',
                                           identification_type: '',
                                           identification_issued_authority: '',
                                           identification_issued_on: '',
                                           identification_expiry_date: '',
                                           })}}/>Nepalese</label>
                        </div>
                        <div className="col-sm-4">
                          <label><input type="radio" onChange={(event)=>{}} checked={this.state.nationality === 'nrn'}
                                        onClick={() => {this.setState({
                                          nationality: 'nrn',
                                          country: '',
                                          citizenship_issued_district: this.state.citizenship_issued_district ? this.state.citizenship_issued_district : 'achham',
                                          passport_issued_district: this.state.passport_issued_district ? this.state.passport_issued_district : 'achham',
                                          is_refugee: 'no',
                                          foreign_passport_number: '',
                                          foreign_passport_issued_on: '',
                                          foreign_passport_expiry_date: '',
                                          foreign_visa_expiry_date: '',
                                          any_other_valid_identification: 'no',
                                          identification_number: '',
                                          identification_type: '',
                                          identification_issued_authority: '',
                                          identification_issued_on: '',
                                          identification_expiry_date: '',
                                          })}}/>NRN</label>
                        </div>
                        <div className="col-sm-4">
                          <label><input type="radio" onChange={(event)=>{}} checked={this.state.nationality === 'foreigner'}
                                        onClick={() => {this.setState({
                                          country: 'Afghanistan',
                                          nationality: 'foreigner',
                                          citizenship_number: '',
                                          citizenship_issued_on: '',
                                          citizenship_issued_district: '',
                                          pan_number: '',
                                          voters_id_number: '',
                                          has_passport: 'no',
                                          passport_issued_on: '',
                                          passport_expiry_date: '',
                                          passport_number: '',
                                          passport_issued_district: '',                                          
                                          has_driving_license: 'no',
                                          driving_license_number: '',
                                          driving_license_issued_on: '',
                                          driving_license_expiry_date: '',
                                          })}}/>Foreigner</label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="clearfix"></div>
                  {this.isNepaleseNrn() && this.isNotMinor() && <div>
                    <Inp label="Citizenship No. *" col="4">
                      <input required="required" maxLength="30" value={this.state.citizenship_number}
                             onChange={(event) => {this.setState({citizenship_number: event.target.value})}}
                             data-should-be={this.state.voters_data.citizenship_number}
                             data-error-message={'Citizenship no. on voter\'s id is ' + this.state.citizenship_number}
                             type="text" className="form-control"/>
                    </Inp>
                    <Inp label="Date of Issue *" col="4">
                      <input required readOnly name="citizenship_issued_on"
                             value={this.state.citizenship_issued_on} type="text"
                             id="citizenship-nepali" className="form-control nepali-date"/>
                    </Inp>
                    <Inp label="Issued District *" col="4">
                      <Select searchable matchPos="start" value={this.state.citizenship_issued_district}
                              onChange={(event) => {this.setState({citizenship_issued_district: event.value})}}
                              options={ProvinceData.filter((district) => {return district.LT === 'District'}).map(Form.mapper)}
                              className="kyc-select capitalable"/>
                      <input required="required" type="hidden" className="hidden-input"
                             value={this.state.citizenship_issued_district}/>
                    </Inp>

                    <Inp label="Voter ID No.">
                      <input minLength={1} maxLength={30} name="voters_id_number"
                             value={this.state.voters_id_number}
                             onChange={(event) => {
                               if (!event.target.value) {
                                 this.setState({voters_data: {}})
                                 return
                               }
                               event.target.value && Ajax().get(Urls.voters, {params: {'voters_id': event.target.value}}).then((response) => {
                                 if (response.status !== 200) {
                                   this.setState({voters_data: {}})
                                   return
                                 }
                                 response.status === 200 && response.data.data && Object.keys(response.data.data).map(key => !this.state[key] && this.setState({[key]: response.data.data[key]})) && this.setState({voters_data: response.data.data})
                               })
                               this.setState({voters_id_number: event.target.value})
                             }}
                             type="text"
                             className="form-control"/>
                    </Inp>

                    <Inp label="Individual PAN No.">
                      <input value={this.state.pan_number}
                             onChange={(event) => {this.setState({pan_number: event.target.value})}}
                             className='form-control' type='text' minLength={3} maxLength={40}/>
                    </Inp>


                    <Inp label="Do you have a passport?">
                      <div className="form-fields-radio">
                        <label><input type="radio" onChange={(event)=>{}} checked={this.state.has_passport === 'yes'}
                                      onClick={() => {this.setState({has_passport: 'yes'})}}/>Yes</label>
                        <label><input type="radio" onChange={(event)=>{}} checked={this.state.has_passport === 'no'}
                                      onClick={() => {this.setState({
                                        has_passport: 'no',
                                        passport_number: '',
                                        passport_issued_on: '',
                                        passport_expiry_date: '',
                                        passport_issued_district: '',
                                        })}}/>No</label>
                      </div>
                    </Inp>
                    {this.state.has_passport === 'yes' && <div>
                      <Inp label="Passport No. *">
                        <input required="required" maxLength="30" value={this.state.passport_number}
                               onChange={(event) => {this.setState({passport_number: event.target.value})}}
                               type="text"
                               className="form-control"/>
                      </Inp>
                      <Inp label="Date of Issue">
                        <input name="passport_issued_on"
                               value={this.state.passport_issued_on}
                               onChange={(event) => {this.setState({passport_issued_on: event.target.value})}}
                               type="text" className="form-control datepicker less-than-today"
                               readOnly={'readonly'}/>
                      </Inp>
                      <Inp label="Expiry Date *">
                        <input required="required" name="passport_expiry_date"
                               value={this.state.passport_expiry_date}
                               onChange={(event) => {this.setState({passport_expiry_on: event.target.value})}}
                               type="text"
                               className="form-control datepicker more-than-today" readOnly={'readonly'}/>
                      </Inp>
                      <Inp label="Issued District *">
                        <Select className="capitalable" value={this.state.passport_issued_district}
                                onChange={(event) => {this.setState({passport_issued_district: event.value})}}
                                options={ProvinceData.filter((district) => {return district.LT === 'District'}).map(Form.mapper)}/>
                        <input required="required" type="hidden" className="hidden-input"
                               value={this.state.passport_issued_district}/>
                      </Inp>
                    </div>}
                    <Inp label="Do you have a driving license?">
                      <div className="form-fields-radio">
                        <label><input type="radio" onChange={(event)=>{}} checked={this.state.has_driving_license === 'yes'}
                                      onClick={() => {this.setState({has_driving_license: 'yes'})}}/>Yes</label>
                        <label><input type="radio" onChange={(event)=>{}} checked={this.state.has_driving_license === 'no'}
                          onClick={() => { this.setState({ has_driving_license: 'no', driving_license_number: '', driving_license_expiry_date: '', driving_license_issued_on: '', })}}/>No</label>
                      </div>
                    </Inp>
                    {this.state.has_driving_license === 'yes' && <div>
                      <Inp label="License No. *">
                        <input required="required" value={this.state.driving_license_number}
                               onChange={(event) => {this.setState({driving_license_number: event.target.value})}}
                               type="text"
                               className="form-control "/>
                      </Inp>
                      <Inp label="Date of Issue">
                        <input name="driving_license_issued_on"
                               value={this.state.driving_license_issued_on}
                               onChange={(event) => {this.setState({driving_license_issued_on: event.target.value})}}
                               type="text"
                               className="form-control datepicker less-than-today" readOnly={'readonly'}/>
                      </Inp>
                      <Inp label="Expiry Date *">
                        <input required="required" name="driving_license_expiry_date"
                               value={this.state.driving_license_expiry_date}
                               onChange={(event) => {this.setState({driving_license_expiry_date: event.target.value})}}
                               type="text"
                               className="form-control datepicker more-than-today" readOnly={'readonly'}/>
                      </Inp>
                    </div>}
                  </div>}
                  <div className="clearfix"></div>
                  {this.isNotNepaleseNrn() && <div>
                    <Inp label="Country *" col="4">
                      <Select class="capitalable" matchPos="start" searchable value={this.state.country}
                              onChange={(event) => {this.setState({country: event.value})}}
                              options={ Countries }/>
                      <input type="hidden" required="required" value={this.state.country}/>
                    </Inp>
                    {this.isNotCountryIndia() && <div>
                      <Inp label="Are you refugee?">
                        <div className="form-fields-radio">
                          <label><input type="radio" onChange={(event)=>{}} checked={this.state.is_refugee === 'yes'}
                                        onClick={() => {this.setState({is_refugee: 'yes'})}}/>Yes</label>
                          <label><input type="radio" onChange={(event)=>{}} checked={this.state.is_refugee === 'no'}
                                        onClick={() => {this.setState({
                                          is_refugee: 'no',
                                          foreign_passport_number: '',
                                          foreign_passport_issued_on: '',
                                          foreign_passport_expiry_date: '',
                                          foreign_visa_expiry_date: '',
                                          })}}/>No</label>
                        </div>
                      </Inp>
                    </div>}
                    {this.isNotNepaleseNrn() && ( this.isNotRefugee() || this.isCountryIndia() ) && <div>
                      { this.isCountryIndia() ? (
                        <div>
                          <Inp label="Passport No.">
                            <input minLength={3} maxLength={40}
                                   value={this.state.foreign_passport_number}
                                   onChange={(event) => {this.setState({foreign_passport_number: event.target.value})}}
                                   type="text"
                                   className="form-control"/>
                          </Inp>
                          <Inp label="Date of Issue">
                            { this.state.foreign_passport_number !== '' ? (
                              <input required="required" name="foreign_passport_issued_on"
                                     value={this.state.foreign_passport_issued_on}
                                     onChange={(event) => {this.setState({foreign_passport_issued_on: event.target.value})}}
                                     type="text"
                                     className="form-control datepicker less-than-today"
                                     readOnly={'readonly'}/>
                            ) : (
                              <input name="foreign_passport_issued_on"
                                     value={this.state.foreign_passport_issued_on}
                                     onChange={(event) => {this.setState({foreign_passport_issued_on: event.target.value})}}
                                     type="text"
                                     className="form-control datepicker less-than-today"
                                     readOnly={'readonly'}/>
                            ) }
                          </Inp>
                          <Inp label="Expiry Date">
                            { this.state.foreign_passport_number !== '' && this.isNotCountryIndia() ? (
                              <input required="required" name="foreign_passport_expiry_date"
                                     value={this.state.foreign_passport_expiry_date}
                                     onChange={(event) => {this.setState({foreign_passport_expiry_date: event.target.value})}}
                                     type="text"
                                     className="form-control datepicker more-than-today"
                                     readOnly={'readonly'}/>
                            ) : (
                              <input name="foreign_passport_expiry_date"
                                     value={this.state.foreign_passport_expiry_date}
                                     onChange={(event) => {this.setState({foreign_passport_expiry_date: event.target.value})}}
                                     type="text"
                                     className="form-control datepicker more-than-today"
                                     readOnly={'readonly'}/>
                            ) }
                          </Inp>
                          <Inp label="Visa Expiry Date">
                            <input name="foreign_visa_expiry_date"
                                   value={this.state.foreign_visa_expiry_date}
                                   onChange={(event) => {this.setState({foreign_visa_expiry_date: event.target.value})}}
                                   type="text"
                                   className="form-control datepicker more-than-today" readOnly={'readonly'}/>
                          </Inp>
                        </div>
                      ) : (
                        <div>
                          <Inp label="Passport No. *">
                            <input required="required" minLength={3} maxLength={40}
                                   value={this.state.foreign_passport_number}
                                   onChange={(event) => {this.setState({foreign_passport_number: event.target.value})}}
                                   type="text"
                                   className="form-control"/>
                          </Inp>
                          <Inp label="Date of Issue">
                            <input name="foreign_passport_issued_on"
                                   value={this.state.foreign_passport_issued_on}
                                   onChange={(event) => {this.setState({foreign_passport_issued_on: event.target.value})}}
                                   type="text"
                                   className="form-control datepicker less-than-today" readOnly={'readonly'}/>
                          </Inp>
                          <Inp label="Expiry Date *">
                            <input required="required" name="foreign_passport_expiry_date"
                                   value={this.state.foreign_passport_expiry_date}
                                   onChange={(event) => {this.setState({foreign_passport_expiry_date: event.target.value})}}
                                   type="text"
                                   className="form-control datepicker more-than-today" readOnly={'readonly'}/>
                          </Inp>
                          <Inp label="Visa Expiry Date *">
                            <input required="required" name="foreign_visa_expiry_date"
                                   value={this.state.foreign_visa_expiry_date}
                                   onChange={(event) => {this.setState({foreign_visa_expiry_date: event.target.value})}}
                                   type="text"
                                   className="form-control datepicker more-than-today" readOnly={'readonly'}/>
                          </Inp>
                        </div>
                      ) }
                      <Inp label="Any other valid identification?">
                        <div className="form-fields-radio">
                          <label><input type="radio" onChange={(event)=>{}} checked={this.state.any_other_valid_identification === 'yes'}
                                        onClick={() => {this.setState({any_other_valid_identification: 'yes'})}}/>Yes</label>
                          <label><input type="radio" onChange={(event)=>{}} checked={this.state.any_other_valid_identification === 'no'}
                                        onClick={() => {this.setState({
                                          any_other_valid_identification: 'no',
                                          identification_number: '',
                                          identification_type: '',
                                          identification_issued_authority: '',
                                          identification_issued_on: '',
                                          identification_expiry_date: '',
                                          })}}/>No</label>
                        </div>
                      </Inp>
                    </div>}
                    {((this.isNotCountryIndia() && this.isRefugee()) || ( this.isNotNepaleseNrn() && this.state.any_other_valid_identification === 'yes'  )) &&
                    <div>
                      <Inp label="ID No. * ">
                        <input required="required" maxLength="30" value={this.state.identification_number}
                               onChange={(event) => {this.setState({identification_number: event.target.value})}}
                               className="form-control" type="text"/>
                      </Inp>
                      <Inp label="ID Type *">
                        <input data-text="true" maxLength="30" required
                               value={this.state.identification_type}
                               onChange={(event) => {this.setState({identification_type: event.target.value})}}
                               className="form-control" type="text"/>
                      </Inp>
                      <Inp label="Issuing Authority * ">
                        <input data-text="true" maxLength="30" required
                               value={this.state.identification_issued_authority}
                               onChange={(event) => {this.setState({identification_issued_authority: event.target.value})}}
                               className="form-control"
                               type="text"/>
                      </Inp>
                      <Inp label="Date of Issue">
                        <input name="identification_issued_on"
                               value={this.state.identification_issued_on}
                               onChange={(event) => {this.setState({identification_issued_on: event.target.value})}}
                               type="text"
                               className="form-control datepicker less-than-today" readOnly={'readonly'}/>
                      </Inp>
                      <Inp label="Expiry Date *">
                        <input required="required" name="identification_expiry_date"
                               value={this.state.identification_expiry_date}
                               onChange={(event) => {this.setState({identification_expiry_date: event.target.value})}}
                               type="text"
                               className="form-control datepicker more-than-today" readOnly={'readonly'}/>
                      </Inp>
                    </div>}
                    {this.isNotNepaleseNrn() && this.isCountryIndia() &&
                    <div className="col-sm-12 wizard-actions">
                      <span
                        className="pull-left">Either one is required (Passport detail/Other Valid Identification) </span>
                    </div>}

                  </div>}
                </form>}
                {this.state.activeStep === 3 && <form>
                  <div className="wrapper-div">
                    <div className="col-xs-12 no-padding">
                      <h4 className="align-left form-heading">Current Residential Address</h4>
                       </div>
                    <Inp col="4 no-padding" label="District *">
                      <Select className="capitalable" searchable value={this.state.current_district_name} onChange={(value) => {
                        this.setState({
                          current_district_name: value.label,
                          current_municipality_name: ''
                        });
                      }} options={Districts}/>
                      <input required="required" type="hidden" className="hidden-input"
                             data-should-be={this.state.current_address_same_as_permanent !== 'no' ? this.state.voters_data.permanent_district : ''}
                             data-error-message={'Current district name on voter\'s ID is ' + this.state.voters_data.permanent_district}
                             value={this.state.current_district_name}/>
                    </Inp>
                    <Inp col="4 no-padding-small" label="Municipality / VDC *">
                      <Select className="capitalable" searchable={false} value={this.state.current_municipality_name}
                              onChange={(value) => this.setState({current_municipality_name: value.label})}
                              options={this.getChildLocation(this.state.current_district_name) }/>
                      <input required="required" type="hidden" className="hidden-input"
                             data-should-be={ this.state.current_address_same_as_permanent !== 'no' ? this.state.voters_data.permanent_municipality : '' }
                             data-error-message={ 'Current municipality name on voter\'s ID is ' + this.state.voters_data.permanent_municipality }
                             value={this.state.current_municipality_name}/>
                    </Inp>
                    <Inp col="4 no-padding p-r-0" label="Ward No. *">
                      <input required="required" value={this.state.current_ward_number}
                             onChange={(event) => {this.setState({current_ward_number: event.target.value})}}
                             className="form-control" type="number" max={40} min={1}/>
                    </Inp>
                    <Inp col="4 no-padding" label="Town / City *">
                      <input required="required" value={this.state.current_town} minLength={3} maxLength={50}
                             onChange={(event) => {this.setState({current_town: event.target.value})}}
                             className="form-control" type="text"/>
                    </Inp>
                    <Inp col="4 no-padding-small" label="Street / Tole">
                      <input value={this.state.current_street}
                             onChange={(event) => {this.setState({current_street: event.target.value})}}
                             minLength={3} maxLength={50}
                             className="form-control" type="text"/>
                    </Inp>
                    <Inp col="4 no-padding p-r-0" label="House / Apartment No.">
                      <input value={this.state.current_house_number}
                             onChange={(event) => {this.setState({current_house_number: event.target.value})}}
                             className="form-control"
                             type="text" maxLength={50} minLength={1}/>
                    </Inp>
                  </div>
                  <div className="col-sm-12">
                    <Inp col="12 no-mph" label="Are you a tenant in your current residence?">
                      <div className="col-sm-12 no-padding col-md-8 pull-right">
                        <div className="form-fields-radio">
                          <label><input type="radio" onChange={(event) => { }} checked={this.state.is_tenant === 'yes'}
                            onClick={() => { this.setState({ is_tenant: 'yes' }) }} />Yes</label>
                          <label><input type="radio" onChange={(event) => { }} checked={this.state.is_tenant === 'no'}
                            onClick={() => { this.setState({ is_tenant: 'no', company_or_landlord_name: '', company_or_landlord_number: '', company_or_landlord_email: '' }) }} />No</label>
                        </div>
                      </div>
                    </Inp>
                  </div>
                  {this.state.is_tenant === 'yes' && <div>
                    <div className="col-sm-12">
                      <h4>Landlord/House owner details ( Contact person if landlord is a company or an agent )</h4>
                    </div>
                    <Inp label="Full Name *">
                      <input data-text="true" required="required" data-fullname-validation="true"
                             value={this.state.company_or_landlord_name}
                             onChange={(event) => {this.setState({company_or_landlord_name: event.target.value})}}
                             className="form-control"
                             type="text" maxLength={50}/>
                    </Inp>
                    <Inp label="Contact No. *">
                      <input required="required" value={this.state.company_or_landlord_number}
                             onChange={(event) => {this.setState({company_or_landlord_number: event.target.value})}}
                             className="form-control"
                             type="text" maxLength={30}/>
                    </Inp>
                    <Inp label="Email Address">
                      <input value={this.state.company_or_landlord_email}
                             onChange={(event) => {this.setState({company_or_landlord_email: event.target.value ? event.target.value.toLowerCase() : ''})}}
                             className="form-control" type="email" minLength={3} maxLength={80}/>
                    </Inp>
                  </div>}
                  {this.isNepaleseNrn() && <div className="col-sm-12">
                    <Inp label="Is current address same as permanent address ( as per Citizenship Certificate )?" col="12 no-mph">
                      <div className="col-sm-12 no-padding col-md-5 pull-right">
                        <div className="form-fields-radio pull-left">
                          <label><input type="radio" onChange={(event) => { }}
                            checked={this.state.current_address_same_as_permanent === 'yes'}
                            onClick={() => { this.setState({ current_address_same_as_permanent: 'yes', permanent_street: '', permanent_house_number: '', permanent_district_name: '', permanent_town: '', permanent_municipality_name: '', permanent_ward_number: '', }) }} />Yes</label>
                          <label><input type="radio" onChange={(event) => { }}
                            checked={this.state.current_address_same_as_permanent === 'no'}
                            onClick={() => { this.setState({ current_address_same_as_permanent: 'no' }) }} />No</label>
                        </div>
                      </div>
                      <div className="row">
                        {this.state.current_address_same_as_permanent === 'no' && <div>
                          <Inp label="District *">
                            <Select className="capitalable" searchable value={this.state.permanent_district_name}
                              onChange={(value) => {
                                this.setState({
                                  permanent_district_name: value.value,
                                  permanent_municipality_name: '',
                                })
                              }} options={Districts} />
                            <input required="required"
                              type="hidden"
                              className="hidden-input"
                              data-should-be={this.state.voters_data.permanent_district}
                              data-error-message={'District ID on voter\'s ID is ' + this.state.voters_data.permanent_district}
                              value={this.state.permanent_district_name} />
                          </Inp>
                          <Inp label="Municipality / VDC *">
                            <Select className="capitalable" searchable={false} value={this.state.permanent_municipality_name}
                              onChange={(value) => {
                                this.setState({
                                  permanent_municipality_name: value.label
                                })
                              }}
                              options={this.getChildLocation(this.state.permanent_district_name)} />
                            <input required="required" type="hidden" className="hidden-input"
                              data-should-be={this.state.voters_data.permanent_municipality}
                              data-error-message={'District ID on voter\'s ID is ' + this.state.voters_data.permanent_municipality}
                              value={this.state.permanent_municipality_name} />
                          </Inp>
                          <Inp label="Ward No. *">
                            <input required="required" value={this.state.permanent_ward_number}
                              onChange={(event) => { this.setState({ permanent_ward_number: event.target.value }) }}
                              className="form-control" type="number" max={40} min={1} />
                          </Inp>
                          <Clear />
                          <Inp label="Town / City *">
                            <input data-text="true" required="required" value={this.state.permanent_town}
                              onChange={(event) => { this.setState({ permanent_town: event.target.value }) }}
                              className="form-control" type="text" minLength={3} maxLength={50} />
                          </Inp>
                          <Inp label="Street / Tole">
                            <input data-text="true" value={this.state.permanent_street}
                              onChange={(event) => { this.setState({ permanent_street: event.target.value }) }}
                              className="form-control" type="text" minLength={3} maxLength={40} />
                          </Inp>

                          <Inp label="House / Apartment No.">
                            <input value={this.state.permanent_house_number}
                              onChange={(event) => { this.setState({ permanent_house_number: event.target.value }) }}
                              className="form-control"
                              type="text" maxLength={50} minLength={1} />
                          </Inp>
                        </div>}
                      </div>
                    </Inp>
                    <div>
                      <Inp label="Is your communication address different than your current and permanent address?"
                        col="12 no-mph">
                        <div className="col-sm-12 col-md-5 no-padding pull-right">
                          <div className="form-fields-radio pull-left">
                            <label><input type="radio" onChange={(event)=>{}}
                                          checked={this.state.current_address_different_as_communication === 'yes'}
                                          onClick={() => {this.setState({current_address_different_as_communication: 'yes'})}}/>Yes</label>
                            <label><input type="radio" onChange={(event)=>{}}
                                          checked={this.state.current_address_different_as_communication === 'no'}
                                          onClick={() => { this.setState({ communication_country: '', communication_email: '', communication_p_o_b_number:'', communication_town: '', communication_street: '', communication_house_number: '', current_address_different_as_communication: 'no'})}}/>No</label>
                          </div>
                        </div>
                            <div className="row">
                          {this.state.current_address_different_as_communication === 'yes' && <div>
                            <Inp label="Country *">
                              <Select className="capitalable" searchable matchPos="start" value={this.state.communication_country}
                                onChange={(event) => { this.setState({ communication_country: event.value }) }}
                                options={Countries} />
                              <input type="hidden" required="required" value={this.state.communication_country} />
                            </Inp>
                            <Inp label="Town / City / State * ">
                              <input required="required" data-text="true" value={this.state.communication_town}
                                onChange={(event) => { this.setState({ communication_town: event.target.value }) }}
                                className="form-control" type="text" minLength={3} maxLength={50} />
                            </Inp>
                            <Inp label="Street / Tole *">
                              <input required='required' data-text="true" value={this.state.communication_street}
                                onChange={(event) => { this.setState({ communication_street: event.target.value }) }}
                                className="form-control"
                                type="text" minLength={3} maxLength={40} />
                            </Inp>
                            <Inp label="House / Apartment No. * ">
                              <input required="required" value={this.state.communication_house_number}
                                onChange={(event) => { this.setState({ communication_house_number: event.target.value }) }}
                                className="form-control"
                                type="text" max={50} min={1} />
                            </Inp>
                            <Inp label="Email Address *">
                              <input required="required" value={this.state.communication_email}
                                onChange={(event) => { this.setState({ communication_email: event.target.value ? event.target.value.toLowerCase() : '' }) }}
                                className="form-control"
                                type="email" minLength={3} maxLength={80} />
                            </Inp>
                            <Inp label="P.O.B. No. / Postal Code">
                              <input value={this.state.communication_p_o_b_number}
                                onChange={(event) => { this.setState({ communication_p_o_b_number: event.target.value }) }}
                                className="form-control" type="text" minLength={3} maxLength={60} />
                            </Inp>
                          </div>}
                        </div>
                      </Inp>
                    </div>
                  </div>}
                  <div className="wrapper-div verify-wrapper">
                    <h4 className="align-left form-heading">Address Verification</h4>
                    <input value={this.state.location_search}
                           onChange={(event) => {this.setState({location_search: event.target.value})}}
                           type="text" className="form-control controls" id="location_search"
                           placeholder="Search your location"/>
                    <div id="gmap" style={{width: '100%', height: '300px'}}></div>
                  </div>
                </form>}
                {this.state.activeStep === 4 && <form>
                  <Inp label="Profession">
                    <input data-text="true" value={this.state.profession}
                           onChange={(event) => {this.setState({profession: event.target.value})}}
                           className="form-control" type="text" minLength={3} maxLength={30}/>
                  </Inp>
                  <Inp label="Employment *">
                    <Select className="capitalable" searchable={false} value={this.state.employment_type}
                            onChange={(event) => {
                              this.setState((prev, props)=>{
                                let obj = { employment_type: event ? event.value : '' };
                                if (event && event.value !== prev.employment_type){
                                  obj = {...obj, nature_of_business: '',
                                  nature_of_business: '',
                                  other_nature_of_business: '',
                                  pan_number_business: '',
                                  other_employment: ''}
                                }
                                if(event && event.value &&event.value.toLowerCase() !== 'both'){
                                  obj = { ...obj, organization_name_2: '', organization_address_2: '', organization_estimate_annual_income_2: '', organization_designation_2: ''};
                                }
                                return obj;
                              });
                              }}
                            options={optionEmploymentType}/>
                    <input required="required" type="hidden" className="hidden-input"
                           value={this.state.employment_type}/>
                  </Inp>
                  {(this.state.employment_type === 'both' || this.state.employment_type === 'business') &&
                  <div>
                    <Inp label="Nature Of Business *">
                      <Select className="capitalable" 
                      searchable={false} value={this.state.nature_of_business}
                              onChange={(event) => {event && this.setState((prev)=>{
                                let obj = { nature_of_business: event.value };
                                if (prev.nature_of_business && prev.nature_of_business.toLowerCase() != event.value && prev.nature_of_business.toLowerCase() != 'other'){
                                  obj = { ...obj, other_nature_of_business: ''}
                                }
                                return obj;
                              })}}
                              options={optionNatureOfBusiness}/>
                      <input required="required" type="hidden" className="hidden-input"
                             value={this.state.nature_of_business}/>
                    </Inp>
                    {this.state.nature_of_business === 'other' && <div>
                      <Inp label="Please Specify">
                        <input value={this.state.other_nature_of_business}
                               onChange={(event) => {this.setState({other_nature_of_business: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={100}/>
                      </Inp>
                    </div>}
                    <Inp label="PAN/VAT No. *">
                      <input required="required" value={this.state.pan_number_business}
                             onChange={(event) => {this.setState({pan_number_business: event.target.value})}}
                             className="form-control" type="text" minLength={3} maxLength={40}/>
                    </Inp>
                  </div>}
                  {this.state.employment_type === 'other' && <div>
                    <Inp label="Please Specify *">
                      <input required value={this.state.other_employment}
                             onChange={(event) => {this.setState({other_employment: event.target.value})}}
                             className="form-control" type="text" minLength={3} maxLength={100}/>
                    </Inp>
                  </div>}
                  <div className='wrapper-div'>
                    <div className="card-content table-responsive">
                      <table className="table table-hover">
                        <tbody>
                        <tr></tr>
                        <tr>
                          <td>S.No.</td>
                          <td>Organization's Name</td>
                          <td>Address</td>
                          <td>Designation</td>
                          <td>Estimated Anual Income</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_name_1}
                                     onChange={ (event) => {this.setState({organization_name_1: event.target.value})} }
                                     className="form-control"
                                     type="text" minLength={2} maxLength={80}/>
                            </div>
                          </td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_address_1}
                                     onChange={ (event) => {this.setState({organization_address_1: event.target.value})} }
                                     className="form-control"
                                     type="text" minLength={3} maxLength={40}/>
                            </div>
                          </td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_designation_1}
                                     onChange={ (event) => {this.setState({organization_designation_1: event.target.value})} }
                                     className="form-control" type="text" minLength={2} maxLength={40}/>
                            </div>
                          </td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_estimate_annual_income_1}
                                     onChange={ (event) => {this.setState({organization_estimate_annual_income_1: event.target.value})} }
                                     className="form-control" type="text" minLength={3} maxLength={100}/>
                            </div>
                          </td>
                        </tr>
                        {this.state.employment_type === 'both' && <tr >
                          <td>2</td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_name_2}
                                     onChange={ (event) => {this.setState({organization_name_2: event.target.value})} }
                                     className="form-control"
                                     type="text" minLength={2} maxLength={80}/>
                            </div>
                          </td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_address_2}
                                     onChange={ (event) => {this.setState({organization_address_2: event.target.value})} }
                                     className="form-control"
                                     type="text" minLength={3} maxLength={40}/>
                            </div>
                          </td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_designation_2}
                                     onChange={ (event) => {this.setState({organization_designation_2: event.target.value})} }
                                     className="form-control" type="text" minLength={2} maxLength={40}/>
                            </div>
                          </td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.organization_estimate_annual_income_2}
                                     onChange={ (event) => {this.setState({organization_estimate_annual_income_2: event.target.value})} }
                                     className="form-control" type="text" minLength={3} maxLength={100}/>
                            </div>
                          </td>
                        </tr>}
                        <tr>
                          <td>{ this.state.employment_type === 'both' ? '3' : '2' }</td>
                          <td>Other Income Source</td>
                          <td colSpan="2">
                            <div className="form-group">
                              <input value={this.state.other_income_source}
                                     onChange={ (event) => {this.setState({other_income_source: event.target.value})} }
                                     className="form-control"
                                     type="text" minLength={3} maxLength={100}/>
                            </div>
                          </td>
                          <td>
                            <div className="form-group">
                              <input value={this.state.other_estimated_annual_income}
                                     onChange={ (event) => {this.setState({other_estimated_annual_income: event.target.value})} }
                                     className="form-control"
                                     type="text" minLength={3} maxLength={100}/>
                            </div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </form>}
                {this.state.activeStep === 5 && <form>
                  {this.isNotMinor() && <div className="form-group no-mph">
                    <label className="label-control">Marital Status</label>
                    <div className="col-sm-12 col-md-10 pull-right no-padding">
                        <div className="form-fields-radio">
                          <div className="col-sm-4">
                            <label><input type="radio" onChange={(event) => { }} checked={this.state.marital_status === 'married'}
                              onClick={() => { this.setState({ marital_status: 'married' }) }} />Married</label>
                          </div>
                          <div className="col-sm-4">
                            <label><input type="radio" onChange={(event)=>{}} checked={this.state.marital_status === 'unmarried'}
                                          onClick={() => {this.setState({
                                            marital_status: 'unmarried',
                                            spouse_name: undefined,
                                            son_name_1: undefined,
                                            son_name_2: undefined,
                                            son_name_3: undefined,
                                            daughter_name_1: undefined,
                                            daughter_name_2: undefined,
                                            daughter_name_3: undefined,
                                            daughter_in_law_name_1: undefined,
                                            daughter_in_law_name_2: undefined,
                                            daughter_in_law_name_3: undefined,
                                            mother_in_law: undefined,
                                            father_in_law: undefined,
                                            })}}/>Unmarried</label>
                          </div>
                        </div>
                    </div>
                  </div>}
                  <div className="card-content col-xs-12 no-padding wrapper-div">
                    <table className="table table-hover family-table">
                      <thead>
                      <tr>
                        <th><strong>S.No.</strong></th>
                        <th><strong>Relation</strong></th>
                        <th><strong>Full Name</strong></th>
                      </tr>
                      </thead>
                      <tbody className="no-absolute">
                      {this.isNotMinor() && this.state.marital_status === 'married' &&
                      <Ttr name="Spouse *">
                        <input required="required" data-fullname-validation="true" value={this.state.spouse_name }
                               onChange={(event) => {this.setState({spouse_name: event.target.value})}}
                               data-should-be={this.state.voters_data.spouse_name}
                               data-error-message={'Spouse\'s Name on voter\'s ID is ' + this.state.voters_data.spouse_name}
                               className="form-control" type="text" minLength={3} maxLength={40}/>

                      </Ttr>}
                      <Ttr name="Father *">
                        <input required="required" data-fullname-validation="true" value={this.state.father_name}
                               onChange={ (event) => {this.setState({father_name: event.target.value})} }
                               data-should-be={this.state.voters_data.father_name}
                               data-error-message={'Father\'s Name on voter\'s ID is ' + this.state.voters_data.father_name}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>
                      <Ttr name="Mother *">
                        <input required="required" data-fullname-validation="true" value={this.state.mother_name}
                               onChange={ (event) => {this.setState({mother_name: event.target.value})} }
                               data-should-be={this.state.voters_data.mother_name}
                               data-error-message={'Mother\'s Name on voter\'s ID is ' + this.state.voters_data.mother_name}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>
                      <Ttr name="Grandfather *">
                        <input required="required" data-fullname-validation="true" value={this.state.grand_father_name}
                               onChange={ (event) => {this.setState({grand_father_name: event.target.value})} }
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>
                      <Ttr name="Grandmother ">
                        <input data-fullname-validation="true" value={this.state.grand_mother_name}
                               onChange={ (event) => {this.setState({grand_mother_name: event.target.value})} }
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>
                      {this.isNotMinor() && this.state.marital_status === 'married' && <Ttr name="Son 1">
                        <input data-fullname-validation="true" value={this.state.son_name_1 }
                               onChange={(event) => {this.setState({son_name_1: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' && <Ttr name="Son 2">
                        <input data-fullname-validation="true" value={this.state.son_name_2 }
                               onChange={(event) => {this.setState({son_name_2: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' && <Ttr name="Son 3">
                        <input data-fullname-validation="true" value={this.state.son_name_3 }
                               onChange={(event) => {this.setState({son_name_3: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' && <Ttr name="Daughter 1">
                        <input data-fullname-validation="true" value={this.state.daughter_name_1 }
                               onChange={(event) => {this.setState({daughter_name_1: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' && <Ttr name="Daughter 2">
                        <input data-fullname-validation="true" value={this.state.daughter_name_2 }
                               onChange={(event) => {this.setState({daughter_name_2: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' && <Ttr name="Daughter 3">
                        <input data-fullname-validation="true" value={this.state.daughter_name_3 }
                               onChange={(event) => {this.setState({daughter_name_3: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' &&
                      <Ttr name="Daughter in law 1">
                        <input data-fullname-validation="true" value={this.state.daughter_in_law_name_1 }
                               onChange={(event) => {this.setState({daughter_in_law_name_1: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' &&
                      <Ttr name="Daughter in law 2">
                        <input data-fullname-validation="true" value={this.state.daughter_in_law_name_2 }
                               onChange={(event) => {this.setState({daughter_in_law_name_2: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' &&
                      <Ttr name="Daughter in law 3">
                        <input data-fullname-validation="true" value={this.state.daughter_in_law_name_3 }
                               onChange={(event) => {this.setState({daughter_in_law_name_3: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' && this.state.gender === 'female' &&
                      <Ttr name="Father in law *">
                        <input data-fullname-validation="true" required="required" value={this.state.father_in_law }
                               onChange={(event) => {this.setState({father_in_law: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      {this.isNotMinor() && this.state.marital_status === 'married' && this.state.gender === 'female' &&
                      <Ttr name="Mother in law *">
                        <input data-fullname-validation="true" required="required" value={this.state.mother_in_law}
                               onChange={(event) => {this.setState({mother_in_law: event.target.value})}}
                               className="form-control" type="text" minLength={3} maxLength={40}/>
                      </Ttr>}
                      </tbody>
                    </table>
                  </div>
                </form>}
                {this.state.activeStep === 6 && <form>
                  <Inp label="Religion">
                    <Select className="capitalable" searchable={false} value={this.state.religion}
                            onChange={(event) => {this.setState({religion: event.value, other_religion: event.value.toLowerCase() != 'other' ? '' : this.state.other_religion})}}
                            options={optionReligion}/>
                  </Inp>
                  {this.state.religion === 'other' && <Inp label="Please Specify">
                    <input value={this.state.other_religion}
                           onChange={(event) => {this.setState({other_religion: event.target.value})}}
                           className="form-control" type="text" minLength={3} maxLength={60}/>
                  </Inp>}
                  <Inp label="Education Qualification">
                    <Select className="capitalable" searchable={false} value={this.state.education}
                            onChange={(event) => {this.setState({education: event.value})}}
                            options={optionEducation}/>
                  </Inp>

                </form>}
                {this.state.activeStep === 7 && <form>
                  <div className="col-sm-12" style={{fontStyle: 'italic', color: '#526e85'}}>Photos must be
                    less than
                    3MB.
                  </div>
                  {this.isNepaleseNrn() && <div className="wrapper-div">
                    {this.uploadField('Your photo (Passport Size) *', 'photo_person', true)}
                    {this.uploadField('Citizenship Photo *', 'photo_citizenship', true)}
                    {this.uploadField('Utility bill', 'photo_utility_bill')}
                    {(this.state.has_driving_license === 'yes') && this.uploadField('Driving license', 'photo_driving_license')}
                    {( this.state.has_voters_id === 'yes' ) && this.uploadField('Voter\'s ID', 'photo_voter_id')}
                    {( this.state.has_passport === 'yes' ) && this.uploadField('Passport', 'photo_passport')}
                  </div>}
                  {this.isNotNepaleseNrn() && this.isNotCountryIndia() && this.isNotRefugee() &&
                  <div className="wrapper-div">
                    {this.uploadField('Your photo (Passport size) *', 'foreigner_photo_person', true)}
                    {this.uploadField('Passport photo *', 'foreigner_photo_passport', true)}
                    {this.uploadField('Visa Photo *', 'foreigner_photo_visa', true)}
                    {this.uploadField('ID card *', 'foreigner_photo_id_card', true)}
                  </div> }
                  {this.isNotNepaleseNrn() && this.isCountryIndia() && <div className="wrapper-div">
                    {this.uploadField('Your photo (Passport size) *', 'indian_photo_person', true)}
                    {this.state.any_other_valid_identification === 'yes' && this.uploadField('ID card', 'indian_photo_id_card') }
                    {this.uploadField('Registration letter form Indian embassy', 'indian_reg_letter_from_embassy')}
                    {this.uploadField('Other document', 'indian_photo_any_other_documents')}
                  </div>}
                  {this.isNotNepaleseNrn() && this.isNotCountryIndia() && this.isRefugee() &&
                  <div className="wrapper-div">
                    {this.uploadField('Your photo (Passport size) *', 'refugee_photo_person', true)}
                    {this.uploadField('ID card *', 'refugee_photo_id_card', true)}
                    {this.uploadField('Card issued by Nepal Government *', 'refugee_photo_id_card_issued_by_nepal_govt', true)}
                  </div>}
                </form>}
                {this.state.activeStep === 8 && <form>
                  <div className="wrapper-div no-mph">
                    {this.uploadField('Signature *', 'client_signature', true)}
                    <Clear/>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 thumby" style={{marginTop: '10px'}}>
                      <h5>Thumb Prints</h5>
                      {this.uploadField('Left *', 'thumb_image_left', true)}
                      {this.uploadField('Right *', 'thumb_image_right', true)}
                    </div>
                    <Clear/>
                    <div className="col-sm-12" style={{marginTop: '15px'}}>
                      <p className="text-left">
                        <label>
                          <input required type="checkbox" checked={this.state.declaration_ok}
                                 onChange={(event, value) => {this.setState({declaration_ok: !this.state.declaration_ok})}}/>
                          &nbsp; I hereby declare that the information provided here is complete, correct, and
                          true to the best of my knowledge and belief. I authorize KYCNEPAL to make any
                          enquiries
                          regarding the information provided.
                        </label>
                      </p>
                    </div>

                  </div>
                  <div className="col-sm-12" style={{marginTop: '15px'}}>
                    <div className="form-group">
                      <label style={{fontWeight: 'normal'}}>Are you sure? You will not be able to edit your details after
                        submission. Your
                        details
                        will
                        be verified by KYCNEPAL admin and you will be notified within 24 hrs.</label>
                      <div className="form-fields-radio submit-verification">
                        <label><input type="radio" onChange={(event)=>{}} checked={this.state.submit_verification === 'yes'}
                                      onClick={() => {this.setState({submit_verification: 'yes'})}}/>Yes</label>
                        <label className="no-mph"><input type="radio" onChange={(event)=>{}} checked={this.state.submit_verification === 'no'}
                                      onClick={() => {this.setState({submit_verification: 'no'})}}/>No</label>
                      </div>
                    </div>
                  </div>
                </form>}
              </div>
            </div>
            <div className='wizard-actions'>
              <div className='pull-left'>All fields marked with an asterisk (*) are required.</div>
              {this.state.activeStep > 1 &&
              <button onClick={() => {this.state.activeStep > 1 && this.changeStep(this.state.activeStep - 1)}}
                      className="btn btn-primary">
                  <span className="btn-label">
                  <i className="material-icons">keyboard_arrow_left</i>
                  </span>Prev
              </button>}
              {this.state.activeStep < 8 &&
              <button onClick={ () => {if (!Validation()) {this.changeStep(this.state.activeStep + 1)}} }
                      className="btn btn-primary"> Next <span className="btn-label btn-label-right"> <i
                className="material-icons">keyboard_arrow_right</i> </span>
              </button>}
              {this.state.activeStep === 8 && this.props.action &&
              <button disabled={this.state.submit_verification !== 'yes'} ref={(ref) => {this.submitref = ref}}
                      onClick={this.submitForVerification}
                      className="btn btn-primary">Submit</button>}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
