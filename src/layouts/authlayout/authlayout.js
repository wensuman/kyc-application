import React from 'react'
import IsLoggedIn from '../../modules/sessions'
import { Link } from 'react-router'

export class AuthLayout extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    if (IsLoggedIn()) {
      this.props.router.push('/')
      return (
        <div>
          Redirecting ...
        </div>
      )
    }
    return (
      <div className="additional">
        <div className="overlay"></div>
        <div className="site-branding">
          <Link to="/" className="custom-logo-link" rel="home">
            <img src="/assets/images/final_kyc_logo.png" alt="LOGO"/>
          </Link>
        </div>
        <div className={'container-page '+this.props.router.location.pathname.split('/').pop()+' '+this.props.router.location.pathname.split('/').pop()+'-container for-' + this.props.router.location.pathname.split('/').pop()}>
          <div className="box"></div>
          <div className="container-forms">
            <div className="container-form">
              <div className="form-item log-in">
                <div className="table">
                  <div className="table-cell">
                    {this.props.children}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

AuthLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default AuthLayout
