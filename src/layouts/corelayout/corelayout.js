import React from 'react'
import Sidebar from '../../components/sidebar'
import IsLoggedIn from '../../modules/sessions'
import Nav from '../../components/nav'

export class CoreLayout extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      template: ''
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount () {

    (function () {
      jQuery('ul.nav:nth-child(2) li a').click(function () {
        jQuery('.toggled').click()
      })
      jQuery('html').removeClass('nav-open')
      let mobile_menu_visible = 0
      jQuery('.close-layer').click(function () {
        jQuery('html').removeClass('nav-open')
        mobile_menu_visible = 0
        jQuery('.close-layer').removeClass('visible')
        setTimeout(function () {
          jQuery('.close-layer').remove()
          jQuery('.navbar-toggle').removeClass('toggled')
        }, 400);
      });
      // let isWindows = navigator.platform.indexOf('Win') > -1 ? true : false
      // if (isWindows && !jQuery('body').hasClass('sidebar-mini')) {
      //   // if we are on windows OS we activate the perfectScrollbar function
      //   // jQuery('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar()
      //
      //   // jQuery('html').addClass('perfect-scrollbar-on')
      // } else {
      //   // jQuery('html').addClass('')
      // }
      jQuery('.navbar-toggle').click(function () {
        if (mobile_menu_visible === 1) {
          jQuery('html').removeClass('nav-open')
          jQuery('.close-layer').remove()
          setTimeout(function () {
            jQuery('.navbar-toggle').removeClass('toggled')
          }, 400)

          mobile_menu_visible = 0
        } else {
          setTimeout(function () {
            jQuery('.navbar-toggle').addClass('toggled')
          }, 430)
          jQuery('.close-layer').css('height', jQuery('.main-panel')[0].scrollHeight + 'px')
          jQuery('.close-layer').appendTo('.main-panel')
          setTimeout(function () {
            jQuery('.close-layer').addClass('visible')
          }, 100)
          jQuery('html').addClass('nav-open')
          mobile_menu_visible = 1
        }
      })
      // toggle_initialized = true
    })()
  }

  render () {
    if (!IsLoggedIn()) {
      this.props.router.push('/login')
      return (<div></div>)
    }
    return (
      <div className="animated fadeIn">
        <div className="wrapper">
          <Sidebar template={this.state.template} router={this.props.router}/>
          <div className="main-panel">
          <Nav/>
            <div className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12" style={{position: 'relative', height: '100%'}}>
                    {this.props.children}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default CoreLayout
