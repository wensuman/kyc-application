import React from 'react'

export class ErrorLayout extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {

    return (
      <div>
            {this.props.children}
      </div>
    )
  }
}

ErrorLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default ErrorLayout
