export const Countries = [{label: 'Afghanistan', value: 'Afghanistan'},
  {label: 'Åland Islands', value: 'Åland Islands'},
  {label: 'Albania', value: 'Albania'},
  {label: 'Algeria', value: 'Algeria'},
  {label: 'American Samoa', value: 'American Samoa'},
  {label: 'Andorra', value: 'Andorra'},
  {label: 'Angola', value: 'Angola'},
  {label: 'Anguilla', value: 'Anguilla'},
  {label: 'Antarctica', value: 'Antarctica'},
  {label: 'Antigua and Barbuda', value: 'Antigua and Barbuda'},
  {label: 'Argentina', value: 'Argentina'},
  {label: 'Armenia', value: 'Armenia'},
  {label: 'Aruba', value: 'Aruba'},
  {label: 'Australia', value: 'Australia'},
  {label: 'Austria', value: 'Austria'},
  {label: 'Azerbaijan', value: 'Azerbaijan'},
  {label: 'Bahamas', value: 'Bahamas'},
  {label: 'Bahrain', value: 'Bahrain'},
  {label: 'Bangladesh', value: 'Bangladesh'},
  {label: 'Barbados', value: 'Barbados'},
  {label: 'Belarus', value: 'Belarus'},
  {label: 'Belgium', value: 'Belgium'},
  {label: 'Belize', value: 'Belize'},
  {label: 'Benin', value: 'Benin'},
  {label: 'Bermuda', value: 'Bermuda'},
  {label: 'Bhutan', value: 'Bhutan'},
  {label: 'Bolivia, Plurinational State of', value: 'Bolivia, Plurinational State of'},
  {label: 'Bonaire, Sint Eustatius and Saba', value: 'Bonaire, Sint Eustatius and Saba'},
  {label: 'Bosnia and Herzegovina', value: 'Bosnia and Herzegovina'},
  {label: 'Botswana', value: 'Botswana'},
  {label: 'Bouvet Island', value: 'Bouvet Island'},
  {label: 'Brazil', value: 'Brazil'},
  {label: 'British Indian Ocean Territory', value: 'British Indian Ocean Territory'},
  {label: 'Brunei Darussalam', value: 'Brunei Darussalam'},
  {label: 'Bulgaria', value: 'Bulgaria'},
  {label: 'Burkina Faso', value: 'Burkina Faso'},
  {label: 'Burundi', value: 'Burundi'},
  {label: 'Cambodia', value: 'Cambodia'},
  {label: 'Cameroon', value: 'Cameroon'},
  {label: 'Canada', value: 'Canada'},
  {label: 'Cape Verde', value: 'Cape Verde'},
  {label: 'Cayman Islands', value: 'Cayman Islands'},
  {label: 'Central African Republic', value: 'Central African Republic'},
  {label: 'Chad', value: 'Chad'},
  {label: 'Chile', value: 'Chile'},
  {label: 'China', value: 'China'},
  {label: 'Christmas Island', value: 'Christmas Island'},
  {label: 'Cocos (Keeling) Islands', value: 'Cocos (Keeling) Islands'},
  {label: 'Colombia', value: 'Colombia'},
  {label: 'Comoros', value: 'Comoros'},
  {label: 'Congo', value: 'Congo'},
  {label: 'Congo, the Democratic Republic of the', value: 'Congo, the Democratic Republic of the'},
  {label: 'Cook Islands', value: 'Cook Islands'},
  {label: 'Costa Rica', value: 'Costa Rica'},
  {label: 'Côte d\'Ivoire', value: 'Côte d\'Ivoire'},
  {label: 'Croatia', value: 'Croatia'},
  {label: 'Cuba', value: 'Cuba'},
  {label: 'Curaçao', value: 'Curaçao'},
  {label: 'Cyprus', value: 'Cyprus'},
  {label: 'Czech Republic', value: 'Czech Republic'},
  {label: 'Denmark', value: 'Denmark'},
  {label: 'Djibouti', value: 'Djibouti'},
  {label: 'Dominica', value: 'Dominica'},
  {label: 'Dominican Republic', value: 'Dominican Republic'},
  {label: 'Ecuador', value: 'Ecuador'},
  {label: 'Egypt', value: 'Egypt'},
  {label: 'El Salvador', value: 'El Salvador'},
  {label: 'Equatorial Guinea', value: 'Equatorial Guinea'},
  {label: 'Eritrea', value: 'Eritrea'},
  {label: 'Estonia', value: 'Estonia'},
  {label: 'Ethiopia', value: 'Ethiopia'},
  {label: 'Falkland Islands (Malvinas)', value: 'Falkland Islands (Malvinas)'},
  {label: 'Faroe Islands', value: 'Faroe Islands'},
  {label: 'Fiji', value: 'Fiji'},
  {label: 'Finland', value: 'Finland'},
  {label: 'France', value: 'France'},
  {label: 'French Guiana', value: 'French Guiana'},
  {label: 'French Polynesia', value: 'French Polynesia'},
  {label: 'French Southern Territories', value: 'French Southern Territories'},
  {label: 'Gabon', value: 'Gabon'},
  {label: 'Gambia', value: 'Gambia'},
  {label: 'Georgia', value: 'Georgia'},
  {label: 'Germany', value: 'Germany'},
  {label: 'Ghana', value: 'Ghana'},
  {label: 'Gibraltar', value: 'Gibraltar'},
  {label: 'Greece', value: 'Greece'},
  {label: 'Greenland', value: 'Greenland'},
  {label: 'Grenada', value: 'Grenada'},
  {label: 'Guadeloupe', value: 'Guadeloupe'},
  {label: 'Guam', value: 'Guam'},
  {label: 'Guatemala', value: 'Guatemala'},
  {label: 'Guernsey', value: 'Guernsey'},
  {label: 'Guinea', value: 'Guinea'},
  {label: 'Guinea-Bissau', value: 'Guinea-Bissau'},
  {label: 'Guyana', value: 'Guyana'},
  {label: 'Haiti', value: 'Haiti'},
  {label: 'Heard Island and McDonald Islands', value: 'Heard Island and McDonald Islands'},
  {label: 'Holy See (Vatican City State)', value: 'Holy See (Vatican City State)'},
  {label: 'Honduras', value: 'Honduras'},
  {label: 'Hong Kong', value: 'Hong Kong'},
  {label: 'Hungary', value: 'Hungary'},
  {label: 'Iceland', value: 'Iceland'},
  {label: 'India', value: 'India'},
  {label: 'Indonesia', value: 'Indonesia'},
  {label: 'Iran, Islamic Republic of', value: 'Iran, Islamic Republic of'},
  {label: 'Iraq', value: 'Iraq'},
  {label: 'Ireland', value: 'Ireland'},
  {label: 'Isle of Man', value: 'Isle of Man'},
  {label: 'Israel', value: 'Israel'},
  {label: 'Italy', value: 'Italy'},
  {label: 'Jamaica', value: 'Jamaica'},
  {label: 'Japan', value: 'Japan'},
  {label: 'Jersey', value: 'Jersey'},
  {label: 'Jordan', value: 'Jordan'},
  {label: 'Kazakhstan', value: 'Kazakhstan'},
  {label: 'Kenya', value: 'Kenya'},
  {label: 'Kiribati', value: 'Kiribati'},
  {label: 'Korea, Democratic People\'s Republic of', value: 'Korea, Democratic People\'s Republic of'},
  {label: 'Korea, Republic of', value: 'Korea, Republic of'},
  {label: 'Kuwait', value: 'Kuwait'},
  {label: 'Kyrgyzstan', value: 'Kyrgyzstan'},
  {label: 'Lao People\'s Democratic Republic', value: 'Lao People\'s Democratic Republic'},
  {label: 'Latvia', value: 'Latvia'},
  {label: 'Lebanon', value: 'Lebanon'},
  {label: 'Lesotho', value: 'Lesotho'},
  {label: 'Liberia', value: 'Liberia'},
  {label: 'Libya', value: 'Libya'},
  {label: 'Liechtenstein', value: 'Liechtenstein'},
  {label: 'Lithuania', value: 'Lithuania'},
  {label: 'Luxembourg', value: 'Luxembourg'},
  {label: 'Macao', value: 'Macao'},
  {label: 'Macedonia, the former Yugoslav Republic of', value: 'Macedonia, the former Yugoslav Republic ,of'},
  {label: 'Madagascar', value: 'Madagascar'},
  {label: 'Malawi', value: 'Malawi'},
  {label: 'Malaysia', value: 'Malaysia'},
  {label: 'Maldives', value: 'Maldives'},
  {label: 'Mali', value: 'Mali'},
  {label: 'Malta', value: 'Malta'},
  {label: 'Marshall Islands', value: 'Marshall Islands'},
  {label: 'Martinique', value: 'Martinique'},
  {label: 'Mauritania', value: 'Mauritania'},
  {label: 'Mauritius', value: 'Mauritius'},
  {label: 'Mayotte', value: 'Mayotte'},
  {label: 'Mexico', value: 'Mexico'},
  {label: 'Micronesia, Federated States of', value: 'Micronesia, Federated States of'},
  {label: 'Moldova, Republic of', value: 'Moldova, Republic of'},
  {label: 'Monaco', value: 'Monaco'},
  {label: 'Mongolia', value: 'Mongolia'},
  {label: 'Montenegro', value: 'Montenegro'},
  {label: 'Montserrat', value: 'Montserrat'},
  {label: 'Morocco', value: 'Morocco'},
  {label: 'Mozambique', value: 'Mozambique'},
  {label: 'Myanmar', value: 'Myanmar'},
  {label: 'Namibia', value: 'Namibia'},
  {label: 'Nauru', value: 'Nauru'},
  {label: 'Nepal', value: 'Nepal'},
  {label: 'Netherlands', value: 'Netherlands'},
  {label: 'New Caledonia', value: 'New Caledonia'},
  {label: 'New Zealand', value: 'New Zealand'},
  {label: 'Nicaragua', value: 'Nicaragua'},
  {label: 'Niger', value: 'Niger'},
  {label: 'Nigeria', value: 'Nigeria'},
  {label: 'Niue', value: 'Niue'},
  {label: 'Norfolk Island', value: 'Norfolk Island'},
  {label: 'Northern Mariana Islands', value: 'Northern Mariana Islands'},
  {label: 'Norway', value: 'Norway'},
  {label: 'Oman', value: 'Oman'},
  {label: 'Pakistan', value: 'Pakistan'},
  {label: 'Palau', value: 'Palau'},
  {label: 'Palestinian Territory, Occupied', value: 'Palestinian Territory, Occupied'},
  {label: 'Panama', value: 'Panama'},
  {label: 'Papua New Guinea', value: 'Papua New Guinea'},
  {label: 'Paraguay', value: 'Paraguay'},
  {label: 'Peru', value: 'Peru'},
  {label: 'Philippines', value: 'Philippines'},
  {label: 'Pitcairn', value: 'Pitcairn'},
  {label: 'Poland', value: 'Poland'},
  {label: 'Portugal', value: 'Portugal'},
  {label: 'Puerto Rico', value: 'Puerto Rico'},
  {label: 'Qatar', value: 'Qatar'},
  {label: 'Réunion', value: 'Réunion'},
  {label: 'Romania', value: 'Romania'},
  {label: 'Russian Federation', value: 'Russian Federation'},
  {label: 'Rwanda', value: 'Rwanda'},
  {label: 'Saint Barthélemy', value: 'Saint Barthélemy'},
  {label: 'Saint Helena, Ascension and Tristan da Cunha', value: 'Saint Helena, Ascension and Tristan da ,Cunha'},
  {label: 'Saint Kitts and Nevis', value: 'Saint Kitts and Nevis'},
  {label: 'Saint Lucia', value: 'Saint Lucia'},
  {label: 'Saint Martin (French part)', value: 'Saint Martin (French part)'},
  {label: 'Saint Pierre and Miquelon', value: 'Saint Pierre and Miquelon'},
  {label: 'Saint Vincent and the Grenadines', value: 'Saint Vincent and the Grenadines'},
  {label: 'Samoa', value: 'Samoa'},
  {label: 'San Marino', value: 'San Marino'},
  {label: 'Sao Tome and Principe', value: 'Sao Tome and Principe'},
  {label: 'Saudi Arabia', value: 'Saudi Arabia'},
  {label: 'Senegal', value: 'Senegal'},
  {label: 'Serbia', value: 'Serbia'},
  {label: 'Seychelles', value: 'Seychelles'},
  {label: 'Sierra Leone', value: 'Sierra Leone'},
  {label: 'Singapore', value: 'Singapore'},
  {label: 'Sint Maarten (Dutch part)', value: 'Sint Maarten (Dutch part)'},
  {label: 'Slovakia', value: 'Slovakia'},
  {label: 'Slovenia', value: 'Slovenia'},
  {label: 'Solomon Islands', value: 'Solomon Islands'},
  {label: 'Somalia', value: 'Somalia'},
  {label: 'South Africa', value: 'South Africa'},
  {label: 'South Georgia and the South Sandwich Islands', value: 'South Georgia and the South Sandwich ,Islands'},
  {label: 'South Sudan', value: 'South Sudan'},
  {label: 'Spain', value: 'Spain'},
  {label: 'Sri Lanka', value: 'Sri Lanka'},
  {label: 'Sudan', value: 'Sudan'},
  {label: 'Suriname', value: 'Suriname'},
  {label: 'Svalbard and Jan Mayen', value: 'Svalbard and Jan Mayen'},
  {label: 'Swaziland', value: 'Swaziland'},
  {label: 'Sweden', value: 'Sweden'},
  {label: 'Switzerland', value: 'Switzerland'},
  {label: 'Syrian Arab Republic', value: 'Syrian Arab Republic'},
  {label: 'Taiwan, Province of China', value: 'Taiwan, Province of China'},
  {label: 'Tajikistan', value: 'Tajikistan'},
  {label: 'Tanzania, United Republic of', value: 'Tanzania, United Republic of'},
  {label: 'Thailand', value: 'Thailand'},
  {label: 'Timor-Leste', value: 'Timor-Leste'},
  {label: 'Togo', value: 'Togo'},
  {label: 'Tokelau', value: 'Tokelau'},
  {label: 'Tonga', value: 'Tonga'},
  {label: 'Trinidad and Tobago', value: 'Trinidad and Tobago'},
  {label: 'Tunisia', value: 'Tunisia'},
  {label: 'Turkey', value: 'Turkey'},
  {label: 'Turkmenistan', value: 'Turkmenistan'},
  {label: 'Turks and Caicos Islands', value: 'Turks and Caicos Islands'},
  {label: 'Tuvalu', value: 'Tuvalu'},
  {label: 'Uganda', value: 'Uganda'},
  {label: 'Ukraine', value: 'Ukraine'},
  {label: 'United Arab Emirates', value: 'United Arab Emirates'},
  {label: 'United Kingdom', value: 'United Kingdom'},
  {label: 'United States', value: 'United States'},
  {label: 'United States Minor Outlying Islands', value: 'United States Minor Outlying Islands'},
  {label: 'Uruguay', value: 'Uruguay'},
  {label: 'Uzbekistan', value: 'Uzbekistan'},
  {label: 'Vanuatu', value: 'Vanuatu'},
  {label: 'Venezuela, Bolivarian Republic of', value: 'Venezuela, Bolivarian Republic of'},
  {label: 'Viet Nam', value: 'Viet Nam'},
  {label: 'Virgin Islands, British', value: 'Virgin Islands, British'},
  {label: 'Virgin Islands, U.S.', value: 'Virgin Islands, U.S.'},
  {label: 'Wallis and Futuna', value: 'Wallis and Futuna'},
  {label: 'Western Sahara', value: 'Western Sahara'},
  {label: 'Yemen', value: 'Yemen'},
  {label: 'Zambia', value: 'Zambia'},
  {label: 'Zimbabwe', value: 'Zimbabwe'}
]

export const Zones = [
  {label: 'Bagmati', value: 'Bagmati'},
  {label: 'Bheri', value: 'Bheri'},
  {label: 'Dhawalagiri', value: 'Dhawalagiri'},
  {label: 'Gandaki', value: 'Gandaki'},
  {label: 'Janakpur', value: 'Janakpur'},
  {label: 'Karnali', value: 'Karnali'},
  {label: 'Koshi', value: 'Koshi'},
  {label: 'Lumbini', value: 'Lumbini'},
  {label: 'Mahakali', value: 'Mahakali'},
  {label: 'Mechi', value: 'Mechi'},
  {label: 'Narayani', value: 'Narayani'},
  {label: 'Rapti', value: 'Rapti'},
  {label: 'Sagarmatha', value: 'Sagarmatha'},
  {label: 'Seti', value: 'Seti'},
]

export const Districts = [
  {label: 'Arghakhanchi', value: 'Arghakhanchi'},
  {label: 'Achham', value: 'Achham'},
  {label: 'Bhaktapur', value: 'Bhaktapur'},
  {label: 'Bhojpur', value: 'Bhojpur'},
  {label: 'Banke', value: 'Banke'},
  {label: 'Bardiya', value: 'Bardiya'},
  {label: 'Baglung', value: 'Baglung'},
  {label: 'Baitadi', value: 'Baitadi'},
  {label: 'Bara', value: 'Bara'},
  {label: 'Bajhang', value: 'Bajhang'},
  {label: 'Bajura', value: 'Bajura'},
  {label: 'Chitawan', value: 'Chitawan'},
  {label: 'Doti', value: 'Doti'},
  {label: 'Dang', value: 'Dang'},
  {label: 'Darchula', value: 'Darchula'},
  {label: 'Dadeldhura', value: 'Dadeldhura'},
  {label: 'Dhankuta', value: 'Dhankuta'},
  {label: 'Dailekh', value: 'Dailekh'},
  {label: 'Dhading', value: 'Dhading'},
  {label: 'Dhanusa', value: 'Dhanusa'},
  {label: 'Dolpa', value: 'Dolpa'},
  {label: 'Dolakha', value: 'Dolakha'},
  {label: 'Gorkha', value: 'Gorkha'},
  {label: 'Gulmi', value: 'Gulmi'},
  {label: 'Humla', value: 'Humla'},
  {label: 'Ilam', value: 'Ilam'},
  {label: 'Jajarkot', value: 'Jajarkot'},
  {label: 'Jhapa', value: 'Jhapa'},
  {label: 'Jumla', value: 'Jumla'},
  {label: 'Kalikot', value: 'Kalikot'},
  {label: 'Kathmandu', value: 'Kathmandu'},
  {label: 'Kapilvastu', value: 'Kapilvastu'},
  {label: 'Kavrepalanchok', value: 'Kavrepalanchok'},
  {label: 'Kanchanpur', value: 'Kanchanpur'},
  {label: 'Khotang', value: 'Khotang'},
  {label: 'Kailali', value: 'Kailali'},
  {label: 'Kaski', value: 'Kaski'},
  {label: 'Lamjung', value: 'Lamjung'},
  {label: 'Lalitpur', value: 'Lalitpur'},
  {label: 'Mahottari', value: 'Mahottari'},
  {label: 'Makawanpur', value: 'Makawanpur'},
  {label: 'Manang', value: 'Manang'},
  {label: 'Mustang', value: 'Mustang'},
  {label: 'Myagdi', value: 'Myagdi'},
  {label: 'Mugu', value: 'Mugu'},
  {label: 'Morang', value: 'Morang'},
  {label: 'Nuwakot', value: 'Nuwakot'},
  {label: 'Nawalparasi', value: 'Nawalparasi'},
  {label: 'Okhaldhunga', value: 'Okhaldhunga'},
  {label: 'Palpa', value: 'Palpa'},
  {label: 'Parbat', value: 'Parbat'},
  {label: 'Panchthar', value: 'Panchthar'},
  {label: 'Parsa', value: 'Parsa'},
  {label: 'Pyuthan', value: 'Pyuthan'},
  {label: 'Rautahat', value: 'Rautahat'},
  {label: 'Rolpa', value: 'Rolpa'},
  {label: 'Rukum', value: 'Rukum'},
  {label: 'Rasuwa', value: 'Rasuwa'},
  {label: 'Ramechhap', value: 'Ramechhap'},
  {label: 'Rupandehi', value: 'Rupandehi'},
  {label: 'Sindhupalchok', value: 'Sindhupalchok'},
  {label: 'Surkhet', value: 'Surkhet'},
  {label: 'Syangja', value: 'Syangja'},
  {label: 'Sunsari', value: 'Sunsari'},
  {label: 'Sarlahi', value: 'Sarlahi'},
  {label: 'Sindhuli', value: 'Sindhuli'},
  {label: 'Sankhuwasabha', value: 'Sankhuwasabha'},
  {label: 'Salyan', value: 'Salyan'},
  {label: 'Saptari', value: 'Saptari'},
  {label: 'Siraha', value: 'Siraha'},
  {label: 'Solukhumbu', value: 'Solukhumbu'},
  {label: 'Tanahu', value: 'Tanahu'},
  {label: 'Terhathum', value: 'Terhathum'},
  {label: 'Taplejung', value: 'Taplejung'},
  {label: 'Udayapur', value: 'Udayapur'}
]

export const ProvinceData = [
  {'LI': 1, 'LN': 'province 1', 'LT': 'State', 'P': 0}, {'LI': 2, 'LN': 'province 2', 'LT': 'State', 'P': 0}, {
    'LI': 3,
    'LN': 'province 3',
    'LT': 'State',
    'P': 0
  }, {'LI': 4, 'LN': 'province 4', 'LT': 'State', 'P': 0}, {
    'LI': 5,
    'LN': 'province 5',
    'LT': 'State',
    'P': 0
  }, {'LI': 6, 'LN': 'province 6', 'LT': 'State', 'P': 0}, {
    'LI': 7,
    'LN': 'province 7',
    'LT': 'State',
    'P': 0
  }, {'LI': 8, 'LN': 'achham', 'LT': 'District', 'P': 7}, {
    'LI': 9,
    'LN': 'arghakhanchi',
    'LT': 'District',
    'P': 5
  }, {'LI': 10, 'LN': 'baglung', 'LT': 'District', 'P': 4}, {
    'LI': 11,
    'LN': 'baitadi',
    'LT': 'District',
    'P': 7
  }, {'LI': 12, 'LN': 'bajhang', 'LT': 'District', 'P': 7}, {
    'LI': 13,
    'LN': 'bajura',
    'LT': 'District',
    'P': 7
  }, {'LI': 14, 'LN': 'banke', 'LT': 'District', 'P': 5}, {'LI': 15, 'LN': 'bara', 'LT': 'District', 'P': 2}, {
    'LI': 16,
    'LN': 'bardiya',
    'LT': 'District',
    'P': 5
  }, {'LI': 17, 'LN': 'bhaktapur', 'LT': 'District', 'P': 3}, {
    'LI': 18,
    'LN': 'bhojpur',
    'LT': 'District',
    'P': 1
  }, {'LI': 19, 'LN': 'chitawan', 'LT': 'District', 'P': 3}, {
    'LI': 20,
    'LN': 'dadeldhura',
    'LT': 'District',
    'P': 7
  }, {'LI': 21, 'LN': 'dailekh', 'LT': 'District', 'P': 6}, {
    'LI': 22,
    'LN': 'dang',
    'LT': 'District',
    'P': 5
  }, {'LI': 23, 'LN': 'darchula', 'LT': 'District', 'P': 7}, {
    'LI': 24,
    'LN': 'dhading',
    'LT': 'District',
    'P': 3
  }, {'LI': 25, 'LN': 'dhankuta', 'LT': 'District', 'P': 1}, {
    'LI': 26,
    'LN': 'dhanusa',
    'LT': 'District',
    'P': 2
  }, {'LI': 27, 'LN': 'dolakha', 'LT': 'District', 'P': 3}, {
    'LI': 28,
    'LN': 'dolpa',
    'LT': 'District',
    'P': 6
  }, {'LI': 29, 'LN': 'doti', 'LT': 'District', 'P': 7}, {
    'LI': 30,
    'LN': 'gorkha',
    'LT': 'District',
    'P': 4
  }, {'LI': 31, 'LN': 'gulmi', 'LT': 'District', 'P': 5}, {
    'LI': 32,
    'LN': 'humla',
    'LT': 'District',
    'P': 6
  }, {'LI': 33, 'LN': 'ilam', 'LT': 'District', 'P': 1}, {
    'LI': 34,
    'LN': 'jajarkot',
    'LT': 'District',
    'P': 6
  }, {'LI': 35, 'LN': 'jhapa', 'LT': 'District', 'P': 1}, {
    'LI': 36,
    'LN': 'jumla',
    'LT': 'District',
    'P': 6
  }, {'LI': 37, 'LN': 'kavrepalanchok', 'LT': 'District', 'P': 3}, {
    'LI': 38,
    'LN': 'kailali',
    'LT': 'District',
    'P': 7
  }, {'LI': 39, 'LN': 'kalikot', 'LT': 'District', 'P': 6}, {
    'LI': 40,
    'LN': 'kanchanpur',
    'LT': 'District',
    'P': 7
  }, {'LI': 41, 'LN': 'kapilvastu', 'LT': 'District', 'P': 5}, {
    'LI': 42,
    'LN': 'kaski',
    'LT': 'District',
    'P': 4
  }, {'LI': 43, 'LN': 'kathmandu', 'LT': 'District', 'P': 3}, {
    'LI': 44,
    'LN': 'khotang',
    'LT': 'District',
    'P': 1
  }, {'LI': 45, 'LN': 'lalitpur', 'LT': 'District', 'P': 3}, {
    'LI': 46,
    'LN': 'lamjung',
    'LT': 'District',
    'P': 4
  }, {'LI': 47, 'LN': 'mahottari', 'LT': 'District', 'P': 2}, {
    'LI': 48,
    'LN': 'makawanpur',
    'LT': 'District',
    'P': 3
  }, {'LI': 49, 'LN': 'manang', 'LT': 'District', 'P': 4}, {
    'LI': 50,
    'LN': 'morang',
    'LT': 'District',
    'P': 1
  }, {'LI': 51, 'LN': 'mugu', 'LT': 'District', 'P': 6}, {
    'LI': 52,
    'LN': 'mustang',
    'LT': 'District',
    'P': 4
  }, {'LI': 53, 'LN': 'myagdi', 'LT': 'District', 'P': 4}, {
    'LI': 54,
    'LN': 'nawalparasi',
    'LT': 'District',
    'P': 45
  }, {'LI': 55, 'LN': 'nuwakot', 'LT': 'District', 'P': 3}, {
    'LI': 56,
    'LN': 'okhaldhunga',
    'LT': 'District',
    'P': 1
  }, {'LI': 57, 'LN': 'palpa', 'LT': 'District', 'P': 5}, {
    'LI': 58,
    'LN': 'panchthar',
    'LT': 'District',
    'P': 1
  }, {'LI': 59, 'LN': 'parbat', 'LT': 'District', 'P': 4}, {
    'LI': 60,
    'LN': 'parsa',
    'LT': 'District',
    'P': 2
  }, {'LI': 61, 'LN': 'pyuthan', 'LT': 'District', 'P': 5}, {
    'LI': 62,
    'LN': 'ramechhap',
    'LT': 'District',
    'P': 3
  }, {'LI': 63, 'LN': 'rasuwa', 'LT': 'District', 'P': 3}, {
    'LI': 64,
    'LN': 'rautahat',
    'LT': 'District',
    'P': 2
  }, {'LI': 65, 'LN': 'rolpa', 'LT': 'District', 'P': 5}, {
    'LI': 66,
    'LN': 'rukum',
    'LT': 'District',
    'P': 56
  }, {'LI': 67, 'LN': 'rupandehi', 'LT': 'District', 'P': 5}, {
    'LI': 68,
    'LN': 'salyan',
    'LT': 'District',
    'P': 6
  }, {'LI': 69, 'LN': 'sankhuwasabha', 'LT': 'District', 'P': 1}, {
    'LI': 70,
    'LN': 'saptari',
    'LT': 'District',
    'P': 2
  }, {'LI': 71, 'LN': 'sarlahi', 'LT': 'District', 'P': 2}, {
    'LI': 72,
    'LN': 'sindhuli',
    'LT': 'District',
    'P': 3
  }, {'LI': 73, 'LN': 'sindhupalchok', 'LT': 'District', 'P': 3}, {
    'LI': 74,
    'LN': 'siraha',
    'LT': 'District',
    'P': 2
  }, {'LI': 75, 'LN': 'solukhumbu', 'LT': 'District', 'P': 1}, {
    'LI': 76,
    'LN': 'sunsari',
    'LT': 'District',
    'P': 1
  }, {'LI': 77, 'LN': 'surkhet', 'LT': 'District', 'P': 6}, {
    'LI': 78,
    'LN': 'syangja',
    'LT': 'District',
    'P': 4
  }, {'LI': 79, 'LN': 'tanahu', 'LT': 'District', 'P': 4}, {
    'LI': 80,
    'LN': 'taplejung',
    'LT': 'District',
    'P': 1
  }, {'LI': 81, 'LN': 'terhathum', 'LT': 'District', 'P': 1}, {
    'LI': 82,
    'LN': 'udayapur',
    'LT': 'District',
    'P': 1
  }, {'LI': 83, 'LN': 'bannigadhi jayagadh', 'P': 8}, {'LI': 84, 'LN': 'chaurpati', 'P': 8}, {
    'LI': 85,
    'LN': 'dhakari',
    'P': 8
  }, {'LI': 86, 'LN': 'kamalbazar', 'P': 8}, {'LI': 88, 'LN': 'mangalsen', 'P': 8}, {
    'LI': 89,
    'LN': 'mellekh',
    'P': 8
  }, {'LI': 90, 'LN': 'panchadewal binayak', 'P': 8}, {'LI': 91, 'LN': 'ramaroshan', 'P': 8}, {
    'LI': 92,
    'LN': 'sanphebagar',
    'P': 8
  }, {'LI': 93, 'LN': 'turmakhad', 'P': 8}, {'LI': 94, 'LN': 'bhumekasthan', 'P': 9}, {
    'LI': 95,
    'LN': 'chhatradev',
    'P': 9
  }, {'LI': 96, 'LN': 'malarani', 'P': 9}, {'LI': 97, 'LN': 'panini', 'P': 9}, {
    'LI': 98,
    'LN': 'sandhikharka',
    'P': 9
  }, {'LI': 99, 'LN': 'sitganga', 'P': 9}, {'LI': 100, 'LN': 'badigad', 'P': 10}, {
    'LI': 101,
    'LN': 'baglung',
    'P': 10
  }, {'LI': 102, 'LN': 'bareng', 'P': 10}, {'LI': 103, 'LN': 'dhorpatan', 'P': 10}, {
    'LI': 104,
    'LN': 'dhorpatan hunting reserve',
    'P': 10
  }, {'LI': 105, 'LN': 'galkot', 'P': 10}, {'LI': 106, 'LN': 'jaimuni', 'P': 10}, {
    'LI': 107,
    'LN': 'kanthekhola',
    'P': 10
  }, {'LI': 108, 'LN': 'nisikhola', 'P': 10}, {'LI': 109, 'LN': 'taman khola', 'P': 10}, {
    'LI': 110,
    'LN': 'tara khola',
    'P': 10
  }, {'LI': 111, 'LN': 'dasharathchanda', 'P': 11}, {'LI': 112, 'LN': 'dilasaini', 'P': 11}, {
    'LI': 113,
    'LN': 'dogadakedar',
    'P': 11
  }, {'LI': 114, 'LN': 'melauli', 'P': 11}, {'LI': 115, 'LN': 'pancheshwar', 'P': 11}, {
    'LI': 116,
    'LN': 'patan',
    'P': 11
  }, {'LI': 117, 'LN': 'purchaudi', 'P': 11}, {'LI': 118, 'LN': 'shivanath', 'P': 11}, {
    'LI': 119,
    'LN': 'sigas',
    'P': 11
  }, {'LI': 120, 'LN': 'surnaya', 'P': 11}, {'LI': 121, 'LN': 'bithadchir', 'P': 12}, {
    'LI': 122,
    'LN': 'bungal',
    'P': 12
  }, {'LI': 123, 'LN': 'chabispathivera', 'P': 12}, {'LI': 124, 'LN': 'durgathali', 'P': 12}, {
    'LI': 125,
    'LN': 'jayaprithivi',
    'P': 12
  }, {'LI': 126, 'LN': 'kanda', 'P': 12}, {'LI': 127, 'LN': 'kedarseu', 'P': 12}, {
    'LI': 129,
    'LN': 'khaptadchhanna',
    'P': 12
  }, {'LI': 130, 'LN': 'masta', 'P': 12}, {'LI': 131, 'LN': 'surma', 'P': 12}, {
    'LI': 132,
    'LN': 'talkot',
    'P': 12
  }, {'LI': 133, 'LN': 'thalara', 'P': 12}, {'LI': 134, 'LN': 'badimalika', 'P': 13}, {
    'LI': 135,
    'LN': 'budhiganga',
    'P': 13
  }, {'LI': 136, 'LN': 'budhinanda', 'P': 13}, {'LI': 137, 'LN': 'chhededaha', 'P': 13}, {
    'LI': 138,
    'LN': 'gaumul',
    'P': 13
  }, {'LI': 139, 'LN': 'himali', 'P': 13}, {'LI': 141, 'LN': 'pandav gupha', 'P': 13}, {
    'LI': 142,
    'LN': 'swami kartik',
    'P': 13
  }, {'LI': 143, 'LN': 'tribeni', 'P': 13}, {'LI': 144, 'LN': 'baijanath', 'P': 14}, {
    'LI': 145,
    'LN': 'duduwa',
    'P': 14
  }, {'LI': 146, 'LN': 'janki', 'P': 14}, {'LI': 147, 'LN': 'khajura', 'P': 14}, {
    'LI': 148,
    'LN': 'kohalpur',
    'P': 14
  }, {'LI': 149, 'LN': 'narainapur', 'P': 14}, {'LI': 150, 'LN': 'nepalgunj', 'P': 14}, {
    'LI': 151,
    'LN': 'rapti sonari',
    'P': 14
  }, {'LI': 153, 'LN': 'adarshkotwal', 'P': 15}, {'LI': 154, 'LN': 'baragadhi', 'P': 15}, {
    'LI': 155,
    'LN': 'devtal',
    'P': 15
  }, {'LI': 156, 'LN': 'jitpur simara', 'P': 15}, {'LI': 157, 'LN': 'kalaiya', 'P': 15}, {
    'LI': 158,
    'LN': 'karaiyamai',
    'P': 15
  }, {'LI': 159, 'LN': 'kolhabi', 'P': 15}, {'LI': 160, 'LN': 'mahagadhimai', 'P': 15}, {
    'LI': 161,
    'LN': 'nijgadh',
    'P': 15
  }, {'LI': 162, 'LN': 'pacharauta', 'P': 15}, {'LI': 163, 'LN': 'parwanipur', 'P': 15}, {
    'LI': 164,
    'LN': 'pheta',
    'P': 15
  }, {'LI': 165, 'LN': 'prasauni', 'P': 15}, {'LI': 166, 'LN': 'simraungadh', 'P': 15}, {
    'LI': 167,
    'LN': 'suwarna',
    'P': 15
  }, {'LI': 168, 'LN': 'badhaiyatal', 'P': 16}, {'LI': 169, 'LN': 'barbardiya', 'P': 16}, {
    'LI': 171,
    'LN': 'geruwa',
    'P': 16
  }, {'LI': 172, 'LN': 'gulariya', 'P': 16}, {'LI': 173, 'LN': 'madhuwan', 'P': 16}, {
    'LI': 174,
    'LN': 'rajapur',
    'P': 16
  }, {'LI': 175, 'LN': 'thakurbaba', 'P': 16}, {'LI': 176, 'LN': 'bansagadhi', 'P': 16}, {
    'LI': 177,
    'LN': 'bansagadhi',
    'P': 16
  }, {'LI': 178, 'LN': 'bhaktapur', 'P': 17}, {'LI': 179, 'LN': 'changunarayan', 'P': 17}, {
    'LI': 180,
    'LN': 'madhyapur thimi',
    'P': 17
  }, {'LI': 181, 'LN': 'suryabinayak', 'P': 17}, {'LI': 182, 'LN': 'aamchowk', 'P': 18}, {
    'LI': 183,
    'LN': 'arun',
    'P': 18
  }, {'LI': 184, 'LN': 'bhojpur', 'P': 18}, {'LI': 185, 'LN': 'hatuwagadhi', 'P': 18}, {
    'LI': 186,
    'LN': 'pauwadungma',
    'P': 18
  }, {'LI': 187, 'LN': 'ramprasad rai', 'P': 18}, {'LI': 188, 'LN': 'salpasilichho', 'P': 18}, {
    'LI': 189,
    'LN': 'shadananda',
    'P': 18
  }, {'LI': 190, 'LN': 'tyamkemaiyung', 'P': 18}, {'LI': 191, 'LN': 'bharatpur', 'P': 19}, {
    'LI': 193,
    'LN': 'ichchhyakamana',
    'P': 19
  }, {'LI': 194, 'LN': 'kalika', 'P': 19}, {'LI': 195, 'LN': 'khairahani', 'P': 19}, {
    'LI': 196,
    'LN': 'madi',
    'P': 19
  }, {'LI': 197, 'LN': 'rapti', 'P': 19}, {'LI': 198, 'LN': 'ratnanagar', 'P': 19}, {
    'LI': 199,
    'LN': 'ajaymeru',
    'P': 20
  }, {'LI': 200, 'LN': 'alital', 'P': 20}, {'LI': 201, 'LN': 'bhageshwar', 'P': 20}, {
    'LI': 202,
    'LN': 'ganayapdhura',
    'P': 20
  }, {'LI': 203, 'LN': 'parashuram', 'P': 20}, {'LI': 204, 'LN': 'nawadurga', 'P': 20}, {
    'LI': 205,
    'LN': 'amargadhi',
    'P': 20
  }, {'LI': 206, 'LN': 'aathabis', 'P': 21}, {'LI': 207, 'LN': 'bhagawatimai', 'P': 21}, {
    'LI': 208,
    'LN': 'bhairabi',
    'P': 21
  }, {'LI': 209, 'LN': 'chamunda bindrasaini', 'P': 21}, {'LI': 210, 'LN': 'dullu', 'P': 21}, {
    'LI': 211,
    'LN': 'dungeshwor',
    'P': 21
  }, {'LI': 212, 'LN': 'gurans', 'P': 21}, {'LI': 213, 'LN': 'mahabu', 'P': 21}, {
    'LI': 214,
    'LN': 'narayan',
    'P': 21
  }, {'LI': 215, 'LN': 'naumule', 'P': 21}, {'LI': 216, 'LN': 'thantikandh', 'P': 21}, {
    'LI': 217,
    'LN': 'banglachuli',
    'P': 22
  }, {'LI': 218, 'LN': 'dangisharan', 'P': 22}, {'LI': 219, 'LN': 'gadhawa', 'P': 22}, {
    'LI': 220,
    'LN': 'ghorahi',
    'P': 22
  }, {'LI': 221, 'LN': 'lamahi', 'P': 22}, {'LI': 222, 'LN': 'rajpur', 'P': 22}, {
    'LI': 223,
    'LN': 'rapti',
    'P': 22
  }, {'LI': 224, 'LN': 'shantinagar', 'P': 22}, {'LI': 225, 'LN': 'tulsipur', 'P': 22}, {
    'LI': 226,
    'LN': 'babai',
    'P': 22
  }, {'LI': 227, 'LN': 'apihimal', 'P': 23}, {'LI': 228, 'LN': 'byas', 'P': 23}, {
    'LI': 229,
    'LN': 'dunhu',
    'P': 23
  }, {'LI': 230, 'LN': 'lekam', 'P': 23}, {'LI': 231, 'LN': 'mahakali', 'P': 23}, {
    'LI': 232,
    'LN': 'malikaarjun',
    'P': 23
  }, {'LI': 233, 'LN': 'marma', 'P': 23}, {'LI': 234, 'LN': 'naugad', 'P': 23}, {
    'LI': 235,
    'LN': 'shailyashikhar',
    'P': 23
  }, {'LI': 236, 'LN': 'benighat rorang', 'P': 24}, {'LI': 237, 'LN': 'dhunibesi', 'P': 24}, {
    'LI': 238,
    'LN': 'gajuri',
    'P': 24
  }, {'LI': 239, 'LN': 'galchi', 'P': 24}, {'LI': 240, 'LN': 'gangajamuna', 'P': 24}, {
    'LI': 241,
    'LN': 'jwalamukhi',
    'P': 24
  }, {'LI': 242, 'LN': 'khaniyabash', 'P': 24}, {'LI': 243, 'LN': 'netrawati', 'P': 24}, {
    'LI': 244,
    'LN': 'nilakantha',
    'P': 24
  }, {'LI': 245, 'LN': 'rubi valley', 'P': 24}, {'LI': 246, 'LN': 'siddhalek', 'P': 24}, {
    'LI': 247,
    'LN': 'thakre',
    'P': 24
  }, {'LI': 248, 'LN': 'tripura sundari', 'P': 24}, {'LI': 249, 'LN': 'chaubise', 'P': 25}, {
    'LI': 250,
    'LN': 'chhathar jorpati',
    'P': 25
  }, {'LI': 251, 'LN': 'dhankuta', 'P': 25}, {'LI': 252, 'LN': 'khalsa chhintang shahidbhumi', 'P': 25}, {
    'LI': 253,
    'LN': 'mahalaxmi',
    'P': 25
  }, {'LI': 254, 'LN': 'pakhribas', 'P': 25}, {'LI': 255, 'LN': 'sangurigadhi', 'P': 25}, {
    'LI': 256,
    'LN': 'aaurahi',
    'P': 26
  }, {'LI': 257, 'LN': 'bateshwor', 'P': 26}, {'LI': 258, 'LN': 'bideha', 'P': 26}, {
    'LI': 259,
    'LN': 'chhireshwornath',
    'P': 26
  }, {'LI': 260, 'LN': 'dhanusadham', 'P': 26}, {'LI': 261, 'LN': 'ganeshman charnath', 'P': 26}, {
    'LI': 262,
    'LN': 'hansapur kathapulla',
    'P': 26
  }, {'LI': 263, 'LN': 'janaknandani', 'P': 26}, {'LI': 264, 'LN': 'janakpur', 'P': 26}, {
    'LI': 265,
    'LN': 'kamala siddhidatri',
    'P': 26
  }, {'LI': 266, 'LN': 'lakshminiya', 'P': 26}, {'LI': 267, 'LN': 'mithila', 'P': 26}, {
    'LI': 268,
    'LN': 'mithila bihari',
    'P': 26
  }, {'LI': 269, 'LN': 'mukhiyapatti musarmiya', 'P': 26}, {'LI': 270, 'LN': 'nagarain', 'P': 26}, {
    'LI': 271,
    'LN': 'sabaila',
    'P': 26
  }, {'LI': 272, 'LN': 'sahidnagar', 'P': 26}, {'LI': 273, 'LN': 'baiteshwor', 'P': 27}, {
    'LI': 274,
    'LN': 'bhimeshwor',
    'P': 27
  }, {'LI': 275, 'LN': 'bigu', 'P': 27}, {'LI': 276, 'LN': 'gaurishankar', 'P': 27}, {
    'LI': 277,
    'LN': 'jiri',
    'P': 27
  }, {'LI': 278, 'LN': 'kalinchok', 'P': 27}, {'LI': 279, 'LN': 'melung', 'P': 27}, {
    'LI': 280,
    'LN': 'sailung',
    'P': 27
  }, {'LI': 281, 'LN': 'tamakoshi', 'P': 27}, {'LI': 282, 'LN': 'chharka tangsong', 'P': 28}, {
    'LI': 283,
    'LN': 'dolpo buddha',
    'P': 28
  }, {'LI': 284, 'LN': 'jagadulla', 'P': 28}, {'LI': 285, 'LN': 'kaike', 'P': 28}, {
    'LI': 286,
    'LN': 'mudkechula',
    'P': 28
  }, {'LI': 287, 'LN': 'shey phoksundo', 'P': 28}, {'LI': 288, 'LN': 'thuli bheri', 'P': 28}, {
    'LI': 289,
    'LN': 'tripurasundari',
    'P': 28
  }, {'LI': 290, 'LN': 'adharsha', 'P': 29}, {'LI': 291, 'LN': 'badikedar', 'P': 29}, {
    'LI': 292,
    'LN': 'bogtan',
    'P': 29
  }, {'LI': 293, 'LN': 'dipayal silgadi', 'P': 29}, {'LI': 294, 'LN': 'jorayal', 'P': 29}, {
    'LI': 295,
    'LN': 'k i singh',
    'P': 29
  }, {'LI': 297, 'LN': 'purbichauki', 'P': 29}, {'LI': 298, 'LN': 'sayal', 'P': 29}, {
    'LI': 299,
    'LN': 'shikhar',
    'P': 29
  }, {'LI': 300, 'LN': 'aarughat', 'P': 30}, {'LI': 301, 'LN': 'ajirkot', 'P': 30}, {
    'LI': 302,
    'LN': 'bhimsen',
    'P': 30
  }, {'LI': 303, 'LN': 'chum nubri', 'P': 30}, {'LI': 304, 'LN': 'dharche', 'P': 30}, {
    'LI': 305,
    'LN': 'gandaki',
    'P': 30
  }, {'LI': 306, 'LN': 'gorkha', 'P': 30}, {'LI': 307, 'LN': 'palungtar', 'P': 30}, {
    'LI': 308,
    'LN': 'sahid lakhan',
    'P': 30
  }, {'LI': 309, 'LN': 'siranchok', 'P': 30}, {'LI': 310, 'LN': 'sulikot', 'P': 30}, {
    'LI': 311,
    'LN': 'chandrakot',
    'P': 31
  }, {'LI': 312, 'LN': 'chatrakot', 'P': 31}, {'LI': 313, 'LN': 'dhurkot', 'P': 31}, {
    'LI': 314,
    'LN': 'gulmidarbar',
    'P': 31
  }, {'LI': 315, 'LN': 'isma', 'P': 31}, {'LI': 316, 'LN': 'kaligandaki', 'P': 31}, {
    'LI': 317,
    'LN': 'madane',
    'P': 31
  }, {'LI': 318, 'LN': 'malika', 'P': 31}, {'LI': 319, 'LN': 'musikot', 'P': 31}, {
    'LI': 320,
    'LN': 'resunga',
    'P': 31
  }, {'LI': 321, 'LN': 'ruru', 'P': 31}, {'LI': 322, 'LN': 'satyawati', 'P': 31}, {
    'LI': 323,
    'LN': 'adanchuli',
    'P': 32
  }, {'LI': 324, 'LN': 'chankheli', 'P': 32}, {'LI': 325, 'LN': 'kharpunath', 'P': 32}, {
    'LI': 326,
    'LN': 'namkha',
    'P': 32
  }, {'LI': 327, 'LN': 'sarkegad', 'P': 32}, {'LI': 328, 'LN': 'simkot', 'P': 32}, {
    'LI': 329,
    'LN': 'tanjakot',
    'P': 32
  }, {'LI': 330, 'LN': 'chulachuli', 'P': 33}, {'LI': 331, 'LN': 'deumai', 'P': 33}, {
    'LI': 332,
    'LN': 'ilam',
    'P': 33
  }, {'LI': 333, 'LN': 'mai', 'P': 33}, {'LI': 334, 'LN': 'maijogmai', 'P': 33}, {
    'LI': 335,
    'LN': 'rong',
    'P': 33
  }, {'LI': 336, 'LN': 'sandakpur', 'P': 33}, {'LI': 337, 'LN': 'suryodaya', 'P': 33}, {
    'LI': 338,
    'LN': 'mangsebung',
    'P': 33
  }, {'LI': 339, 'LN': 'fakphokthum', 'P': 33}, {'LI': 340, 'LN': 'barekot', 'P': 34}, {
    'LI': 341,
    'LN': 'bheri',
    'P': 34
  }, {'LI': 342, 'LN': 'chhedagad', 'P': 34}, {'LI': 343, 'LN': 'junichande', 'P': 34}, {
    'LI': 344,
    'LN': 'kuse',
    'P': 34
  }, {'LI': 345, 'LN': 'shiwalaya', 'P': 34}, {'LI': 346, 'LN': 'tribeni nalagad', 'P': 34}, {
    'LI': 347,
    'LN': 'arjundhara',
    'P': 35
  }, {'LI': 348, 'LN': 'barhadashi', 'P': 35}, {'LI': 349, 'LN': 'bhadrapur', 'P': 35}, {
    'LI': 350,
    'LN': 'birtamod',
    'P': 35
  }, {'LI': 351, 'LN': 'buddhashanti', 'P': 35}, {'LI': 352, 'LN': 'damak', 'P': 35}, {
    'LI': 353,
    'LN': 'gauradhaha',
    'P': 35
  }, {'LI': 354, 'LN': 'gauriganj', 'P': 35}, {'LI': 355, 'LN': 'haldibari', 'P': 35}, {
    'LI': 356,
    'LN': 'jhapa',
    'P': 35
  }, {'LI': 357, 'LN': 'kachankawal', 'P': 35}, {'LI': 358, 'LN': 'kamal', 'P': 35}, {
    'LI': 359,
    'LN': 'mechinagar',
    'P': 35
  }, {'LI': 360, 'LN': 'shivasataxi', 'P': 35}, {'LI': 361, 'LN': 'kankai', 'P': 35}, {
    'LI': 362,
    'LN': 'chandannath',
    'P': 36
  }, {'LI': 363, 'LN': 'guthichaur', 'P': 36}, {'LI': 364, 'LN': 'hima', 'P': 36}, {
    'LI': 365,
    'LN': 'kanakasundari',
    'P': 36
  }, {'LI': 366, 'LN': 'patrasi', 'P': 36}, {'LI': 367, 'LN': 'sinja', 'P': 36}, {
    'LI': 368,
    'LN': 'tatopani',
    'P': 36
  }, {'LI': 369, 'LN': 'tila', 'P': 36}, {'LI': 370, 'LN': 'banepa', 'P': 37}, {
    'LI': 371,
    'LN': 'bethanchowk',
    'P': 37
  }, {'LI': 372, 'LN': 'bhumlu', 'P': 37}, {'LI': 373, 'LN': 'chaurideurali', 'P': 37}, {
    'LI': 374,
    'LN': 'dhulikhel',
    'P': 37
  }, {'LI': 375, 'LN': 'khanikhola', 'P': 37}, {'LI': 376, 'LN': 'mahabharat', 'P': 37}, {
    'LI': 377,
    'LN': 'mandandeupur',
    'P': 37
  }, {'LI': 378, 'LN': 'namobuddha', 'P': 37}, {'LI': 379, 'LN': 'panauti', 'P': 37}, {
    'LI': 380,
    'LN': 'panchkhal',
    'P': 37
  }, {'LI': 381, 'LN': 'roshi', 'P': 37}, {'LI': 382, 'LN': 'temal', 'P': 37}, {
    'LI': 383,
    'LN': 'bardagoriya',
    'P': 38
  }, {'LI': 384, 'LN': 'bhajani', 'P': 38}, {'LI': 385, 'LN': 'chure', 'P': 38}, {
    'LI': 386,
    'LN': 'dhangadhi',
    'P': 38
  }, {'LI': 387, 'LN': 'gauriganga', 'P': 38}, {'LI': 388, 'LN': 'ghodaghodi', 'P': 38}, {
    'LI': 389,
    'LN': 'godawari',
    'P': 38
  }, {'LI': 390, 'LN': 'janaki', 'P': 38}, {'LI': 391, 'LN': 'joshipur', 'P': 38}, {
    'LI': 392,
    'LN': 'kailari',
    'P': 38
  }, {'LI': 393, 'LN': 'lamkichuha', 'P': 38}, {'LI': 394, 'LN': 'mohanyal', 'P': 38}, {
    'LI': 395,
    'LN': 'tikapur',
    'P': 38
  }, {'LI': 396, 'LN': 'kalika', 'P': 39}, {'LI': 397, 'LN': 'khandachakra', 'P': 39}, {
    'LI': 398,
    'LN': 'mahawai',
    'P': 39
  }, {'LI': 399, 'LN': 'naraharinath', 'P': 39}, {'LI': 400, 'LN': 'pachaljharana', 'P': 39}, {
    'LI': 401,
    'LN': 'palata',
    'P': 39
  }, {'LI': 402, 'LN': 'raskot', 'P': 39}, {'LI': 403, 'LN': 'sanni tribeni', 'P': 39}, {
    'LI': 404,
    'LN': 'tilagufa',
    'P': 39
  }, {'LI': 405, 'LN': 'bedkot', 'P': 40}, {'LI': 406, 'LN': 'belauri', 'P': 40}, {
    'LI': 407,
    'LN': 'beldandi',
    'P': 40
  }, {'LI': 408, 'LN': 'bhimdatta', 'P': 40}, {'LI': 409, 'LN': 'krishnapur', 'P': 40}, {
    'LI': 410,
    'LN': 'laljhadi',
    'P': 40
  }, {'LI': 411, 'LN': 'mahakali', 'P': 40}, {'LI': 412, 'LN': 'punarbas', 'P': 40}, {
    'LI': 413,
    'LN': 'shuklaphanta',
    'P': 40
  }, {'LI': 415, 'LN': 'banganga', 'P': 41}, {'LI': 416, 'LN': 'bijayanagar', 'P': 41}, {
    'LI': 417,
    'LN': 'buddhabhumi',
    'P': 41
  }, {'LI': 418, 'LN': 'kapilbastu', 'P': 41}, {'LI': 419, 'LN': 'krishnanagar', 'P': 41}, {
    'LI': 420,
    'LN': 'maharajgunj',
    'P': 41
  }, {'LI': 421, 'LN': 'mayadevi', 'P': 41}, {'LI': 422, 'LN': 'shivaraj', 'P': 41}, {
    'LI': 423,
    'LN': 'suddhodhan',
    'P': 41
  }, {'LI': 424, 'LN': 'yashodhara', 'P': 41}, {'LI': 425, 'LN': 'annapurna', 'P': 42}, {
    'LI': 426,
    'LN': 'machhapuchchhre',
    'P': 42
  }, {'LI': 427, 'LN': 'madi', 'P': 42}, {'LI': 428, 'LN': 'pokhara lekhnath', 'P': 42}, {
    'LI': 429,
    'LN': 'rupa',
    'P': 42
  }, {'LI': 430, 'LN': 'budhanilakantha', 'P': 43}, {'LI': 431, 'LN': 'chandragiri', 'P': 43}, {
    'LI': 432,
    'LN': 'dakshinkali',
    'P': 43
  }, {'LI': 433, 'LN': 'gokarneshwor', 'P': 43}, {'LI': 434, 'LN': 'kageshwori manahora', 'P': 43}, {
    'LI': 435,
    'LN': 'kathmandu',
    'P': 43
  }, {'LI': 436, 'LN': 'kirtipur', 'P': 43}, {'LI': 437, 'LN': 'nagarjun', 'P': 43}, {
    'LI': 438,
    'LN': 'shankharapur',
    'P': 43
  }, {'LI': 439, 'LN': 'tarakeshwor', 'P': 43}, {'LI': 440, 'LN': 'tokha', 'P': 43}, {
    'LI': 441,
    'LN': 'ainselukhark',
    'P': 44
  }, {'LI': 442, 'LN': 'barahapokhari', 'P': 44}, {'LI': 443, 'LN': 'diprung', 'P': 44}, {
    'LI': 444,
    'LN': 'halesi tuwachung',
    'P': 44
  }, {'LI': 445, 'LN': 'jantedhunga', 'P': 44}, {'LI': 446, 'LN': 'kepilasagadhi', 'P': 44}, {
    'LI': 447,
    'LN': 'khotehang',
    'P': 44
  }, {'LI': 448, 'LN': 'lamidanda', 'P': 44}, {'LI': 449, 'LN': 'rupakot majhuwagadhi', 'P': 44}, {
    'LI': 450,
    'LN': 'sakela',
    'P': 44
  }, {'LI': 451, 'LN': 'bagmati', 'P': 45}, {'LI': 452, 'LN': 'godawari', 'P': 45}, {
    'LI': 453,
    'LN': 'konjyosom',
    'P': 45
  }, {'LI': 454, 'LN': 'lalitpur', 'P': 45}, {'LI': 455, 'LN': 'mahalaxmi', 'P': 45}, {
    'LI': 456,
    'LN': 'mahankal',
    'P': 45
  }, {'LI': 457, 'LN': 'besishahar', 'P': 46}, {'LI': 458, 'LN': 'dordi', 'P': 46}, {
    'LI': 459,
    'LN': 'dudhpokhari',
    'P': 46
  }, {'LI': 460, 'LN': 'kwholasothar', 'P': 46}, {'LI': 461, 'LN': 'madhyanepal', 'P': 46}, {
    'LI': 462,
    'LN': 'marsyangdi',
    'P': 46
  }, {'LI': 463, 'LN': 'sundarbazar', 'P': 46}, {'LI': 464, 'LN': 'rainas', 'P': 46}, {
    'LI': 465,
    'LN': 'aurahi',
    'P': 47
  }, {'LI': 466, 'LN': 'balwa', 'P': 47}, {'LI': 467, 'LN': 'bardibas', 'P': 47}, {
    'LI': 468,
    'LN': 'bhangaha',
    'P': 47
  }, {'LI': 469, 'LN': 'ekdanra', 'P': 47}, {'LI': 470, 'LN': 'gaushala', 'P': 47}, {
    'LI': 471,
    'LN': 'jaleswor',
    'P': 47
  }, {'LI': 472, 'LN': 'loharpatti', 'P': 47}, {'LI': 473, 'LN': 'mahottari', 'P': 47}, {
    'LI': 474,
    'LN': 'manra',
    'P': 47
  }, {'LI': 475, 'LN': 'matihani', 'P': 47}, {'LI': 476, 'LN': 'pipra', 'P': 47}, {
    'LI': 477,
    'LN': 'ramgopalpur',
    'P': 47
  }, {'LI': 478, 'LN': 'samsi', 'P': 47}, {'LI': 479, 'LN': 'sonama', 'P': 47}, {
    'LI': 480,
    'LN': 'bagmati',
    'P': 48
  }, {'LI': 481, 'LN': 'bakaiya', 'P': 48}, {'LI': 482, 'LN': 'bhimphedi', 'P': 48}, {
    'LI': 484,
    'LN': 'hetauda',
    'P': 48
  }, {'LI': 485, 'LN': 'indrasarowar', 'P': 48}, {'LI': 486, 'LN': 'kailash', 'P': 48}, {
    'LI': 487,
    'LN': 'makawanpurgadhi',
    'P': 48
  }, {'LI': 488, 'LN': 'manahari', 'P': 48}, {'LI': 490, 'LN': 'raksirang', 'P': 48}, {
    'LI': 491,
    'LN': 'thaha',
    'P': 48
  }, {'LI': 492, 'LN': 'chame gaunpalika', 'P': 49}, {'LI': 493, 'LN': 'narphu gaunpalika', 'P': 49}, {
    'LI': 494,
    'LN': 'nashong gaunpalika',
    'P': 49
  }, {'LI': 495, 'LN': 'neshyang gaunpalika', 'P': 49}, {'LI': 496, 'LN': 'belbari', 'P': 50}, {
    'LI': 497,
    'LN': 'biratnagar',
    'P': 50
  }, {'LI': 498, 'LN': 'budhiganga', 'P': 50}, {'LI': 499, 'LN': 'dhanpalthan', 'P': 50}, {
    'LI': 500,
    'LN': 'gramthan',
    'P': 50
  }, {'LI': 501, 'LN': 'jahada', 'P': 50}, {'LI': 502, 'LN': 'kanepokhari', 'P': 50}, {
    'LI': 503,
    'LN': 'katahari',
    'P': 50
  }, {'LI': 504, 'LN': 'kerabari', 'P': 50}, {'LI': 505, 'LN': 'letang', 'P': 50}, {
    'LI': 506,
    'LN': 'miklajung',
    'P': 50
  }, {'LI': 507, 'LN': 'patahrishanishchare', 'P': 50}, {'LI': 508, 'LN': 'rangeli', 'P': 50}, {
    'LI': 509,
    'LN': 'ratuwamai',
    'P': 50
  }, {'LI': 510, 'LN': 'sundarharaicha', 'P': 50}, {'LI': 511, 'LN': 'sunwarshi', 'P': 50}, {
    'LI': 512,
    'LN': 'uralabari',
    'P': 50
  }, {'LI': 513, 'LN': 'chhayanath rara', 'P': 51}, {'LI': 514, 'LN': 'khatyad', 'P': 51}, {
    'LI': 515,
    'LN': 'mugum karmarong',
    'P': 51
  }, {'LI': 516, 'LN': 'soru', 'P': 51}, {'LI': 517, 'LN': 'gharapjhong', 'P': 52}, {
    'LI': 518,
    'LN': 'lomanthang',
    'P': 52
  }, {'LI': 519, 'LN': 'barhagaun muktikhsetra', 'P': 52}, {'LI': 520, 'LN': 'thasang', 'P': 52}, {
    'LI': 521,
    'LN': 'dalome',
    'P': 52
  }, {'LI': 522, 'LN': 'annapurna', 'P': 53}, {'LI': 523, 'LN': 'beni', 'P': 53}, {
    'LI': 524,
    'LN': 'dhaulagiri',
    'P': 53
  }, {'LI': 525, 'LN': 'dhorpatan hunting reserve', 'P': 53}, {'LI': 526, 'LN': 'malika', 'P': 53}, {
    'LI': 527,
    'LN': 'mangala',
    'P': 53
  }, {'LI': 528, 'LN': 'raghuganga', 'P': 53}, {'LI': 529, 'LN': 'bardaghat', 'P': 54}, {
    'LI': 530,
    'LN': 'binayee',
    'P': 54
  }, {'LI': 531, 'LN': 'bulingtar', 'P': 54}, {'LI': 532, 'LN': 'bungdikali', 'P': 54}, {
    'LI': 534,
    'LN': 'devchuli',
    'P': 54
  }, {'LI': 535, 'LN': 'gaidakot', 'P': 54}, {'LI': 536, 'LN': 'hupsekot', 'P': 54}, {
    'LI': 537,
    'LN': 'kawasoti',
    'P': 54
  }, {'LI': 538, 'LN': 'madhyabindu', 'P': 54}, {'LI': 539, 'LN': 'palhi nandan', 'P': 54}, {
    'LI': 540,
    'LN': 'pratappur',
    'P': 54
  }, {'LI': 541, 'LN': 'ramgram', 'P': 54}, {'LI': 542, 'LN': 'sarawal', 'P': 54}, {
    'LI': 543,
    'LN': 'sunwal',
    'P': 54
  }, {'LI': 544, 'LN': 'tribenisusta', 'P': 54}, {'LI': 545, 'LN': 'belkotgadhi', 'P': 55}, {
    'LI': 546,
    'LN': 'bidur',
    'P': 55
  }, {'LI': 547, 'LN': 'dupcheshwar', 'P': 55}, {'LI': 548, 'LN': 'kakani', 'P': 55}, {
    'LI': 549,
    'LN': 'kispang',
    'P': 55
  }, {'LI': 551, 'LN': 'likhu', 'P': 55}, {'LI': 552, 'LN': 'meghang', 'P': 55}, {
    'LI': 553,
    'LN': 'panchakanya',
    'P': 55
  }, {'LI': 555, 'LN': 'shivapuri', 'P': 55}, {'LI': 556, 'LN': 'suryagadhi', 'P': 55}, {
    'LI': 557,
    'LN': 'tadi',
    'P': 55
  }, {'LI': 558, 'LN': 'tarkeshwar', 'P': 55}, {'LI': 559, 'LN': 'champadevi', 'P': 56}, {
    'LI': 560,
    'LN': 'chisankhugadhi',
    'P': 56
  }, {'LI': 561, 'LN': 'khijidemba', 'P': 56}, {'LI': 562, 'LN': 'likhu', 'P': 56}, {
    'LI': 563,
    'LN': 'manebhanjyang',
    'P': 56
  }, {'LI': 564, 'LN': 'molung', 'P': 56}, {'LI': 565, 'LN': 'siddhicharan', 'P': 56}, {
    'LI': 566,
    'LN': 'sunkoshi',
    'P': 56
  }, {'LI': 567, 'LN': 'bagnaskali', 'P': 57}, {'LI': 568, 'LN': 'mathagadhi', 'P': 57}, {
    'LI': 569,
    'LN': 'nisdi',
    'P': 57
  }, {'LI': 570, 'LN': 'purbakhola', 'P': 57}, {'LI': 571, 'LN': 'rainadevi chhahara', 'P': 57}, {
    'LI': 572,
    'LN': 'rambha',
    'P': 57
  }, {'LI': 573, 'LN': 'rampur', 'P': 57}, {'LI': 574, 'LN': 'ribdikot', 'P': 57}, {
    'LI': 575,
    'LN': 'tansen',
    'P': 57
  }, {'LI': 576, 'LN': 'tinau', 'P': 57}, {'LI': 577, 'LN': 'falelung', 'P': 58}, {
    'LI': 578,
    'LN': 'falgunanda',
    'P': 58
  }, {'LI': 579, 'LN': 'hilihang', 'P': 58}, {'LI': 580, 'LN': 'kummayak', 'P': 58}, {
    'LI': 581,
    'LN': 'miklajung',
    'P': 58
  }, {'LI': 582, 'LN': 'phidim', 'P': 58}, {'LI': 583, 'LN': 'tumbewa', 'P': 58}, {
    'LI': 584,
    'LN': 'yangwarak',
    'P': 58
  }, {'LI': 585, 'LN': 'bihadi', 'P': 59}, {'LI': 586, 'LN': 'jaljala', 'P': 59}, {
    'LI': 587,
    'LN': 'kushma',
    'P': 59
  }, {'LI': 588, 'LN': 'mahashila', 'P': 59}, {'LI': 589, 'LN': 'modi', 'P': 59}, {
    'LI': 590,
    'LN': 'painyu',
    'P': 59
  }, {'LI': 591, 'LN': 'phalebas', 'P': 59}, {'LI': 592, 'LN': 'bahudaramai', 'P': 60}, {
    'LI': 593,
    'LN': 'belwa',
    'P': 60
  }, {'LI': 594, 'LN': 'bindabasini', 'P': 60}, {'LI': 595, 'LN': 'birgunj', 'P': 60}, {
    'LI': 596,
    'LN': 'chhipaharmai',
    'P': 60
  }, {'LI': 598, 'LN': 'dhobini', 'P': 60}, {'LI': 599, 'LN': 'jagarnathpur', 'P': 60}, {
    'LI': 600,
    'LN': 'pakahamainpur',
    'P': 60
  }, {'LI': 602, 'LN': 'parsagadhi', 'P': 60}, {'LI': 603, 'LN': 'paterwasugauli', 'P': 60}, {
    'LI': 604,
    'LN': 'pokhariya',
    'P': 60
  }, {'LI': 605, 'LN': 'sakhuwaprasauni', 'P': 60}, {'LI': 606, 'LN': 'subarnapur', 'P': 60}, {
    'LI': 607,
    'LN': 'ayirabati',
    'P': 61
  }, {'LI': 608, 'LN': 'gaumukhi', 'P': 61}, {'LI': 609, 'LN': 'jhimruk', 'P': 61}, {
    'LI': 610,
    'LN': 'mallarani',
    'P': 61
  }, {'LI': 611, 'LN': 'mandavi', 'P': 61}, {'LI': 612, 'LN': 'naubahini', 'P': 61}, {
    'LI': 613,
    'LN': 'pyuthan',
    'P': 61
  }, {'LI': 614, 'LN': 'sarumarani', 'P': 61}, {'LI': 615, 'LN': 'sworgadwary', 'P': 61}, {
    'LI': 616,
    'LN': 'doramba',
    'P': 62
  }, {'LI': 617, 'LN': 'gokulganga', 'P': 62}, {'LI': 618, 'LN': 'khadadevi', 'P': 62}, {
    'LI': 619,
    'LN': 'likhu',
    'P': 62
  }, {'LI': 620, 'LN': 'manthali', 'P': 62}, {'LI': 621, 'LN': 'ramechhap', 'P': 62}, {
    'LI': 622,
    'LN': 'sunapati',
    'P': 62
  }, {'LI': 623, 'LN': 'umakunda', 'P': 62}, {'LI': 624, 'LN': 'gosaikunda', 'P': 63}, {
    'LI': 625,
    'LN': 'kalika',
    'P': 63
  }, {'LI': 626, 'LN': 'naukunda', 'P': 63}, {'LI': 627, 'LN': 'parbati kunda', 'P': 63}, {
    'LI': 628,
    'LN': 'uttargaya',
    'P': 63
  }, {'LI': 629, 'LN': 'baudhimai', 'P': 64}, {'LI': 630, 'LN': 'birndaban', 'P': 64}, {
    'LI': 631,
    'LN': 'chandrapur',
    'P': 64
  }, {'LI': 632, 'LN': 'dewahhi gonahi', 'P': 64}, {'LI': 633, 'LN': 'durga bhagwati', 'P': 64}, {
    'LI': 634,
    'LN': 'gadhimai',
    'P': 64
  }, {'LI': 635, 'LN': 'garuda', 'P': 64}, {'LI': 636, 'LN': 'gaur', 'P': 64}, {
    'LI': 637,
    'LN': 'gujara',
    'P': 64
  }, {'LI': 638, 'LN': 'ishanath', 'P': 64}, {'LI': 639, 'LN': 'katahariya', 'P': 64}, {
    'LI': 640,
    'LN': 'madhav narayan',
    'P': 64
  }, {'LI': 641, 'LN': 'maulapur', 'P': 64}, {'LI': 642, 'LN': 'paroha', 'P': 64}, {
    'LI': 643,
    'LN': 'phatuwa bijayapur',
    'P': 64
  }, {'LI': 644, 'LN': 'rajpur', 'P': 64}, {'LI': 645, 'LN': 'duikholi', 'P': 65}, {
    'LI': 646,
    'LN': 'rolpa',
    'P': 65
  }, {'LI': 647, 'LN': 'runtigadi', 'P': 65}, {'LI': 648, 'LN': 'sunchhahari', 'P': 65}, {
    'LI': 649,
    'LN': 'suwarnabati',
    'P': 65
  }, {'LI': 650, 'LN': 'thawang', 'P': 65}, {'LI': 651, 'LN': 'tribeni', 'P': 65}, {
    'LI': 652,
    'LN': 'lungri',
    'P': 65
  }, {'LI': 653, 'LN': 'sukidaha', 'P': 65}, {'LI': 654, 'LN': 'madi', 'P': 65}, {
    'LI': 655,
    'LN': 'aathbiskot',
    'P': 66
  }, {'LI': 656, 'LN': 'banfikot', 'P': 66}, {'LI': 657, 'LN': 'bhume', 'P': 66}, {
    'LI': 658,
    'LN': 'chaurjahari',
    'P': 66
  }, {'LI': 659, 'LN': 'dhorpatan hunting reserve', 'P': 66}, {'LI': 660, 'LN': 'musikot', 'P': 66}, {
    'LI': 661,
    'LN': 'putha uttarganga',
    'P': 66
  }, {'LI': 662, 'LN': 'sani bheri', 'P': 66}, {'LI': 663, 'LN': 'sisne', 'P': 66}, {
    'LI': 664,
    'LN': 'tribeni',
    'P': 66
  }, {'LI': 665, 'LN': 'butwal', 'P': 67}, {'LI': 666, 'LN': 'devdaha', 'P': 67}, {
    'LI': 667,
    'LN': 'gaidahawa',
    'P': 67
  }, {'LI': 668, 'LN': 'kanchan', 'P': 67}, {'LI': 669, 'LN': 'kotahimai', 'P': 67}, {
    'LI': 670,
    'LN': 'lumbini sanskritik development area',
    'P': 67
  }, {'LI': 671, 'LN': 'marchawari', 'P': 67}, {'LI': 672, 'LN': 'mayadevi', 'P': 67}, {
    'LI': 673,
    'LN': 'omsatiya',
    'P': 67
  }, {'LI': 674, 'LN': 'rohini', 'P': 67}, {'LI': 675, 'LN': 'sainamaina', 'P': 67}, {
    'LI': 676,
    'LN': 'sammarimai',
    'P': 67
  }, {'LI': 677, 'LN': 'siddharthanagar', 'P': 67}, {'LI': 678, 'LN': 'siyari', 'P': 67}, {
    'LI': 679,
    'LN': 'sudhdhodhan',
    'P': 67
  }, {'LI': 680, 'LN': 'tillotama', 'P': 67}, {'LI': 681, 'LN': 'lumbini sanskritik', 'P': 67}, {
    'LI': 682,
    'LN': 'bagchaur',
    'P': 68
  }, {'LI': 683, 'LN': 'bangad kupinde', 'P': 68}, {'LI': 684, 'LN': 'chhatreshwori', 'P': 68}, {
    'LI': 685,
    'LN': 'darma',
    'P': 68
  }, {'LI': 686, 'LN': 'dhorchaur', 'P': 68}, {'LI': 687, 'LN': 'kalimati', 'P': 68}, {
    'LI': 688,
    'LN': 'kapurkot',
    'P': 68
  }, {'LI': 689, 'LN': 'kumakhmalika', 'P': 68}, {'LI': 690, 'LN': 'sharada', 'P': 68}, {
    'LI': 691,
    'LN': 'tribeni',
    'P': 68
  }, {'LI': 692, 'LN': 'bhotkhola', 'P': 69}, {'LI': 693, 'LN': 'chainpur', 'P': 69}, {
    'LI': 694,
    'LN': 'chichila',
    'P': 69
  }, {'LI': 695, 'LN': 'dharmadevi', 'P': 69}, {'LI': 696, 'LN': 'khandbari', 'P': 69}, {
    'LI': 697,
    'LN': 'madi',
    'P': 69
  }, {'LI': 698, 'LN': 'makalu', 'P': 69}, {'LI': 699, 'LN': 'panchakhapan', 'P': 69}, {
    'LI': 700,
    'LN': 'sabhapokhari',
    'P': 69
  }, {'LI': 701, 'LN': 'silichong', 'P': 69}, {'LI': 702, 'LN': 'belhi chapena', 'P': 70}, {
    'LI': 703,
    'LN': 'bishnupur',
    'P': 70
  }, {'LI': 704, 'LN': 'bode barsain', 'P': 70}, {'LI': 705, 'LN': 'chhinnamasta', 'P': 70}, {
    'LI': 706,
    'LN': 'dakneshwori',
    'P': 70
  }, {'LI': 707, 'LN': 'hanumannagar kankalani', 'P': 70}, {'LI': 708, 'LN': 'kanchanrup', 'P': 70}, {
    'LI': 709,
    'LN': 'khadak',
    'P': 70
  }, {'LI': 711, 'LN': 'krishna savaran', 'P': 70}, {'LI': 712, 'LN': 'mahadev', 'P': 70}, {
    'LI': 713,
    'LN': 'rajbiraj',
    'P': 70
  }, {'LI': 714, 'LN': 'rupani', 'P': 70}, {'LI': 715, 'LN': 'saptakoshi', 'P': 70}, {
    'LI': 716,
    'LN': 'shambhunath',
    'P': 70
  }, {'LI': 717, 'LN': 'surunga', 'P': 70}, {'LI': 718, 'LN': 'tilathi koiladi', 'P': 70}, {
    'LI': 719,
    'LN': 'tirahut',
    'P': 70
  }, {'LI': 720, 'LN': 'bagmati', 'P': 71}, {'LI': 721, 'LN': 'balara', 'P': 71}, {
    'LI': 722,
    'LN': 'barahathawa',
    'P': 71
  }, {'LI': 723, 'LN': 'bishnu', 'P': 71}, {'LI': 724, 'LN': 'bramhapuri', 'P': 71}, {
    'LI': 725,
    'LN': 'chakraghatta',
    'P': 71
  }, {'LI': 726, 'LN': 'chandranagar', 'P': 71}, {'LI': 727, 'LN': 'dhankaul', 'P': 71}, {
    'LI': 728,
    'LN': 'gaudeta',
    'P': 71
  }, {'LI': 729, 'LN': 'haripur', 'P': 71}, {'LI': 730, 'LN': 'haripurwa', 'P': 71}, {
    'LI': 731,
    'LN': 'hariwan',
    'P': 71
  }, {'LI': 732, 'LN': 'ishworpur', 'P': 71}, {'LI': 733, 'LN': 'kabilasi', 'P': 71}, {
    'LI': 734,
    'LN': 'lalbandi',
    'P': 71
  }, {'LI': 735, 'LN': 'malangawa', 'P': 71}, {'LI': 736, 'LN': 'ramnagar', 'P': 71}, {
    'LI': 737,
    'LN': 'dudhouli',
    'P': 72
  }, {'LI': 738, 'LN': 'ghanglekh', 'P': 72}, {'LI': 739, 'LN': 'golanjor', 'P': 72}, {
    'LI': 740,
    'LN': 'hariharpurgadhi',
    'P': 72
  }, {'LI': 741, 'LN': 'kamalamai', 'P': 72}, {'LI': 742, 'LN': 'marin', 'P': 72}, {
    'LI': 743,
    'LN': 'phikkal',
    'P': 72
  }, {'LI': 744, 'LN': 'sunkoshi', 'P': 72}, {'LI': 745, 'LN': 'tinpatan', 'P': 72}, {
    'LI': 746,
    'LN': 'balefi',
    'P': 73
  }, {'LI': 747, 'LN': 'barhabise', 'P': 73}, {'LI': 748, 'LN': 'bhotekoshi', 'P': 73}, {
    'LI': 749,
    'LN': 'chautara sangachokgadhi',
    'P': 73
  }, {'LI': 750, 'LN': 'helambu', 'P': 73}, {'LI': 751, 'LN': 'indrawati', 'P': 73}, {
    'LI': 752,
    'LN': 'jugal',
    'P': 73
  }, {'LI': 753, 'LN': 'lisangkhu pakhar', 'P': 73}, {'LI': 754, 'LN': 'melamchi', 'P': 73}, {
    'LI': 755,
    'LN': 'panchpokhari thangpal',
    'P': 73
  }, {'LI': 756, 'LN': 'sunkoshi', 'P': 73}, {'LI': 757, 'LN': 'tripurasundari', 'P': 73}, {
    'LI': 758,
    'LN': 'arnama',
    'P': 74
  }, {'LI': 759, 'LN': 'aurahi', 'P': 74}, {'LI': 760, 'LN': 'bariyarpatti', 'P': 74}, {
    'LI': 761,
    'LN': 'bhagawanpur',
    'P': 74
  }, {'LI': 762, 'LN': 'bishnupur', 'P': 74}, {'LI': 763, 'LN': 'dhangadhimai', 'P': 74}, {
    'LI': 764,
    'LN': 'golbazar',
    'P': 74
  }, {'LI': 765, 'LN': 'kalyanpur', 'P': 74}, {'LI': 766, 'LN': 'karjanhya', 'P': 74}, {
    'LI': 767,
    'LN': 'lahan',
    'P': 74
  }, {'LI': 768, 'LN': 'laxmipur patari', 'P': 74}, {'LI': 769, 'LN': 'mirchaiya', 'P': 74}, {
    'LI': 770,
    'LN': 'naraha',
    'P': 74
  }, {'LI': 771, 'LN': 'nawarajpur', 'P': 74}, {'LI': 772, 'LN': 'sakhuwanankarkatti', 'P': 74}, {
    'LI': 773,
    'LN': 'siraha',
    'P': 74
  }, {'LI': 774, 'LN': 'sukhipur', 'P': 74}, {'LI': 775, 'LN': 'dudhkaushika', 'P': 75}, {
    'LI': 776,
    'LN': 'dudhkoshi',
    'P': 75
  }, {'LI': 777, 'LN': 'khumbupasanglahmu', 'P': 75}, {'LI': 778, 'LN': 'likhupike', 'P': 75}, {
    'LI': 779,
    'LN': 'mahakulung',
    'P': 75
  }, {'LI': 780, 'LN': 'nechasalyan', 'P': 75}, {'LI': 781, 'LN': 'solududhakunda', 'P': 75}, {
    'LI': 782,
    'LN': 'sotang',
    'P': 75
  }, {'LI': 783, 'LN': 'barah', 'P': 76}, {'LI': 784, 'LN': 'barju', 'P': 76}, {
    'LI': 785,
    'LN': 'bhokraha',
    'P': 76
  }, {'LI': 786, 'LN': 'dewanganj', 'P': 76}, {'LI': 787, 'LN': 'dharan', 'P': 76}, {
    'LI': 788,
    'LN': 'duhabi',
    'P': 76
  }, {'LI': 789, 'LN': 'gadhi', 'P': 76}, {'LI': 790, 'LN': 'harinagara', 'P': 76}, {
    'LI': 791,
    'LN': 'inaruwa',
    'P': 76
  }, {'LI': 792, 'LN': 'itahari', 'P': 76}, {'LI': 793, 'LN': 'koshi', 'P': 76}, {
    'LI': 795,
    'LN': 'ramdhuni',
    'P': 76
  }, {'LI': 796, 'LN': 'barahtal', 'P': 77}, {'LI': 797, 'LN': 'bheriganga', 'P': 77}, {
    'LI': 798,
    'LN': 'birendranagar',
    'P': 77
  }, {'LI': 799, 'LN': 'chaukune', 'P': 77}, {'LI': 800, 'LN': 'chingad', 'P': 77}, {
    'LI': 801,
    'LN': 'gurbhakot',
    'P': 77
  }, {'LI': 802, 'LN': 'lekbeshi', 'P': 77}, {'LI': 803, 'LN': 'panchpuri', 'P': 77}, {
    'LI': 804,
    'LN': 'simta',
    'P': 77
  }, {'LI': 805, 'LN': 'aandhikhola', 'P': 78}, {'LI': 806, 'LN': 'arjunchaupari', 'P': 78}, {
    'LI': 807,
    'LN': 'biruwa',
    'P': 78
  }, {'LI': 808, 'LN': 'galyang', 'P': 78}, {'LI': 809, 'LN': 'harinas', 'P': 78}, {
    'LI': 810,
    'LN': 'kaligandagi',
    'P': 78
  }, {'LI': 811, 'LN': 'phedikhola', 'P': 78}, {'LI': 812, 'LN': 'putalibazar', 'P': 78}, {
    'LI': 813,
    'LN': 'waling',
    'P': 78
  }, {'LI': 814, 'LN': 'chapakot', 'P': 78}, {'LI': 815, 'LN': 'bhirkot', 'P': 78}, {
    'LI': 816,
    'LN': 'anbukhaireni',
    'P': 79
  }, {'LI': 817, 'LN': 'bandipur', 'P': 79}, {'LI': 818, 'LN': 'bhanu', 'P': 79}, {
    'LI': 819,
    'LN': 'bhimad',
    'P': 79
  }, {'LI': 820, 'LN': 'byas', 'P': 79}, {'LI': 821, 'LN': 'devghat', 'P': 79}, {
    'LI': 822,
    'LN': 'ghiring',
    'P': 79
  }, {'LI': 823, 'LN': 'myagde', 'P': 79}, {'LI': 824, 'LN': 'rhishing', 'P': 79}, {
    'LI': 825,
    'LN': 'shuklagandaki',
    'P': 79
  }, {'LI': 826, 'LN': 'aathrai tribeni', 'P': 80}, {'LI': 827, 'LN': 'maiwakhola', 'P': 80}, {
    'LI': 828,
    'LN': 'meringden',
    'P': 80
  }, {'LI': 829, 'LN': 'mikwakhola', 'P': 80}, {'LI': 830, 'LN': 'phaktanglung', 'P': 80}, {
    'LI': 831,
    'LN': 'phungling',
    'P': 80
  }, {'LI': 832, 'LN': 'sidingba', 'P': 80}, {'LI': 833, 'LN': 'sirijangha', 'P': 80}, {
    'LI': 834,
    'LN': 'yangwarak',
    'P': 80
  }, {'LI': 835, 'LN': 'aathrai', 'P': 81}, {'LI': 836, 'LN': 'chhathar', 'P': 81}, {
    'LI': 837,
    'LN': 'laligurans',
    'P': 81
  }, {'LI': 838, 'LN': 'menchayam', 'P': 81}, {'LI': 839, 'LN': 'myanglung', 'P': 81}, {
    'LI': 840,
    'LN': 'phedap',
    'P': 81
  }, {'LI': 841, 'LN': 'belaka', 'P': 82}, {'LI': 842, 'LN': 'chaudandigadhi', 'P': 82}, {
    'LI': 843,
    'LN': 'katari',
    'P': 82
  }, {'LI': 845, 'LN': 'rautamai', 'P': 82}, {'LI': 846, 'LN': 'sunkoshi', 'P': 82}, {
    'LI': 847,
    'LN': 'tapli',
    'P': 82
  }, {'LI': 848, 'LN': 'triyuga', 'P': 82}, {'LI': 849, 'LN': 'udayapurgadhi', 'P': 82}

]

export const OptionYesNo = [{label: 'Yes', value: 'yes'}, {label: 'No', value: 'no'}]
export const OptionGender = [{label: 'Male', value: 'male'}, {label: 'Female', value: 'female'}, {
  label: 'Other',
  value: 'other'
}]
export const OptionNationality = [{label: 'Nepalese', value: 'nepalese'}, {
  label: 'NRN',
  value: 'nrn'
}, {label: 'Foreigner', value: 'foreigner'}]
export const optionEmploymentType = [{label: 'Salaried', value: 'salaried'}, {
  label: 'Business',
  value: 'business'
}, {label: 'Both', value: 'both'}, {label: 'Other', value: 'other'}]
export const optionNatureOfBusiness = [{label: 'Trading', value: 'trading'}, {
  label: 'Industry',
  value: 'industry'
}, {label: 'Service', value: 'service'}, {label: 'Other', value: 'other'}]
export const optionReligion = [{label: 'Buddhist', value: 'buddhist'}, {
  label: 'Christian',
  value: 'christian'
}, {label: 'Hindu', value: 'hindu'}, {label: 'Muslim', value: 'muslim'}, {label: 'Other', value: 'other'}]
export const optionEducation = [
  {label: 'Uneducated', value: 'uneducated'},
  {label: 'Literate', value: 'literate'},
  {label: 'SLC', value: 'slc'},
  {label: 'Graduate', value: 'graduate'},
  {label: 'Post graduate', value: 'post graduate'},
  {label: 'PhD', value: 'phd'}]

export const optionMaritalStatus = [{label: 'Married', value: 'married'}, {
  label: 'Unmarried',
  value: 'unmarried'
}]
