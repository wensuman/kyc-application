import api_url from './api'
export const server_uri = api_url;
export const bank_uri  = 'https://dev-bank.kycnpl.com/'
export const server_url = server_uri+'api/'
export const wp_uri = 'https://kycnpl.com';



export const urler = function (url) {
    return server_url + url + '/'
}

export const urle = function(url){
  return server_url + url
}

export const Urls = {
    site:server_uri,
    bank:bank_uri,
    main:server_url,
    wp_site_faq: wp_uri+'/faq',
    profile:urler('profile/me'),
    login: urler('auth/login'),
    banks: urler('bank-list'),
    access: urler('access'),
    register:urler('auth/register'),
    tickets: urler('tickets'),
    verify: urler('auth/verify'),
    log: urler('log'),
    upload: urler('upload'),
    forgot_password: urler('forgot-password'),
    dashboard: urler('dashboard'),
    changePassword: urler('change-password'),
    voters: urler('voters-details'),
    form:{
        template: urler('template'),
        submission: urler('submission'),
        history: urler('history'),
        deleter: urler('history')
    }
}



export default server_url;
